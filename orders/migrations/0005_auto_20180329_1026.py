# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-03-29 10:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0004_auto_20180327_0936'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ordereventhasproduct',
            name='base_is_default',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='ordereventhasproduct',
            name='price_is_default',
            field=models.BooleanField(default=True),
        ),
    ]
