from django.forms import ModelForm, TextInput
from django import forms
from orders.models import Order,\
    OrderEvent, OrderEventHasProduct, Transport, GatherNote, \
    GatherNoteProducts, DeliveryNote, DeliveryNoteProducts, \
    ReturnReceipt, CountingNote, CountingNoteProducts


class OrderForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = Order
        fields = '__all__'
        exclude = ['oh_amount']


class OrderEventForm(ModelForm):
    base_total_rent = forms.CharField(help_text='Grundhyra uthyrning')
    base_total_forward = forms.CharField(
            help_text='Grundhyra vidareuthyrning')
    sum_total_sale = forms.CharField(
                                    help_text="Summa försäljning",
                                    required=False,
                                    )
    sum_total = forms.CharField(
                                help_text='Pris per dag',
                                required=False,
                                )
    sum_total_30 = forms.CharField(
                                   help_text='Summa 30 dagar',
                                   required=False
                                   )

    def __init__(self, *args, **kwargs):
        super(OrderEventForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

        self.fields['base_total_rent'].widget.attrs.update({
            'class': "form-control form-control-sm ",
            })

        self.fields['base_total_forward'].widget.attrs.update({
            'class': "form-control form-control-sm ",
            })

    class Meta:
        model = OrderEvent
        fields = '__all__'
        widgets = {
            'gather_note': TextInput(),
            'delivery_note': TextInput(),
            'counting_note': TextInput(),
            'base_total_rent': TextInput(),
            'base_total_forward': TextInput(),
        }


class OehpForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(OehpForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = OrderEventHasProduct
        fields = '__all__'
        widgets = {
            'amount': TextInput(),
            'base': TextInput(),
            'price': TextInput(),
            'discount': TextInput(),
            'total_price': TextInput(),
            'total_price_per_month': TextInput(),
            'account': TextInput(),
        }


class TransportForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(TransportForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = Transport
        fields = '__all__'
        widgets = {
            'price': TextInput(),
            'order_event': TextInput(),
            'total_weight': TextInput(),
        }


class GatherNoteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(GatherNoteForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = GatherNote
        fields = '__all__'


class GatherNoteProductsForm(ModelForm):
    # product = ModelChoiceField(queryset=Product.objects.all())

    def __init__(self, *args, **kwargs):
        super(GatherNoteProductsForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = GatherNoteProducts
        fields = '__all__'
        widgets = {
           'amount': TextInput()
           }


class DeliveryNoteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DeliveryNoteForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = DeliveryNote
        fields = '__all__'
        widgets = {
            'phone_nr': TextInput(),
        }


class DeliveryNoteProductsForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(DeliveryNoteProductsForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = DeliveryNoteProducts
        fields = '__all__'
        widgets = {
            'amount': TextInput()
        }


class ReturnReceiptForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(ReturnReceiptForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = ReturnReceipt
        fields = '__all__'


class CountingNoteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CountingNoteForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = CountingNote
        fields = '__all__'


class CountingNoteProductsForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(CountingNoteProductsForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

    class Meta:
        model = CountingNoteProducts
        fields = '__all__'
        widgets = {
            'amount': TextInput(),
            'bad_amount': TextInput(),
            'amount_out': TextInput()
        }


class ListOrderEventForm(forms.Form):
    RADIO_COICES = [('all', 'Alla'),
                    ('yes', 'Ja'),
                    ('no', 'Nej')]

    customer = forms.CharField(required=False, max_length=128, label="Kund")
    start_date = forms.DateField(required=False, label="datum (från)")
    end_date = forms.DateField(required=False, label="datum (till)")
    delivery_event = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Leverans"
    )
    return_event = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Retur"
    )
    ongoing = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Pågående"
    )
    completed = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Avslutad"
    )
    has_rent_date = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Uthyrningsdatum inlagt"
    )
    shipped = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Levererad"
    )
    price_on_shipping = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Fraktpris inlagt"
    )
    gather_note_done = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Plocksedel klar"
    )
    delivery_note_done = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Följesedel klar"
    )
    counted = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Avräknad"
    )
    products_invoiceable = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Produkter fakturerbara"
    )
    products_invoiced = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Produkter fakturerade"
    )
    shipping_invoiceable = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Frakt fakturerbar"
    )
    shipping_invoiced = forms.ChoiceField(
        widget=forms.Select,
        choices=RADIO_COICES,
        label="Frakt fakturerad"
    )

    def __init__(self, *args, **kwargs):
        super(ListOrderEventForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

        self.fields['customer'].widget.attrs.update({
            'placeholder': 'Kund'})
        self.fields['start_date'].widget.attrs.update({
            'placeholder': 'Datum (från)'})
        self.fields['end_date'].widget.attrs.update({
            'placeholder': 'Datum (till)'})
