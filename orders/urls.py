from django.conf.urls import url
from orders import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),

    url(r'^order_info/(?P<order_id>[\w\-]+)/$',
        views.order_info, name='order_info'),
    url(r'^add_order/(?P<customer_id>[\w\-]+)/$',
        views.add_order, name='add_order'),
    url(r'^edit_order/(?P<order_id>[\w\-]+)/$',
        views.edit_order, name='edit_order'),
    url(r'^edit_order_event/(?P<order_event_id>[\w\-]+)/$',
        views.edit_order_event,
        name='edit_order_event'),

    url(r'^order_overview/(?P<order_id>[\w\-]+)/$',
        views.order_overview, name='order_overview'),

    url(r'^order_events/(?P<order_id>[\w\-]+)/$',
        views.order_events, name='order_events'),

    url(r'^edit_gather_note/(?P<gather_note_id>[\w\-]+)/$',
        views.edit_gather_note,
        name='edit_gather_note'),

    url(r'^gather_note_pdf/(?P<gather_note_id>[\w\-]+)/$',
        views.gather_note_pdf,
        name='gather_note_pdf'),

    url(r'^edit_delivery_note/(?P<delivery_note_id>[\w\-]+)/$',
        views.edit_delivery_note,
        name='edit_delivery_note'),

    url(r'^edit_return_receipt/(?P<return_receipt_id>[\w\-]+)/$',
        views.edit_return_receipt, name='edit_return_receipt'),

    url(r'^return_receipts_list/$', views.return_receipts_list,
        name='return_receipts_list'),

    url(r'^edit_counting_note/(?P<counting_note_id>[\w\-]+)/$',
        views.edit_counting_note,
        name='edit_counting_note'),

    url(r'^list_gather_notes/$',
        views.list_gather_notes, name="list_gather_notes"),
    url(r'^list_orders/$', views.list_orders, name="list_orders"),
    url(r'^list_counting_notes/$',
        views.list_counting_notes, name="list_counting_notes"),
    url(r'^search/$', views.search, name="search"),
    url(r'^list_order_events/$',
        views.list_order_events, name="list_order_events"),



    # api calls-------------------------------------------------


    url(r'^add_order_event/(?P<order_id>[\w\-]+)/(?P<event_type>[\w\-]+)/$',
        views.add_order_event, name='add_order_event'),

    url(r'^gather_note_gathered/(?P<gather_note_id>[\w\-]+)/$',
        views.gather_note_gathered, name='gather_note_gathered'),

    url(r'^add_gather_note/$', views.add_gather_note, name='add_gather_note'),

    url(r'^download_pdf/(?P<document_type>[\w\-]+)/(?P<document_id>[\w\-]+)/$',
        views.download_pdf, name='download_pdf'),

    url(r'^add_delivery_note/$',
        views.add_delivery_note, name="add_delivery_note"),
    url(r'^add_return_receipt/$',
        views.add_return_receipt, name="add_return_receipt"),
    url(r'^get_total_weight/$',
        views.get_total_weight, name="get_total_weight"),

    url(r'^is_base_default/$',
        views.is_base_default, name="is_base_default"),
    url(r'^is_price_default/$',
        views.is_price_default, name="is_price_default"),
    url(r'^is_discount_default/$',
        views.is_discount_default, name="is_discount_default"),
    url(r'^is_return_product_match/(?P<order_id>[\w\-]+)'
        + '/(?P<order_event_id>[\w\-]+)/$',
        views.is_return_product_match, name="is_return_product_match"),
    url(r'^filter_gather_notes/$',
        views.filter_gather_notes, name="filter_gather_notes"),
    url(r'^get_order_defaults/$',
        views.get_order_defaults, name="get_order_defaults"),
    url(r'^get_projects/$', views.get_projects, name="get_projects"),

]
