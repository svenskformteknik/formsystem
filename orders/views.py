from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q

from products.models import Product, PriceList

from customers.models import Customer
from invoices.models import Invoice
from orders.forms import ListOrderEventForm
from orders.models import Order, OrderEvent, \
    OrderEventHasProduct, Transport, GatherNote, GatherNoteProducts, \
    TransportType, DeliveryNote, DeliveryNoteProducts, ReturnReceipt, \
    CountingNote, CountingNoteProducts, ApiKey
from products.models import Account

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from orders.forms import OrderForm, \
    OrderEventForm, OehpForm, TransportForm, GatherNoteForm, \
    GatherNoteProductsForm, DeliveryNoteForm, DeliveryNoteProductsForm, \
    ReturnReceiptForm, CountingNoteForm, CountingNoteProductsForm

from django.forms import inlineformset_factory
from datetime import datetime
from weasyprint import HTML, CSS

from invoices.views import rented_out, products_equal
from invoices.models import InvoiceTriggerDate
from django.core import serializers
import json
import re
from products.views import update_inventory


# helper functions ----------------------------------------------------------

def check_oe_states(order_event_id):

    print('checking oe states')

    # get order event
    order_event = OrderEvent.objects.get(pk=order_event_id)

    # check if transport price is 1 or more
    if order_event.transport.price:
        # set price on shipping to true
        order_event.price_on_shipping = True
    else:
        order_event.price_on_shipping = False

    # check if transport type is egen
    if order_event.transport.transport_type.name == 'egen':
        # set price on shipping to true
        order_event.price_on_shipping = True

        if not order_event.shipping_invoiced:
            order_event.shipping_invoiceable = True
        else:
            order_event.shipping_invoiceable = False
    else:

        if not order_event.shipping_invoiced and order_event.price_on_shipping:
            order_event.shipping_invoiceable = True
        else:
            order_event.shipping_invoiceable = False

    # check if order event has rent date and set status
    if order_event.rent_start_date or order_event.rent_stop_date:
        order_event.has_rent_date = True
    else:
        order_event.has_rent_date = False

    # check if delivery event can be invoiceable
    if order_event.delivery_event:
        if order_event.has_rent_date and order_event.gather_note_done \
            and order_event.delivery_note_done \
                and not order_event.products_invoiced:
            order_event.products_invoiceable = True
        else:
            order_event.products_invoiceable = False

    # check if return event can be invoiceble
    elif order_event.return_event:
        if order_event.has_rent_date and order_event.counted \
                and not order_event.products_invoiced:
            order_event.products_invoiceable = True
        else:
            order_event.products_invoiceable = False

    # check if order event can be completed
    if order_event.products_invoiced and order_event.shipping_invoiced:
        order_event.ongoing = False

    order_event.save()


def oe_overview(order_id):
    order = Order.objects.get(pk=order_id)
    order_events = list(order.orderevent_set.all().order_by("rent_start_date"))
    order_event_data = []

    for order_event in order_events:
        oe_products = {"uthyrning": {},
                       "försäljning": {},
                       "vidareuthyrning": {},
                       "försäljning förbrukningsmaterial": {},
                       "ersättning": {}}

        products = list(order_event.ordereventhasproduct_set.all())

        for product in products:
            name = product.product
            if product.price_type == 'uthyrning':
                if name in oe_products["uthyrning"]:
                    oe_products["uthyrning"][name] += product.amount
                else:
                    oe_products["uthyrning"][name] = product.amount

            elif product.price_type == "försäljning":
                if name in oe_products["försäljning"]:
                    oe_products["försäljning"][name] += product.amount
                else:
                    oe_products["försäljning"][name] = product.amount

            elif product.price_type == "vidareuthyrning":
                if name in oe_products["vidareuthyrning"]:
                    oe_products["vidareuthyrning"][name] += product.amount
                else:
                    oe_products["vidareuthyrning"][name] = product.amount

            elif product.price_type == "försäljning förbrukningsmaterial":
                if name in oe_products["försäljning förbrukningsmaterial"]:
                    oe_products["försäljning förbrukningsmaterial"][name] +=\
                            product.amount
                else:
                    oe_products["försäljning förbrukningsmaterial"][name] =\
                            product.amount

            elif product.price_type == "ersättning":
                if name in oe_products["ersättning"]:
                    oe_products["ersättning"][name] += product.amount
                else:
                    oe_products["ersättning"][name] = product.amount

        data = {
            "rent_date": order_event.rent_start_date,
            "uthyrning": oe_products["uthyrning"],
            "försäljning": oe_products["försäljning"],
            "vidareuthyrning": oe_products["vidareuthyrning"],
            "försäljning förbrukningsmaterial":
            oe_products["försäljning förbrukningsmaterial"],
            "ersättning": oe_products["ersättning"]
        }

        if order_event.return_event:
            data.update({"type": "Retur"})
        elif order_event.delivery_event:
            data.update({"type": "Leverans"})

        order_event_data.append(
            data
        )

    rented_products = []
    sold_products = []
    rent_forward_products = []
    sold_consumption_products = []
    replacement_products = []

    for oe in order_event_data:
        for product in oe["uthyrning"]:
            if product in rented_products:
                pass
            else:
                rented_products.append(product)

        for product in oe["försäljning"]:
            if product in sold_products:
                pass
            else:
                sold_products.append(product)

        for product in oe["vidareuthyrning"]:
            if product in rent_forward_products:
                pass
            else:
                rent_forward_products.append(product)

        for product in oe["försäljning förbrukningsmaterial"]:
            if product in sold_consumption_products:
                pass
            else:
                sold_consumption_products.append(product)

        for product in oe["ersättning"]:
            if product in replacement_products:
                pass
            else:
                replacement_products.append(product)

    rented_products.sort()
    rented_table = []

    table_headers = [oe["type"] for oe in order_event_data]
    table_headers.insert(0, "")

    table_dates = [oe["rent_date"] for oe in order_event_data]
    table_dates.insert(0, "")

    for product in rented_products:
        row = []
        row.append(product)
        for oe in order_event_data:
            if product in oe["uthyrning"] and oe["type"] == "Leverans":
                row.append(oe["uthyrning"][product])
            elif product in oe["uthyrning"] and oe["type"] == "Retur":
                row.append(-1 * oe["uthyrning"][product])
            else:
                row.append("")

        rented_table.append(row)

    sold_table = []
    sold_products.sort()

    for product in sold_products:
        row = []
        row.append(product)
        for oe in order_event_data:
            if product in oe["försäljning"]:
                row.append(oe["försäljning"][product])
            else:
                row.append("")

        sold_table.append(row)

    rent_forward_table = []
    rent_forward_products.sort()

    for product in rent_forward_products:
        row = []
        row.append(product)
        for oe in order_event_data:
            if product in oe["vidareuthyrning"] and oe["type"] == "Leverans":
                row.append(oe["vidareuthyrning"][product])
            elif product in oe["vidareuthyrning"] and oe["type"] == "Retur":
                row.append(-1 * oe["vidareuthyrning"][product])
            else:
                row.append("")

        rent_forward_table.append(row)

    sold_consumption_table = []
    sold_consumption_products.sort()

    for product in sold_consumption_products:
        row = []
        row.append(product)
        for oe in order_event_data:
            if product in oe["försäljning förbrukningsmaterial"]:
                row.append(oe["försäljning förbrukningsmaterial"][product])
            else:
                row.append("")

        sold_consumption_table.append(row)

    replacement_table = []
    replacement_products.sort()

    for product in replacement_products:
        row = []
        row.append(product)
        for oe in order_event_data:
            if product in oe["ersättning"]:
                row.append(oe["ersättning"][product])
            else:
                row.append("")

        replacement_table.append(row)

    # calculate all products out now
    renting_now, warnings = rented_out(order)

    return rented_table, table_headers, table_dates,\
        sold_table, rent_forward_table, sold_consumption_table,\
        replacement_table, renting_now, warnings


def update_base_fields(order_event):

    oeps = order_event.ordereventhasproduct_set.all()
    base_total_rent = 0
    base_total_forward = 0
    for product in oeps:
        if product.price_type == "uthyrning":
            base_total_rent += round(product.amount*product.base, 2)
        elif product.price_type == "vidareuthyrning":
            base_total_forward += round(product.amount*product.base, 2)

    order_event.base_total_rent = base_total_rent
    order_event.base_total_forward = base_total_forward
    order_event.save()


def get_product_changes(old_list, new_list, gather_note=False,
                        order_event=False, counting_note=False):
    '''
    takes two arguments
    '''

    def find_from_id(item_id, item_list):
        '''
        takes an objects id and returns the first object it
        finds with that id from a list
        '''
        for item in item_list:
            if item.id == item_id:
                return item

    # gets id of every object in list
    p_before = set([product.id for product in old_list])
    p_after = set([product.id for product in new_list])

    # finds ids of products that should be removed, added or changed
    remove_ids = p_before.difference(p_after)
    add_ids = p_after.difference(p_before)
    same_ids = p_before.intersection(p_after)

    # gets product objects that should be removed
    remove_products_list = []
    for i in remove_ids:
        for prod in old_list:
            if prod.id == i:
                remove_products_list.append(prod)

    # gets product objects that should be added
    add_products_list = []
    for i in add_ids:
        for prod in new_list:
            if prod.id == i:
                add_products_list.append(prod)

    # gets product objects that should be changed
    change_products_list = []
    for i in same_ids:
        old_item = find_from_id(i, old_list)
        new_item = find_from_id(i, new_list)
        if gather_note:
            # get field names from model object
            fields = GatherNoteProducts._meta.get_fields()
        elif order_event:
            fields = OrderEventHasProduct._meta.get_fields()
        elif counting_note:
            fields = CountingNoteProducts._meta.get_fields()
        else:
            fields = []

        # if any value has changed add to product change list
        for field in fields:
            # get object attributes with
            # object.serializable_value(name_of_attribute)
            if not old_item.serializable_value(
                    str(field.attname)) == new_item.serializable_value(
                            str(field.attname)):
                # append old (what should be changed) and new (changed to)
                change_products_list.append(new_item)
                break

    return remove_products_list, add_products_list, change_products_list


def sync_oe_to_gn(order_event, remove_products_list,
                  add_products_list, change_products_list):

    def get_gnp_from_oe_pid(oep_id, gnps):
        for product in gnps:
            if product.oep_id == oep_id:
                return product

    gn = order_event.gather_note
    gnps = gn.gathernoteproducts_set.all()
    for product in remove_products_list:
        gnp = get_gnp_from_oe_pid(product.id, gnps)
        if gnp:
            gnp.delete()

    for product in add_products_list:
        gnp = GatherNoteProducts()
        gnp.gather_note = gn
        gnp.oep_id = product.id
        gnp.amount = product.amount
        gnp.product = product.product
        gnp.price_type = product.price_type
        gnp.position = "lagervägen"
        gnp.save()

    for product in change_products_list:
        gnp = get_gnp_from_oe_pid(product.id, gnps)
        if gnp:
            gnp.oe_pid = product.id
            gnp.amount = product.amount
            gnp.product = product.product
            gnp.price_type = product.price_type
            gnp.save()


def sync_gn_to_oe(order_event, remove_products_list,
                  add_products_list, change_products_list):

    def get_oep_from_oep_id(oep_id, gnps):
        for product in oeps:
            if product.id == oep_id:
                return product

    def set_price_row(oe, oep):
        '''
            takes an order event, order event product and a note product
            and sets order event product field based on note product.
        '''
        std_price_list = oe.order.order_price_list
        product = Product.objects.get(name=oep.product)
        unit = product.unit
        price = product.price_set.get(pricelist__name=std_price_list)
        oep.base = round(price.base*unit, 2)
        if oep.price_type == "uthyrning":
            oep.price = round(price.rent*unit, 2)
        elif oep.price_type == "vidareuthyrning":
            oep.price = round(price.rent*unit, 2)
        elif oep.price_type == "försäljning":
            oep.price = round(price.selling*unit, 2)
        elif oep.price_type == "försäljning förbrukningsmaterial":
            oep.price = round(price.selling*unit, 2)

        oep.discount = oe.order.order_discount.discount
        discount_factor = 1-(oep.discount/100)
        oep.total_price = round(oep.price*oep.amount*unit*discount_factor, 2)
        oep.total_price_per_month = round(
                oep.price*oep.amount*unit*discount_factor*30, 2)
        oep.account = Account.objects.get(
                account_type=oep.price_type).account_nr

    oe = order_event
    oeps = oe.ordereventhasproduct_set.all()
    for product in remove_products_list:
        oep = get_oep_from_oep_id(product.oep_id, oeps)
        print(oep)
        if oep:
            oep.delete()

    for product in add_products_list:
        oep = OrderEventHasProduct()
        oep.order_event = oe
        oep.product = product.product
        oep.amount = product.amount
        oep.price_type = product.price_type
        oep.price_list = oe.order.order_price_list
        set_price_row(oe, oep)
        oep.save()
        product.oep_id = oep.id
        product.save()

    for product in change_products_list:
        oep = get_oep_from_oep_id(product.oep_id, oeps)
        print(oep)
        if oep:
            oep.product = product.product
            oep.amount = product.amount
            oep.price_type = product.price_type
            oep.price_list = oe.order.order_price_list
            set_price_row(oe, oep)
            oep.save()


def sync_cn_to_oe(order_event, remove_products_list,
                  add_products_list, change_products_list):

    def get_oep_from_oep_id(oep_id, gnps):
        for product in oeps:
            if product.id == oep_id:
                return product

    def set_price_row(oe, oep):
        '''
            takes an order event, order event product and a note product
            and sets order event product field based on note product.
        '''
        std_price_list = oe.order.order_price_list
        product = Product.objects.get(name=oep.product)
        unit = product.unit
        price = product.price_set.get(pricelist__name=std_price_list)
        oep.base = round(price.base*unit, 2)
        if oep.price_type == "uthyrning":
            oep.price = round(price.rent*unit, 2)
        elif oep.price_type == "vidareuthyrning":
            oep.price = round(price.rent*unit, 2)
        elif oep.price_type == "ersättning":
            oep.price = round(price.selling*unit, 2)

        oep.discount = oe.order.order_discount.discount
        discount_factor = 1-(oep.discount/100)
        oep.total_price = round(oep.price*oep.amount*unit*discount_factor, 2)
        oep.total_price_per_month = round(
                oep.price*oep.amount*unit*discount_factor*30, 2)
        oep.account = Account.objects.get(
                account_type=oep.price_type).account_nr

    oe = order_event
    oeps = oe.ordereventhasproduct_set.all()
    for product in remove_products_list:
        oep = get_oep_from_oep_id(product.oep_id, oeps)
        print(oep)
        if oep:
            oep.delete()

    for product in add_products_list:
        oep = OrderEventHasProduct()
        oep.order_event = oe
        oep.product = product.product
        oep.amount = product.amount
        oep.price_type = product.price_type
        oep.price_list = oe.order.order_price_list
        set_price_row(oe, oep)
        oep.save()
        product.oep_id = oep.id
        product.save()

    for product in change_products_list:
        oep = get_oep_from_oep_id(product.oep_id, oeps)
        print(oep)
        if oep:
            oep.product = product.product
            oep.amount = product.amount
            oep.price_type = product.price_type
            oep.price_list = oe.order.order_price_list
            set_price_row(oe, oep)
            oep.save()
        else:
            oep = OrderEventHasProduct()
            oep.order_event = oe
            oep.product = product.product
            oep.amount = product.amount
            oep.price_type = product.price_type
            oep.price_list = oe.order.order_price_list
            set_price_row(oe, oep)
            oep.save()
            product.oep_id = oep.id
            product.save()


def sync_oe_to_cn(order_event, remove_products_list,
                  add_products_list, change_products_list):

    def get_cnp_from_oe_pid(oep_id, cnps):
        for product in cnps:
            if product.oep_id == oep_id:
                return product

    cn = order_event.counting_note
    cnps = cn.countingnoteproducts_set.all()

    for product in remove_products_list:
        cnp = get_cnp_from_oe_pid(product.id, cnps)
        if cnp:
            cnp.delete()

    for product in add_products_list:
        cnp = CountingNoteProducts()
        cnp.counting_note = cn
        cnp.oep_id = product.id
        cnp.amount = product.amount
        cnp.bad_amount = 0
        cnp.product = product.product
        cnp.price_type = product.price_type
        cnp.position = "lagervägen"
        cnp.save()

    for product in change_products_list:
        cnp = get_cnp_from_oe_pid(product.id, cnps)
        if cnp:
            cnp.oe_pid = product.id
            cnp.amount = product.amount
            cnp.product = product.product
            cnp.price_type = product.price_type
            cnp.save()


# fortnox api functions ------------------------------------------------------


def get_api_keys():

    # get api keys from database
    fortnox_api = ApiKey.objects.filter(name="fortnox").first()
    TOKEN = fortnox_api.access_token
    SECRET = fortnox_api.client_secret

    # return keys
    return TOKEN, SECRET


# Create your views here -----------------------------------------------------

@login_required
def index(request):

    context_dict = {}

    return render(request, 'index.html', context=context_dict)


@login_required
def user_logout(request):

    # logout the user
    logout(request)

    return HttpResponseRedirect(reverse('index'))


def user_login(request):

    # create context dictionary
    context_dict = {}

    # check if request is a POST object (user posted login credentials)
    if request.method == 'POST':

        # collect the usenrame and password
        username = request.POST.get('username')
        password = request.POST.get('password')

        # check if user and password is correct
        user = authenticate(username=username, password=password)

        # if user exists and put in right credentials
        if user:

            # check if user is active
            if user.is_active:

                # login user and return to index page
                login(request, user)
                return HttpResponseRedirect(reverse('index'))

            # if user is not active tell user that his/her account
            # has been suspended
            else:
                return HttpResponse("Ditt konto har avaktiverats")

        # if user put in wrong credentials tell user
        else:
            return HttpResponse("Fel inlogningsuppgifter")

    # if request is not a POST object send user to login page
    else:
        return render(request, 'login.html', context_dict)


def search(request):

    q = request.GET.get("q")

    customer_results = \
        Customer.objects.filter(Q(name__icontains=q) |
                                Q(orgnr__icontains=q) |
                                Q(fortnox_id__icontains=q) |
                                Q(tel1__icontains=q) |
                                Q(tel2__icontains=q) |
                                Q(email__icontains=q) |
                                Q(visiting_address__icontains=q) |
                                Q(visiting_address__icontains=q)
                                )

    order_results = Order.objects.filter(Q(customer__name__icontains=q) |
                                         Q(project__icontains=q) |
                                         Q(work_address__icontains=q)
                                         )

    return render(request, "search_results.html", {
        "customer_results": customer_results,
        "order_results": order_results}
                  )


def order_info(request, order_id):

    # get order from url order_id
    order = Order.objects.filter(id=order_id).first()
    order_events = order.orderevent_set.all().order_by(
            "created_date").reverse()
    trigger_dates = InvoiceTriggerDate.objects.filter(order=order)
    last_trigger_date = trigger_dates.last()

    context_dict = {'order': order,
                    'navbar': 'order_info',
                    'customer_id': order.customer.id,
                    'last_trigger_date': last_trigger_date,
                    'order_events': order_events}

    return render(request, 'order_info.html', context_dict)


def order_events(request, order_id):

    # get order from url order_id
    order = Order.objects.filter(id=order_id).first()
    order_events = order.orderevent_set.all().order_by(
            "created_date").reverse()

    # set some state variables
    start_event = False
    end_event = False
    start_event_id = None
    end_event_id = None

    # filter out start event that has products invoiceable
    start_event_trigger = order.orderevent_set.filter(
            products_invoiceable=True) & order.orderevent_set.filter(
                    start_event=True)
    start_event_trigger = start_event_trigger.first()

    # filter out end event that has products invoiceable
    end_event_trigger = order.orderevent_set.filter(
            products_invoiceable=True) & order.orderevent_set.filter(
                    end_event=True)
    end_event_trigger = end_event_trigger.first()

    # if we have start event trigger set start event variabe to true
    # this will be sent to page and if True show button to create
    # start invice
    if start_event_trigger:
        start_event = True
        start_event_id = start_event_trigger.id

    # if we have end event trigger set end event variabe to true
    # this will be sent to page and if True show button to create
    # end invice
    if end_event_trigger:
        end_event = True
        end_event_id = end_event_trigger.id

    context_dict = {'order': order,
                    'navbar': 'order_events',
                    'customer_id': order.customer.id,
                    'start_event': start_event,
                    'end_event': end_event,
                    'start_event_id': start_event_id,
                    'end_event_id': end_event_id,
                    'order_events': order_events}

    return render(request, 'order_events.html', context_dict)


def add_order(request, customer_id):

    # if request method is POST
    if request.method == 'POST':

        # create order form
        form = OrderForm(request.POST)

        # check if form is valid
        if form.is_valid():

            # if form is valid save form to database and store an
            # instance of the obejct in variable instance
            instance = form.save()
            instance.ongoing = True
            instance.save()

            # redirect to order information page passing order_id to the view
            return HttpResponseRedirect(
                reverse('order_info', kwargs={'order_id': instance.id}))

        else:
            print(form.errors)
    else:

        # if request method is GET
        # create customer object using customer_id from url
        customer = Customer.objects.filter(pk=customer_id).first()

        # create form and put initial data for customer field
        form = OrderForm(initial={'customer': customer})

    return render(request, 'add_order.html', {'form': form})


def edit_order(request, order_id):

    # get order to edit
    order = Order.objects.get(pk=order_id)

    if request.method == 'POST':

        form = OrderForm(request.POST, instance=order)

        if form.is_valid():

            form.save()

            # redirect to order information page passing order_id to the view
            return HttpResponseRedirect(
                reverse(
                    'order_info',
                    kwargs={'order_id': order_id}
                )
            )

    else:

        form = OrderForm(instance=order)

    return render(request, 'edit_order.html', {'form': form,
                                               'order': order})


def edit_order_event(request, order_event_id):

    # create inline formset classes
    OehpInlineFormSet = inlineformset_factory(
        OrderEvent,
        OrderEventHasProduct,
        form=OehpForm,
        extra=0
    )

    TransportInlineFormSet = inlineformset_factory(
        OrderEvent,
        Transport,
        form=TransportForm,
        extra=1
    )

    # get order_event object from order_event id
    order_event = OrderEvent.objects.get(pk=order_event_id)
    oeps_old = order_event.ordereventhasproduct_set.all()
    oeps_old = [p for p in oeps_old]

    # get order id from order_event object
    order_id = order_event.order.id

    # if the request method is POST (data sent)
    if request.method == 'POST':

        # create order_event form and populate it with data from POST
        event_form = OrderEventForm(request.POST, instance=order_event)

        # create oehp formset and populate it with data from POST
        oehp_formset = OehpInlineFormSet(
            request.POST,
            instance=order_event
        )

        # create transport formset
        transport_formset = TransportInlineFormSet(
            request.POST,
            instance=order_event
        )

        if not order_event.gather_note_done and \
                event_form.data["gather_note_done"] == "True":
            print("changed to gn_done")
            changed_to_gn_done = True
        else:
            changed_to_gn_done = False

        if order_event.gather_note_done and \
                event_form.data["gather_note_done"] == "False":
            print("changed to gn_not_done")
            changed_to_gn_not_done = True
        else:
            changed_to_gn_not_done = False

        # check if forms are valid.
        # if not valid send forms back to page with errors
        if event_form.is_valid() and oehp_formset.is_valid() \
                and transport_formset.is_valid():

            # save data to database and store order_event object in
            # variable instance and use it to get the order_event
            # object just saved
            instance = event_form.save(commit=True)

            # set rent dates to same (did this becuse do not want to
            # change database structuer.
            order_event = OrderEvent.objects.get(pk=instance.pk)
            if order_event.rent_start_date:
                order_event.rent_stop_date = order_event.rent_start_date
            elif order_event.rent_stop_date:
                order_event.rent_start_date = order_event.rent_stop_date
            order_event.save()

            # create another set of inline formset with instance
            # of the order_event and populate it with POST data
            oehp_formset = OehpInlineFormSet(
                request.POST,
                instance=order_event
            )

            transport_formset = TransportInlineFormSet(
                request.POST,
                instance=order_event
            )

            # check agian if formset is correct.
            if oehp_formset.is_valid() and transport_formset.is_valid():

                # save formset.
                oehp_formset.save()
                transport_formset.save()

                if changed_to_gn_done:
                    order_event = OrderEvent.objects.get(pk=order_event_id)
                    prod_to_change = \
                        order_event.ordereventhasproduct_set.filter(
                            price_type="försäljning")
                    for product in prod_to_change:
                        model_product = Product.objects.get(
                                name=product.product)
                        model_product.total_nr -= product.amount
                        model_product.save()

                if changed_to_gn_not_done:
                    order_event = OrderEvent.objects.get(pk=order_event_id)
                    prod_to_change = \
                        order_event.ordereventhasproduct_set.filter(
                            price_type="försäljning")
                    for product in prod_to_change:
                        model_product = Product.objects.get(
                                name=product.product)
                        model_product.total_nr += product.amount
                        model_product.save()

                # sync products between order event and gahter note
                if order_event.delivery_event:
                    order_event = OrderEvent.objects.get(pk=order_event_id)
                    oeps_new = order_event.ordereventhasproduct_set.all()
                    oeps_new = [p for p in oeps_new]
                    remove, add, change = get_product_changes(
                                oeps_old, oeps_new, order_event=True)
                    sync_oe_to_gn(order_event, remove, add, change)

#                if order_event.return_event and order_event.counting_note:
#                    if order_event.counting_note.returnreceipt.project ==\
#                            order_event.order.project:
#                       order_event = OrderEvent.objects.get(pk=order_event_id)
#                        oeps_new = order_event.ordereventhasproduct_set.all()
#                        oeps_new = [p for p in oeps_new]
#                        remove, add, change = get_product_changes(
#                                    oeps_old, oeps_new, order_event=True)
#                        sync_oe_to_cn(order_event, remove, add, change)

                check_oe_states(order_event_id)

                # redirect to order info view
                if request.POST.getlist("save_and_edit"):
                    return HttpResponseRedirect("")
                else:
                    return HttpResponseRedirect(
                        reverse('order_events', kwargs={'order_id': order_id}))
        else:
            print(event_form.errors)
            print(oehp_formset.errors)
            print(transport_formset.errors)

    # if request method is GET
    else:

        # create order_event form
        event_form = OrderEventForm(instance=order_event)

        # create formsets with instance of the order_event
        oehp_formset = OehpInlineFormSet(instance=order_event)

        transport_formset = TransportInlineFormSet(instance=order_event)

    check_oe_states(order_event_id)

    # render page
    return render(request, 'edit_order_event.html',
                  {'event_form': event_form,
                   'oehp_formset': oehp_formset,
                   'transport_formset': transport_formset,
                   'order_event_id': order_event_id,
                   'order_event': order_event}
                  )


def order_overview(request, order_id):

    order = Order.objects.get(pk=order_id)
    # get order overview as tables from order overview function
    rented_table, table_headers, table_dates,\
        sold_table, rent_forward_table, sold_consumption_table,\
        replacement_table, renting_now, warnings = oe_overview(order_id)

    context_dict = {"order": order,
                    "navbar": "overview",
                    "table_headers": table_headers,
                    "table_dates": table_dates,
                    "rented_table": rented_table,
                    "sold_table": sold_table,
                    "rent_forward_table": rent_forward_table,
                    "sold_consumption_table": sold_consumption_table,
                    "replacement_table": replacement_table,
                    "renting_now": renting_now,
                    "warnings": warnings}

    return render(request, "order_overview.html", context_dict)


def edit_gather_note(request, gather_note_id):

    # create inline formset classes
    GnpInlineFormSet = inlineformset_factory(
        GatherNote,
        GatherNoteProducts,
        form=GatherNoteProductsForm,
        extra=0
    )

    gather_note = GatherNote.objects.get(pk=gather_note_id)
    gnps_old = gather_note.gathernoteproducts_set.all()
    gnps_old = [p for p in gnps_old]

    # CHECK THIS ERROR
    if request.method == "POST":

        gather_note_form = GatherNoteForm(request.POST, instance=gather_note)

        # create inline formsets and populate it with data from POST
        gnp_formset = GnpInlineFormSet(
            request.POST,
            instance=gather_note
        )

        if not gather_note.gathered and \
                gather_note_form.data["gathered"] == "True":
            print("changed to gathered")
            changed_to_gathered = True
        else:
            changed_to_gathered = False

        if gather_note.gathered and gather_note_form.data["ongoing"] == "True":
            print("changed to ongoing")
            changed_to_ongoing = True
        else:
            changed_to_ongoing = False

        if gather_note_form.is_valid() and gnp_formset.is_valid():
            gather_note_form.save()
            gnp_formset.save()

            gather_note = GatherNote.objects.get(pk=gather_note_id)

            gnp_formset = GnpInlineFormSet(instance=gather_note)

            # sync products between order event and gahter note
            order_event = OrderEvent.objects.get(
                    gather_note__id=gather_note.id)
            gnps_new = gather_note.gathernoteproducts_set.all()
            gnps_old = [p for p in gnps_old]
            remove, add, change = get_product_changes(
                        gnps_old, gnps_new, gather_note=True)
            sync_gn_to_oe(order_event, remove, add, change)
            update_base_fields(order_event)

            if changed_to_gathered:
                products_list = gather_note.gathernoteproducts_set.all()
                errors = update_inventory(products_list,
                                          add=False,
                                          gather_note=True)

                if errors:
                    gather_note.gathered = False
                    gather_note.ongoing = True
                    gather_note.save()

            elif changed_to_ongoing:
                products_list = gather_note.gathernoteproducts_set.all()
                errors = update_inventory(products_list,
                                          add=True,
                                          gather_note=True)

                if errors:
                    gather_note.gathered = True
                    gather_note.ongoing = False
                    gather_note.save()
            else:
                errors = []

        else:
            print(gnp_formset.errors)
            print(gather_note_form.errors)
            errors = []

    else:
        gather_note_form = GatherNoteForm(instance=gather_note)

        # create formsets and query an empty objects
        gnp_formset = GnpInlineFormSet(instance=gather_note)

        errors = None

    return render(request, 'edit_gather_note.html',
                  {'gnp_formset': gnp_formset,
                   'gather_note_form': gather_note_form,
                   'gather_note': gather_note,
                   'document_type': "gather_note",
                   'customer': gather_note.orderevent.order.customer.name,
                   'project': gather_note.orderevent.order.project,
                   'order_id': gather_note.orderevent.order.id,
                   'work_address': gather_note.orderevent.order.work_address,
                   'work_address2': gather_note.orderevent.order.work_address2,
                   'delivery_date': gather_note.orderevent.transport.time,
                   'transport_type':
                   gather_note.orderevent.transport.transport_type.name,
                   'person_ordering':
                   gather_note.orderevent.order.person_ordering,
                   'po_tel': gather_note.orderevent.order.po_tel,
                   'errors': errors})


def gather_note_pdf(request, gather_note_id):

    # get gather note from gather note id
    gather_note = GatherNote.objects.get(pk=gather_note_id)
    # get products that belong to gater note
    products = gather_note.gathernoteproducts_set.all()

    # collect information into variables
    customer = gather_note.orderevent.order.customer.name
    project = gather_note.orderevent.order.project
    work_address = gather_note.orderevent.order.work_address
    work_address2 = gather_note.orderevent.order.work_address2
    work_postnr = gather_note.orderevent.order.work_postnr
    work_place = gather_note.orderevent.order.work_place
    delivery_time = gather_note.orderevent.transport.time
    transport = gather_note.orderevent.transport.transport_type.name
    weight = gather_note.orderevent.transport.total_weight
    person_ordering = gather_note.orderevent.order.person_ordering
    po_tel = gather_note.orderevent.order.po_tel
    comments = gather_note.comments

    # add variables to conext dict
    context_dict = {"gather_note": gather_note,
                    "products": products,
                    "customer": customer,
                    "project": project,
                    "work_address": work_address,
                    "work_addrss2": work_address2,
                    "work_postnr": work_postnr,
                    "work_place": work_place,
                    "delivery_time": delivery_time,
                    "transport": transport,
                    "weight": weight,
                    "person_ordering": person_ordering,
                    "po_tel": po_tel,
                    "comments": comments,
                    }

    return render(request, "gather_note_pdf_template.html", context_dict)


def edit_delivery_note(request, delivery_note_id):

    # create inline formset class fo delivery note
    DnpInlineFormset = inlineformset_factory(
        DeliveryNote,
        DeliveryNoteProducts,
        form=DeliveryNoteProductsForm,
        extra=0
    )

    delivery_note = DeliveryNote.objects.get(pk=delivery_note_id)

    if request.POST:

        print(request.POST.getlist("update"))
        print(request.POST.getlist("save"))
        if request.POST.getlist("update"):
            dnps = delivery_note.deliverynoteproducts_set.all()
            for product in dnps:
                product.delete()

            order_event = OrderEvent.objects.get(delivery_note=delivery_note)
            oeps = order_event.ordereventhasproduct_set.all()
            for product in oeps:
                dnp = DeliveryNoteProducts()
                dnp.delivery_note = delivery_note
                dnp.amount = product.amount
                dnp.product = product.product
                dnp.save()

            return HttpResponseRedirect("")

        else:

            # create forms
            delivery_note_form = DeliveryNoteForm(
                request.POST, instance=delivery_note)

            dnp_formset = DnpInlineFormset(
                request.POST, instance=delivery_note)

            # check forms
            if delivery_note_form.is_valid() and dnp_formset.is_valid():

                instance = delivery_note_form.save(commit=True)
                delivery_note = DeliveryNote.objects.get(pk=instance.pk)

                dnp_formset = DnpInlineFormset(
                        request.POST, instance=delivery_note)

                if dnp_formset.is_valid():
                    dnp_formset.save()

    else:
        delivery_note_form = DeliveryNoteForm(instance=delivery_note)

        dnp_formset = DnpInlineFormset(instance=delivery_note)

    return render(
            request,
            'edit_delivery_note.html',
            {'dnp_formset': dnp_formset,
             'delivery_note_form': delivery_note_form,
             'delivery_note': delivery_note,
             "document_type": "delivery_note",
             'customer': delivery_note.orderevent.order.customer.name,
             'project': delivery_note.orderevent.order.project,
             'order_id': delivery_note.orderevent.order.id,
             'work_address': delivery_note.orderevent.order.work_address,
             'work_address2': delivery_note.orderevent.order.work_address2,
             'transport_type':
             delivery_note.orderevent.transport.transport_type.name,
             'person_ordering':
             delivery_note.orderevent.order.person_ordering,
             'po_tel': delivery_note.orderevent.order.po_tel,
             'delivery_date': delivery_note.orderevent.transport.time})


def delivery_note_pdf(request, delivery_note_id):

    # get gather note from gather note id
    delivery_note = DeliveryNote.objects.get(pk=delivery_note_id)
    # get products that belong to gater note
    products = delivery_note.deliverynoteproducts_set.all()

    # collect information into variables
    order_id = delivery_note.orderevent.order.id
    customer = delivery_note.orderevent.order.customer.name
    project = delivery_note.orderevent.order.project
    work_address = delivery_note.orderevent.order.work_address
    work_address2 = delivery_note.orderevent.order.work_address2
    work_postnr = delivery_note.orderevent.order.work_postnr
    work_place = delivery_note.orderevent.order.work_place
    delivery_date = delivery_note.orderevent.transport.time
    other_info = delivery_note.other_info
    transport_type = delivery_note.orderevent.transport.transport_type.name
    person_ordering = delivery_note.orderevent.order.person_ordering
    po_tel = delivery_note.orderevent.order.po_tel

    # add variables to conext dict
    context_dict = {"delivery_note": delivery_note,
                    "order_id": order_id,
                    "products": products,
                    "customer": customer,
                    "project": project,
                    "work_address": work_address,
                    "work_addrss2": work_address2,
                    "work_postnr": work_postnr,
                    "work_place": work_place,
                    "delivery_date": delivery_date,
                    "other_info": other_info,
                    "transport_type": transport_type,
                    "person_ordering": person_ordering,
                    "po_tel": po_tel,
                    }

    return render(request, "delivery_note_pdf_template.html", context_dict)


def edit_return_receipt(request, return_receipt_id):

    return_receipt = ReturnReceipt.objects.get(pk=return_receipt_id)

    if request.method == "POST":

        form = ReturnReceiptForm(request.POST, instance=return_receipt)

        if form.is_valid():
            form.save()

            customer = form.cleaned_data['customer']
            match = re.search("(?:id:)(.*)", customer)
            fortnox_id = match.group(1)
            project = form.cleaned_data['project']
            counting_note = form.cleaned_data['counting_note']
            products = counting_note.countingnoteproducts_set.all()
            if not products:
                print("no products")
                customer = Customer.objects.get(fortnox_id=fortnox_id)
                order = customer.order_set.get(project=project)
                products_out, warnings = rented_out(order)
                print(products_out)
                for product in products_out:
                    cn_product = CountingNoteProducts()
                    cn_product.counting_note = counting_note
                    cn_product.amount_out = product.amount
                    cn_product.amount = 0
                    cn_product.price_type = product.price_type
                    cn_product.product = product.product
                    cn_product.position = "lagervägen"
                    cn_product.save()

            else:
                print("has products")
            return HttpResponseRedirect(reverse("return_receipts_list"))

        else:
            print(form.errors)

    else:
        form = ReturnReceiptForm(instance=return_receipt)

    return render(request, 'edit_return_receipt.html',
                  {'form': form,
                   "document_type": "return_receipt",
                   'return_receipt_id': return_receipt_id})


def return_receipt_pdf(request, return_receipt_id):

    return_receipt = ReturnReceipt.objects.get(pk=return_receipt_id)

    # collect information into variables
    date_and_time = return_receipt.datetime
    customer = return_receipt.customer
    project = return_receipt.project
    info = return_receipt.info

    # add variables to conext dict
    context_dict = {"return_receipt": return_receipt,
                    "date_and_time": date_and_time,
                    "info": info,
                    "customer": customer,
                    "project": project,
                    }

    return render(request, "return_receipt_pdf_template.html", context_dict)


def return_receipts_list(request):

    # get all customer obejcts
    all_return_receipts = ReturnReceipt.objects.all().order_by(
        "datetime").reverse()

    # paginate all customer objects into different pages
    paginator = Paginator(all_return_receipts, 100)

    # fetch page 1
    page = request.GET.get('page', 1)

    # try to open page
    try:
        return_receipts = paginator.page(page)
    except PageNotAnInteger:
        return_receipts = paginator.page(1)  # if not number try open page 1
    except EmptyPage:
        return_receipts = paginator.page(paginator.num_pages)

    return render(request, 'return_receipts_list.html',
                  {'return_receipts': return_receipts})


def edit_counting_note(request, counting_note_id):

    CnpInlinFormSet = inlineformset_factory(
        CountingNote,
        CountingNoteProducts,
        form=CountingNoteProductsForm,
        extra=0
    )

    counting_note = CountingNote.objects.get(pk=counting_note_id)
    cnps_old = counting_note.countingnoteproducts_set.all()
    cnps_old = [p for p in cnps_old]
    return_receipt_info = counting_note.returnreceipt.info

    if request.method == "POST":

        counting_note_form = CountingNoteForm(
            request.POST, instance=counting_note)

        cnp_formset = CnpInlinFormSet(
            request.POST,
            instance=counting_note
        )

        if not counting_note.counted and \
                counting_note_form.data["counted"] == "True":
            print("changed to counted")
            changed_to_counted = True
        else:
            changed_to_counted = False

        if counting_note.counted and \
                counting_note_form.data["ongoing"] == "True":
            print("changed to ongoing")
            changed_to_ongoing = True
        else:
            changed_to_ongoing = False

        if counting_note_form.is_valid() and cnp_formset.is_valid():

            counting_note_form.save()
            cnp_formset.save()

            counting_note = CountingNote.objects.get(pk=counting_note_id)
            cnp_formset = CnpInlinFormSet(instance=counting_note)

            if counting_note.ongoing:
                cnps_new = counting_note.countingnoteproducts_set.all()
                cnps_new = [p for p in cnps_new]
                try:
                    order_event = counting_note.orderevent_set.get(
                            order__project=counting_note.returnreceipt.project)
                    remove, add, change = get_product_changes(
                            cnps_old, cnps_new, counting_note=True)
                    sync_cn_to_oe(order_event, remove, add, change)
                except:
                    # write exception redirect here
                    pass

            if changed_to_counted:
                products_list = counting_note.countingnoteproducts_set.all()
                errors = update_inventory(products_list,
                                          add=True,
                                          counting_note=True)

                if errors:
                    counting_note.counted = False
                    counting_note.ongoing = True
                    counting_note.save()

            elif changed_to_ongoing:
                products_list = counting_note.countingnoteproducts_set.all()
                errors = update_inventory(products_list,
                                          add=False,
                                          counting_note=True)

                if errors:
                    counting_note.counted = True
                    counting_note.ongoing = False
                    counting_note.save()
            else:
                errors = []
        else:
            print(counting_note_form.errors)
            print(cnp_formset.errors)
            errors = []

    else:
        counting_note_form = CountingNoteForm(instance=counting_note)

        cnp_formset = CnpInlinFormSet(instance=counting_note)

        errors = None

    order_events = counting_note.orderevent_set.all()

    return render(request, 'edit_counting_note.html',
                  {'counting_note_form': counting_note_form,
                   "document_type": 'counting_note',
                   "order_events": order_events,
                   'cnp_formset': cnp_formset,
                   'counting_note': counting_note,
                   "return_receipt_info": return_receipt_info,
                   'errors': errors})


def counting_note_pdf(request, counting_note_id):

    # get gather note from gather note id
    counting_note = CountingNote.objects.get(pk=counting_note_id)
    # get products that belong to gater note
    products = counting_note.countingnoteproducts_set.all()

    # collect information into variables
    order_events = counting_note.orderevent_set.all()
    return_receipt_info = counting_note.returnreceipt.info
    comment = counting_note.comment
    customers = []
    projects = []
    addresses = []
    for order_event in order_events:
        customers.append(order_event.order.customer.name)
        projects.append(order_event.order.project)
        addresses.append({
                        "work_adresss": order_event.order.work_address,
                        "work_adress2": order_event.order.work_address2,
                        "work_postnr": order_event.order.work_postnr,
                        "work_place": order_event.order.work_place,
                        })

    delivered = counting_note.datetime

    # add variables to conext dict
    context_dict = {"counting_note": counting_note,
                    "return_receipt_info": return_receipt_info,
                    "comment": comment,
                    "products": products,
                    "customers": customers,
                    "projects": projects,
                    "addresses": addresses,
                    "delivered": delivered,
                    }

    return render(request, "counting_note_pdf_template.html", context_dict)


def list_gather_notes(request):

    if request.method == "POST":

        ongoing = request.POST.getlist('ongoing')
        gathered = request.POST.getlist("gathered")

        if ongoing and not gathered:
            gather_notes = GatherNote.objects.filter(ongoing=True) & \
                    GatherNote.objects.filter(gathered=False)
        elif not ongoing and gathered:
            gather_notes = GatherNote.objects.filter(ongoing=False) & \
                    GatherNote.objects.filter(gathered=True)
        elif ongoing and gathered:
            gather_notes = GatherNote.objects.filter(ongoing=True) & \
                    GatherNote.objects.filter(gathered=True)
        elif not ongoing and not gathered:
            gather_notes = GatherNote.objects.filter(ongoing=False) & \
                    GatherNote.objects.filter(gathered=False)

    else:

        gather_notes = GatherNote.objects.all()

    gather_notes = gather_notes.order_by("orderevent__transport__time")

    no_oe_gather_notes = GatherNote.objects.filter(orderevent=None)

    return render(request, "list_gather_notes.html", {
        "no_oe_gather_notes": no_oe_gather_notes,
        "gather_notes": gather_notes})


def list_orders(request):

    # if request method is post
    if request.method == "POST":

        # get filter options
        ongoing = request.POST.getlist('ongoing')
        waiting = request.POST.getlist('waiting')
        incomplete = request.POST.getlist('incomplete')

        # filter orders based on posted filter options
        if ongoing and waiting and incomplete:
            orders = Order.objects.filter(ongoing=True) & \
                Order.objects.filter(waiting=True) & \
                Order.objects.filter(incomplete_order_event=True)
        elif ongoing and waiting and not incomplete:
            orders = Order.objects.filter(ongoing=True) & \
                Order.objects.filter(waiting=True) & \
                Order.objects.filter(incomplete_order_event=False)
        elif ongoing and not waiting and incomplete:
            orders = Order.objects.filter(ongoing=True) & \
                Order.objects.filter(waiting=False) & \
                Order.objects.filter(incomplete_order_event=True)
        elif ongoing and not waiting and not incomplete:
            orders = Order.objects.filter(ongoing=True) & \
                Order.objects.filter(waiting=False) & \
                Order.objects.filter(incomplete_order_event=False)
        elif not ongoing and waiting and incomplete:
            orders = Order.objects.filter(ongoing=False) & \
                Order.objects.filter(waiting=True) & \
                Order.objects.filter(incomplete_order_event=True)
        elif not ongoing and waiting and not incomplete:
            orders = Order.objects.filter(ongoing=False) & \
                Order.objects.filter(waiting=True) & \
                Order.objects.filter(incomplete_order_event=False)
        elif not ongoing and not waiting and incomplete:
            orders = Order.objects.filter(ongoing=False) & \
                Order.objects.filter(waiting=False) & \
                Order.objects.filter(incomplete_order_event=True)
        elif not ongoing and not waiting and not incomplete:
            orders = Order.objects.filter(ongoing=False) & \
                Order.objects.filter(waiting=False) & \
                Order.objects.filter(incomplete_order_event=False)

    else:

        # if not request method not post get all order objects
        orders = Order.objects.all()

    # sort objects by project
    orders = orders.order_by("project")

    return render(request, "list_orders.html", {"orders": orders})


def list_counting_notes(request):

    if request.method == "POST":

        ongoing = request.POST.getlist('ongoing')
        counted = request.POST.getlist('counted')

        if ongoing and counted:
            counting_notes = CountingNote.objects.filter(ongoing=True) & \
                CountingNote.objects.filter(counted=True)
        elif ongoing and not counted:
            counting_notes = CountingNote.objects.filter(ongoing=True) & \
                CountingNote.objects.filter(counted=False)
        elif not ongoing and counted:
            counting_notes = CountingNote.objects.filter(ongoing=False) & \
                CountingNote.objects.filter(counted=True)
        elif not ongoing and not counted:
            counting_notes = CountingNote.objects.filter(ongoing=False) & \
                CountingNote.objects.filter(counted=False)

    else:

        counting_notes = CountingNote.objects.all()

    counting_notes = counting_notes.order_by("datetime")

    no_oe_counting_notes = CountingNote.objects.filter(orderevent=None)

    return render(request, "list_counting_notes.html",
                  {"counting_notes": counting_notes,
                   "no_oe_counting_notes": no_oe_counting_notes})


def list_order_events(request):

    order_events = OrderEvent.objects.all()

    if request.method == "POST":
        print("testing")

        form = ListOrderEventForm(request.POST)

        if form.is_valid():
            customer = form.cleaned_data['customer']
            customer = Customer.objects.filter(name=customer)
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            delivery_event = form.cleaned_data['delivery_event']
            return_event = form.cleaned_data['return_event']
            ongoing = form.cleaned_data['ongoing']
            completed = form.cleaned_data['completed']
            has_rent_date = form.cleaned_data['has_rent_date']
            shipped = form.cleaned_data['shipped']
            price_on_shipping = form.cleaned_data['price_on_shipping']
            gather_note_done = form.cleaned_data['gather_note_done']
            delivery_note_done = form.cleaned_data['delivery_note_done']
            counted = form.cleaned_data['counted']
            products_invoiceable = form.cleaned_data['products_invoiceable']
            products_invoiced = form.cleaned_data['products_invoiced']
            shipping_invoiceable = form.cleaned_data['shipping_invoiceable']
            shipping_invoiced = form.cleaned_data['shipping_invoiced']
            print(delivery_event)
            if customer:
                order_events = OrderEvent.objects.filter(
                        order__customer=customer).order_by("created_date")
            else:
                order_events = OrderEvent.objects.all().order_by(
                        "created_date")

            if start_date and end_date:
                order_events = order_events.filter(
                        created_date__range=(start_date, end_date))

            if delivery_event == 'yes':
                order_events = order_events.filter(delivery_event=True)
            elif delivery_event == 'no':
                order_events = order_events.filter(delivery_event=False)

            if return_event == 'yes':
                order_events = order_events.filter(return_event=True)
            elif return_event == 'no':
                order_events = order_events.filter(return_event=False)

            if ongoing == 'yes':
                order_events = order_events.filter(ongoing=True)
            elif ongoing == 'no':
                order_events = order_events.filter(ongoing=False)

            if completed == 'yes':
                order_events = order_events.filter(completed=True)
            elif completed == 'no':
                order_events = order_events.filter(completed=False)

            if has_rent_date == 'yes':
                order_events = order_events.filter(has_rent_date=True)
            elif has_rent_date == 'no':
                order_events = order_events.filter(has_rent_date=False)

            if shipped == 'yes':
                order_events = order_events.filter(shipped=True)
            elif shipped == 'no':
                order_events = order_events.filter(shipped=False)

            if price_on_shipping == 'yes':
                order_events = order_events.filter(price_on_shipping=True)
            elif price_on_shipping == 'no':
                order_events = order_events.filter(price_on_shipping=False)

            if gather_note_done == 'yes':
                order_events = order_events.filter(gather_note_done=True)
            elif gather_note_done == 'no':
                order_events = order_events.filter(gather_note_done=False)

            if delivery_note_done == 'yes':
                order_events = order_events.filter(delivery_note_done=True)
            elif delivery_note_done == 'no':
                order_events = order_events.filter(delivery_note_done=False)

            if counted == 'yes':
                order_events = order_events.filter(counted=True)
            elif counted == 'no':
                order_events = order_events.filter(counted=False)

            if products_invoiceable == 'yes':
                order_events = order_events.filter(products_invoiceable=True)
            elif products_invoiceable == 'no':
                order_events = order_events.filter(products_invoiceable=False)

            if products_invoiced == 'yes':
                order_events = order_events.filter(products_invoiced=True)
            elif products_invoiced == 'no':
                order_events = order_events.filter(products_invoiced=False)

            if shipping_invoiceable == 'yes':
                order_events = order_events.filter(shipping_invoiceable=True)
            elif shipping_invoiceable == 'no':
                order_events = order_events.filter(shipping_invoiceable=False)

            if shipping_invoiced == 'yes':
                order_events = order_events.filter(shipping_invoiced=True)
            elif shipping_invoiced == 'no':
                order_events = order_events.filter(shipping_invoiced=False)

        else:
            print(form.errors)

    else:
        form = ListOrderEventForm()

    context_dict = {
                    "order_events": order_events,
                    "form": form,
                    }

    return render(request, "list_order_events.html", context_dict)


#######################################
# API calls
#######################################


def add_order_event(request, order_id, event_type):

    # get order from id
    order = Order.objects.get(pk=order_id)

    # create new order event
    order_event = OrderEvent()

    # set order event as start event if it is the first order event
    # for the order
    if not order.orderevent_set.all():
        order_event.start_event = True

    # link order with order event and set created time as now
    order_event.order = order
    order_event.created_date = datetime.now()

    # check which type of event it is and flag it as such
    if event_type == "delivery":
        order_event.delivery_event = True
        gather_note = GatherNote()
        gather_note.gathered = False
        gather_note.ongoing = True
        gather_note.save()
        order_event.gather_note = gather_note
    elif event_type == "return":
        order_event.return_event = True

    # flag order event as ongoing
    order_event.ongoing = True
    order_event.save()
    transport_type = TransportType.objects.filter(name="lillebil").first()
    transport = Transport()
    transport.order_event = order_event
    transport.transport_type = transport_type
    transport.save()

    order.save()

    return HttpResponse(1, content_type='application/json')


def gather_note_gathered(request, gather_note_id):

    # get gather note from id
    gather_note = GatherNote.objects.get(pk=gather_note_id)
    # flag it as gathered and unflag it as ongoing
    gather_note.gathered = True
    gather_note.ongoing = False
    gather_note.save()

    return HttpResponse(1, content_type='application/json')


def add_gather_note(request):

    order_event_id = request.POST["order_event_id"]
    order_event = OrderEvent.objects.get(pk=order_event_id)
    # generate new gather note
    new_gn = GatherNote()
    # set status to not gatered and ongoing
    new_gn.gathered = False
    new_gn.ongoing = True

    new_gn.save()
    order_event.gather_note = new_gn

    oeps = order_event.ordereventhasproduct_set.all()
    for oep in oeps:
        gnp = GatherNoteProducts()
        gnp.gather_note = new_gn
        gnp.oep_id = oep.id
        gnp.amount = oep.amount
        gnp.product = oep.product
        gnp.price_type = oep.price_type
        gnp.position = "lagervägen"
        gnp.save()

    order_event.save()

    return HttpResponse(new_gn.pk, content_type='application/json')


def download_pdf(request, document_type, document_id):

    # check wich document type to print
    if document_type == "gather_note":
        page = gather_note_pdf(request, document_id)

    # place holders - should be implemented later ---------
    elif document_type == "counting_note":
        page = counting_note_pdf(request, document_id)
    elif document_type == "delivery_note":
        page = delivery_note_pdf(request, document_id)
    elif document_type == "return_receipt":
        page = return_receipt_pdf(request, document_id)

    # get css info from file in static folder
    css = CSS(filename='static/css/print.css')

    # generate pdf file with weasyprint library
    pdf_file = HTML(string=page.content).write_pdf(stylesheets=[css])

    # send repsonse object as pdf
    response = HttpResponse(pdf_file, content_type='application/pdf')
    return response


def add_delivery_note(request):

    # get products, amount sd order vent id from POST
    products = request.POST.getlist("products[]")
    amounts = request.POST.getlist("amounts[]")
    order_event_id = request.POST.getlist("order_event_id")
    order_event_id = int(order_event_id[0])

    # get order event from order event id
    order_event = OrderEvent.objects.get(pk=order_event_id)

    # create a new delivery note and save to database
    new_delivery_note = DeliveryNote()
    new_delivery_note.save()

    # add order event to delivery note
    new_delivery_note.orderevent = order_event

    # add delivery note to order event and save to datbase
    order_event.delivery_note = new_delivery_note
    order_event.save()

    # loop through products and amounts
    for product, amount in zip(products, amounts):

        # create delivery note product and link
        # product and amount to delivery note
        dn_product = DeliveryNoteProducts()
        dn_product.delivery_note = new_delivery_note
        dn_product.amount = amount
        dn_product.product = product
        dn_product.save()

    return HttpResponse(new_delivery_note.pk, content_type='application/json')


def add_return_receipt(request):

    # create counting note
    counting_note = CountingNote()

    # set time stamp as now
    counting_note.datetime = datetime.now()
    # set as ongoing and not counted

    counting_note.ongoing = True
    counting_note.counted = False
    counting_note.save()
    # create return receipt and link it to counting note
    return_receipt = ReturnReceipt()
    return_receipt.counting_note = counting_note
    return_receipt.datetime = counting_note.datetime
    return_receipt.save()

    return HttpResponse(1, content_type='application/json')


def get_total_weight(request):

    products = request.POST.getlist("products[]")
    amounts = request.POST.getlist("amounts[]")

    total_weight = 0
    for product, amount in zip(products, amounts):
        prod = Product.objects.filter(name=product).first()
        total_weight += float(amount)*prod.weight

    return HttpResponse(total_weight, content_type='application/json')


def is_base_default(request):

    base = request.POST["base"]
    product = request.POST["product"]
    price_list = request.POST["price_list"]

    product = Product.objects.filter(name=product).first()
    unit = product.unit
    price_list = PriceList.objects.filter(name=price_list)
    product_prices = product.price_set.filter(pricelist=price_list).first()

    if round(product_prices.base*unit, 2) == float(base):
        base_is_default = 1
    else:
        base_is_default = 0

    return HttpResponse(base_is_default, content_type='application/json')


def is_price_default(request):

    price = request.POST["price"]
    product = request.POST["product"]
    price_list = request.POST["price_list"]
    price_type = request.POST["price_type"]

    product = Product.objects.filter(name=product).first()
    price_list = PriceList.objects.filter(name=price_list)
    product_prices = product.price_set.filter(pricelist=price_list).first()

    if price_type == "uthyrning":
        stored_price = round(product_prices.rent*product.unit, 2)
    elif price_type == "vidareuthyrning":
        stored_price = round(product_prices.rent*product.unit, 2)
    elif price_type == "försäljning":
        stored_price = round(product_prices.selling*product.unit, 2)
    elif price_type == "ersättning":
        stored_price = round(product_prices.selling*product.unit, 2)

    print(float(price))
    print(stored_price)

    if stored_price == float(price):
        price_is_default = 1
    else:
        price_is_default = 0

    return HttpResponse(price_is_default, content_type='application/json')


def is_discount_default(request):

    discount = request.POST["discount"]
    order_event_id = request.POST["order_event_id"]
    invoice_id = request.POST["invoice_id"]

    if invoice_id == "None":
        order_event = OrderEvent.objects.get(pk=order_event_id)
        order = order_event.order
        order_discount = order.order_discount.discount
    else:
        invoice = Invoice.objects.get(pk=invoice_id)
        order_discount = invoice.order.order_discount.discount

    if order_discount == float(discount):
        discount_is_default = 1
    else:
        discount_is_default = 0

    return HttpResponse(discount_is_default, content_type='application/json')


def is_return_product_match(request, order_id, order_event_id):

    product = request.POST["product"]
    price = request.POST["price"]
    base = request.POST["base"]
    discount = request.POST["discount"]
    price_type = request.POST["price_type"]

    returned_product = OrderEventHasProduct()
    returned_product.product = product
    returned_product.price_type = price_type
    returned_product.price = float(price)
    returned_product.base = float(base)
    returned_product.discount = float(discount)

    order = Order.objects.get(pk=order_id)
    latest_order_event = OrderEvent.objects.get(pk=order_event_id)
    rented_products, warnings = rented_out(order, latest_order_event)

    product_is_equal = 0
    print(rented_products)
    for prod in rented_products:
        print(product_is_equal)
        if products_equal(prod, returned_product):
            product_is_equal = 1
            break
        print(product_is_equal)

    return HttpResponse(product_is_equal, content_type='application/json')


def filter_gather_notes(request):

    ongoing = request.POST['ongoing']
    gathered = request.POST['gathered']

    print(ongoing)
    print(gathered)

    if ongoing == "true" and gathered == "false":
        gather_notes = GatherNote.objects.filter(ongoing=True)
    elif ongoing == "false" and gathered == "true":
        gather_notes = GatherNote.objects.filter(gathered=True)
    elif ongoing == "true" and gathered == "true":
        gather_notes = GatherNote.objects.filter(
                ongoing=True).filter(gathered=True)
    else:
        gather_notes = GatherNote.objects.all()

    gather_notes = serializers.serialize('json', gather_notes)

    return HttpResponse(gather_notes, content_type='application/json')


def get_order_defaults(request):

    order_event_id = request.POST["order_event_id"]
    invoice_id = request.POST["invoice_id"]

    if invoice_id == "None":
        order_event = OrderEvent.objects.get(pk=order_event_id)
        order = order_event.order
        discount = order.order_discount.discount
        std_price_list = order.order_price_list
        defaults = {"discount": discount, "std_price_list": std_price_list}

    else:
        invoice = Invoice.objects.get(pk=invoice_id)
        discount = invoice.order.order_discount.discount
        defaults = {"discount": discount}

    defaults = json.dumps(defaults)

    return HttpResponse(defaults, content_type='application/json')


def get_projects(request):

    fortnox_id = request.POST["fortnox_id"]
    customer = Customer.objects.get(fortnox_id=fortnox_id)
    orders = Order.objects.filter(customer=customer)
    projects = [order.project for order in orders]
    projects = json.dumps(projects)

    return HttpResponse(projects, content_type="application/json")
