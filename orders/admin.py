from django.contrib import admin
from orders.models import Order, OrderEvent, \
    OrderEventHasProduct, TransportType, Transport, \
    DeliveryNote, DeliveryNoteProducts, GatherNote, GatherNoteProducts, \
    CountingNote, CountingNoteProducts, ReturnReceipt, OrderDiscount, ApiKey


class EventProductInline(admin.TabularInline):
    model = OrderEventHasProduct
    extra = 1


class TransportTypeInline(admin.TabularInline):
    model = Transport


class DeliveryNoteInline(admin.TabularInline):
    model = DeliveryNote
    extra = 1


class DeliveryNoteProductsInline(admin.TabularInline):
    model = DeliveryNoteProducts
    extra = 1


class GatherNoteInline(admin.TabularInline):
    model = GatherNote
    extra = 1


class GatherNoteProductInline(admin.TabularInline):
    model = GatherNoteProducts
    extra = 1


class CountingNoteProductInline(admin.TabularInline):
    model = CountingNoteProducts
    extra = 1


class CountingNoteAdmin(admin.ModelAdmin):
    inlines = [
        CountingNoteProductInline
    ]


class GatherNoteAdmin(admin.ModelAdmin):
    inlines = [
        GatherNoteProductInline
    ]


class DeliveryNoteAdmin(admin.ModelAdmin):
    inlines = [
        DeliveryNoteProductsInline
    ]


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'project')


class CountingNoteInline(admin.TabularInline):
    model = CountingNote
    extra = 1


class CoutingNoteAdmin(admin.ModelAdmin):
    inlines = [
        CountingNoteInline
    ]


class OrderEventAdmin(admin.ModelAdmin):
    list_display = ("id", "delivery_event", "return_event",
                    "order", "ongoing", "shipped")

    list_filter = ("delivery_event", "return_event", "order", "ongoing",
                   "shipped")

    search_fields = [
            "id",
            "order__id",
            ]

    inlines = [
        EventProductInline,
        TransportTypeInline,
    ]


# Register your models here.
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderEvent, OrderEventAdmin)
admin.site.register(OrderEventHasProduct)
admin.site.register(TransportType)
admin.site.register(Transport)
admin.site.register(DeliveryNote, DeliveryNoteAdmin)
admin.site.register(DeliveryNoteProducts)
admin.site.register(GatherNote, GatherNoteAdmin)
admin.site.register(GatherNoteProducts)
admin.site.register(CountingNote, CountingNoteAdmin)
admin.site.register(CountingNoteProducts)
admin.site.register(ReturnReceipt)
admin.site.register(OrderDiscount)
admin.site.register(ApiKey)
