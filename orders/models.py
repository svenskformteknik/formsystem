from django.db import models

from customers.models import Customer

# Create your models here.


# Api keys --------------------------------------------------

class ApiKey(models.Model):
    name = models.CharField(max_length=128, unique=True)
    access_token = models.CharField(max_length=512)
    client_secret = models.CharField(max_length=512)

    def __str__(self):
        return self.name


# order group -------------------------------------------

class OrderDiscount(models.Model):
    discount = models.FloatField()

    def __str__(self):
        return str(self.discount)


class Order(models.Model):
    customer = models.ForeignKey(Customer, blank=True, null=True)
    project = models.CharField(max_length=128)
    work_address = models.CharField(max_length=128, blank=True, null=True)
    work_address2 = models.CharField(max_length=128, blank=True, null=True)
    work_postnr = models.CharField(max_length=12, blank=True, null=True)
    work_place = models.CharField(max_length=128, blank=True, null=True)
    additional_info = models.TextField(max_length=1024, blank=True, null=True)
    person_ordering = models.CharField(max_length=128)
    po_tel = models.CharField(max_length=20, blank=True, null=True)
    our_referent = models.CharField(max_length=128)
    order_discount = models.ForeignKey(OrderDiscount, default=1)
    order_price_list = models.CharField(max_length=128)
    starting_time = models.DateField(blank=True, null=True)
    completion_time = models.DateField(blank=True, null=True)
    invoiced_to = models.DateField(blank=True, null=True)
    incomplete_order_event = models.BooleanField()
    waiting = models.BooleanField()
    waiting_reason = models.TextField(max_length=1024, blank=True, null=True)
    ongoing = models.BooleanField()
    completed = models.BooleanField()

    def __str__(self):
        return str(self.id)


class DeliveryNote(models.Model):
    other_info = models.TextField(max_length=1024, blank=True, null=True)
    # goods_description = models.TextField(max_length=1024)
    sign = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return str(self.id)


class DeliveryNoteProducts(models.Model):
    delivery_note = models.ForeignKey(DeliveryNote)
    amount = models.IntegerField()
    product = models.CharField(max_length=128)

    def __str__(self):
        return self.product


class GatherNote(models.Model):
    comments = models.TextField(max_length=1024, blank=True, null=True)
    gathered = models.BooleanField()
    ongoing = models.BooleanField()

    def __str__(self):
        return str(self.id)


class GatherNoteProducts(models.Model):
    gather_note = models.ForeignKey(GatherNote)
    oep_id = models.IntegerField(blank=True, null=True)
    price_type = models.CharField(max_length=128, blank=True, null=True)
    amount = models.IntegerField()
    product = models.CharField(max_length=128)
    rent_forward = models.BooleanField(default=False)
    position = models.CharField(max_length=128, blank=True, null=True)
    comment = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return str(self.product)


class CountingNote(models.Model):
    comment = models.TextField(max_length=1024, blank=True, null=True)
    datetime = models.DateTimeField()
    sign = models.CharField(max_length=128, blank=True, null=True)
    ongoing = models.BooleanField()
    counted = models.BooleanField()

    def __str__(self):
        return str(self.id)


class CountingNoteProducts(models.Model):

    EXCEPTION_CHOICES = (
        ('inget', 'Inget'),
        ('kass', 'Kass'),
    )

    counting_note = models.ForeignKey(CountingNote)
    oep_id = models.IntegerField(blank=True, null=True)
    amount_out = models.IntegerField(default=0, blank=True, null=True)
    amount = models.IntegerField()
    bad_amount = models.IntegerField(default=0)
    product = models.CharField(max_length=128)
    price_type = models.CharField(max_length=128, blank=True, null=True)
    rent_forward = models.BooleanField(default=False)
    position = models.CharField(max_length=128, blank=True, null=True)

    exception = models.CharField(max_length=128,
                                 choices=EXCEPTION_CHOICES,
                                 default='inget')

    comment = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return str(self.product)


class ReturnReceipt(models.Model):
    counting_note = models.OneToOneField(CountingNote)
    datetime = models.DateTimeField()
    customer = models.CharField(max_length=128, blank=True, null=True)
    project = models.CharField(max_length=128, blank=True, null=True)
    info = models.TextField(max_length=1024, blank=True, null=True)

    def __str__(self):
        return str(self.id)


class OrderEvent(models.Model):
    order = models.ForeignKey(Order)
    gather_note = models.OneToOneField(GatherNote, blank=True, null=True)
    delivery_note = models.OneToOneField(DeliveryNote, blank=True, null=True)
    counting_note = models.ForeignKey(CountingNote, blank=True, null=True)
    info = models.TextField(max_length=1024, blank=True, null=True)
    created_date = models.DateTimeField()
    rent_start_date = models.DateField(blank=True, null=True)
    rent_stop_date = models.DateField(blank=True, null=True)

    delivery_event = models.BooleanField(default=False)
    return_event = models.BooleanField(default=False)
    rest_event = models.BooleanField(default=False)
    start_event = models.BooleanField(default=False)
    end_event = models.BooleanField(default=False)

    base_total_rent = models.FloatField(default=0)
    base_total_forward = models.FloatField(default=0)

    has_rent_date = models.BooleanField(default=False)
    ongoing = models.BooleanField(default=False)
    shipped = models.BooleanField(default=False)
    price_on_shipping = models.BooleanField(default=False)
    gather_note_done = models.BooleanField(default=False)
    delivery_note_done = models.BooleanField(default=False)
    products_invoiceable = models.BooleanField(default=False)
    products_invoiced = models.BooleanField(default=False)
    shipping_invoiceable = models.BooleanField(default=False)
    shipping_invoiced = models.BooleanField(default=False)
    counted = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


class OrderEventHasProduct(models.Model):
    order_event = models.ForeignKey(OrderEvent)
    product = models.CharField(max_length=128)
    price_list = models.CharField(max_length=128)
    price_type = models.CharField(max_length=128)
    return_is_match = models.BooleanField(default=True)
    amount = models.IntegerField(default=0)
    base = models.FloatField(default=0)
    base_is_default = models.BooleanField(default=True)
    price = models.FloatField(default=0)
    price_is_default = models.BooleanField(default=True)
    discount = models.FloatField(default=0)
    discount_is_default = models.BooleanField(default=True)
    total_price = models.FloatField(default=0)
    total_price_per_month = models.FloatField(default=0)
    account = models.CharField(max_length=4)

    def __str__(self):
        return str(self.product)


class TransportType(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Transport(models.Model):
    transport_type = models.ForeignKey(TransportType)
    order_event = models.OneToOneField(OrderEvent)
    time = models.DateTimeField(blank=True, null=True)
    total_weight = models.FloatField(blank=True, null=True)
    goods_description = models.TextField(
            max_length=1024, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)

    def __str__(self):
        return str(self.transport_type)
