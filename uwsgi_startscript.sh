#!/bin/bash

echo REMOVING PID FILES
rm celerybeat.pid

echo STARTING SUPERVISOR
supervisord -c /etc/supervisor/conf.d/formsys-celery-worker.conf &
supervisord -c /etc/supervisor/conf.d/formsys-celery-beat.conf &
echo STARTING UWSGI
uwsgi /etc/uwsgi/sites/djangoapp.ini 
