from django.forms import ModelForm
from customers.models import Customer, Referent

class CustomerForm(ModelForm):

    # this function sets custom class on form element
    # to render correct with bootstrap 4
    def __init__(self, *args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-sm'

    class Meta:
        model = Customer
        fields = '__all__'
        help_texts = {
            'name': 'Group to which this message belongs to',
        }


class ReferentForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(ReferentForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-sm'

    class Meta:
        model = Referent
        fields = "__all__"