from django.db import models


# Create your models here.
class Customer(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)
    fortnox_id = models.CharField(max_length=128, unique=True)
    orgnr = models.CharField(max_length=30, blank=True, null=True)
    active = models.BooleanField()
    customer_type = models.CharField(max_length=128, blank=True, null=True)
    tel1 = models.CharField(max_length=20, blank=True, null=True)
    tel2 = models.CharField(max_length=20, blank=True, null=True)
    fax = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=128, blank=True, null=True)
    web_address = models.CharField(max_length=128, blank=True, null=True)
    visiting_address = models.CharField(max_length=128, blank=True, null=True)
    visiting_postnr = models.CharField(max_length=12, blank=True, null=True)
    visiting_place = models.CharField(max_length=128, blank=True, null=True)
    visiting_country = models.CharField(max_length=128, blank=True, null=True)
    payment_terms = models.CharField(max_length=128, blank=True, null=True)
    invoice_delivery = models.CharField(max_length=128, blank=True, null=True)
    invoice_address = models.CharField(max_length=128, blank=True, null=True)
    invoice_address2 = models.CharField(max_length=128, blank=True, null=True)
    invoice_postnr = models.CharField(max_length=12, blank=True, null=True)
    invoice_place = models.CharField(max_length=128, blank=True, null=True)
    invoice_country = models.CharField(max_length=128, blank=True, null=True)
    invoice_email = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return self.name


class Referent(models.Model):
    customer = models.ForeignKey(Customer)
    primary = models.BooleanField()
    surname = models.CharField(max_length=128)
    lastname = models.CharField(max_length=128)
    phone_nr = models.IntegerField()
    email = models.EmailField()

    def __str__(self):
        return self.surname + ' ' + self.lastname
