from django.conf.urls import url
from customers import views

urlpatterns = [

    url(r'^customer_list/$', views.customer_list, name='customer_list'),

    url(r'^customer_info/(?P<customer_id>[\w\-]+)/$', views.customer_info,
        name='customer_info'),

    url(r'^customer_orders/(?P<customer_id>[\w\-]+)/$', views.customer_orders,
        name='customer_orders'),

    url(r'^add_referent/(?P<customer_id>[\w\-]+)/$', views.add_referent,
        name='add_referent'),

    # api calls ------------------------------
    url(r'^sync_new_customers/$', views.sync_new_customers, name="sync_new_customers"),
    url(r'^sync_fnox_customer_info/(?P<fnox_id>[\w\-]+)/$',
        views.sync_fnox_customer_info, name="sync_fnox_customer_info"),

    url(r'^remove_referent/(?P<referent_id>[\w\-]+)/$', views.remove_referent,
        name="remove_referent"),
    url(r'^get_customers/$', views.get_customers, name="get_customers"),

]
