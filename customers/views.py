from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
import requests

from orders.views import get_api_keys
from orders.models import Order

from customers.models import Customer, Referent
from customers.forms import ReferentForm

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json


# ------------------------------
# fortnox calls
# ------------------------------


def request_page(page_nr):

        # get fortnox api keys
    TOKEN, SECRET = get_api_keys()

    try:
        r = requests.get(
            url="https://api.fortnox.se/3/customers/?page=" + str(page_nr),
            headers={
                "Access-Token": TOKEN,
                "Client-Secret": SECRET,
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
        )
        print('Response HTTP Status Code : {status_code}'.format(
            status_code=r.status_code))

        return r

    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')


def get_fnox_total_pages():

    # get fortnox api keys
    TOKEN, SECRET = get_api_keys()

    try:
        r = requests.get(
            url="https://api.fortnox.se/3/customers",
            headers={
                "Access-Token": TOKEN,
                "Client-Secret": SECRET,
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
        )
        print('Response HTTP Status Code : {status_code}'.format(
            status_code=r.status_code))

    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')

    response = r.json()
    total_pages = response['MetaInformation']['@TotalPages']

    return total_pages


def get_fnox_customer(customer_id):

    # get fortnox api keys
    TOKEN, SECRET = get_api_keys()

    # connect to fortnox and get one customer specified by fortnox customer id
    try:
        r = requests.get(
            url="https://api.fortnox.se/3/customers/" + str(customer_id),
            headers={
                "Access-Token": TOKEN,
                "Client-Secret": SECRET,
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
        )
        print(
            'Response HTTP Status Code : {status_code}'.format(
                status_code=r.status_code))

        return r

    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')


def get_fnox_customer_ids():
    # Customers (GET https://api.fortnox.se/3/customers)

    customer_ids = []
    total_pages = get_fnox_total_pages()
    page_nrs = range(1, total_pages + 1)
    for page_nr in page_nrs:
        page = request_page(page_nr)

        data = page.json()
        customers = data["Customers"]

        for customer in customers:
            customer_ids.append(customer["CustomerNumber"])

    return customer_ids


def get_db_customer_ids():
    db_customers = Customer.objects.all()
    db_customer_ids = [customer.fortnox_id for customer in db_customers]

    return db_customer_ids


def add_customer_to_db(fnox_id):
    if not Customer.objects.filter(fortnox_id=fnox_id).exists():
        r = get_fnox_customer(fnox_id)
        fnox_customer_data = r.json()
        fnox_customer_data = fnox_customer_data["Customer"]
        customer = Customer()
        customer.name = fnox_customer_data["Name"]
        customer.fortnox_id = fnox_customer_data["CustomerNumber"]
        customer.orgnr = fnox_customer_data["OrganisationNumber"]
        customer.active = fnox_customer_data["Active"]

        if customer == "PRIVATE":
            customer.customer_type = "Privat"
        else:
            customer.customer_type = "Företag"

        customer.tel1 = fnox_customer_data["Phone1"]
        customer.tel2 = fnox_customer_data["Phone2"]
        customer.fax = fnox_customer_data["Fax"]
        customer.email = fnox_customer_data["Email"]
        customer.web_address = fnox_customer_data["WWW"]
        customer.visiting_address = fnox_customer_data["VisitingAddress"]
        customer.visiting_postnr = fnox_customer_data["VisitingZipCode"]
        customer.visiting_place = fnox_customer_data["VisitingCity"]
        customer.visiting_country = fnox_customer_data["VisitingCountry"]
        customer.payment_terms = fnox_customer_data["TermsOfPayment"]

        if fnox_customer_data["DefaultDeliveryTypes"]["Invoice"] == "PRINT":
            customer.invoice_delivery = "Utskrift"
        else:
            customer.invoice_delivery = "E-post"

        customer.invoice_address = fnox_customer_data["Address1"]
        customer.invoice_address2 = fnox_customer_data["Address2"]
        customer.invoice_postnr = fnox_customer_data["ZipCode"]
        customer.invoice_place = fnox_customer_data["City"]
        customer.invoice_country = fnox_customer_data["Country"]
        customer.invoice_email = fnox_customer_data["EmailInvoice"]
        customer.save()
    else:
        print("Customer already exists in database")


def update_db_customer_info(fnox_id):
    if Customer.objects.filter(fortnox_id=fnox_id).exists():
        r = get_fnox_customer(fnox_id)
        fnox_customer_data = r.json()
        fnox_customer_data = fnox_customer_data["Customer"]
        customer = Customer.objects.get(fortnox_id=fnox_id)
        customer.name = fnox_customer_data["Name"]
        customer.fortnox_id = fnox_customer_data["CustomerNumber"]
        customer.orgnr = fnox_customer_data["OrganisationNumber"]
        customer.active = fnox_customer_data["Active"]

        if customer == "PRIVATE":
            customer.customer_type = "Privat"
        else:
            customer.customer_type = "Företag"

        customer.tel1 = fnox_customer_data["Phone1"]
        customer.tel2 = fnox_customer_data["Phone2"]
        customer.fax = fnox_customer_data["Fax"]
        customer.email = fnox_customer_data["Email"]
        customer.web_address = fnox_customer_data["WWW"]
        customer.visiting_address = fnox_customer_data["VisitingAddress"]
        customer.visiting_postnr = fnox_customer_data["VisitingZipCode"]
        customer.visiting_place = fnox_customer_data["VisitingCity"]
        customer.visiting_country = fnox_customer_data["VisitingCountry"]
        customer.payment_terms = fnox_customer_data["TermsOfPayment"]

        if fnox_customer_data["DefaultDeliveryTypes"]["Invoice"] == "PRINT":
            customer.invoice_delivery = "Utskrift"
        else:
            customer.invoice_delivery = "E-post"

        customer.invoice_address = fnox_customer_data["Address1"]
        customer.invoice_address2 = fnox_customer_data["Address2"]
        customer.invoice_postnr = fnox_customer_data["ZipCode"]
        customer.invoice_place = fnox_customer_data["City"]
        customer.invoice_country = fnox_customer_data["Country"]
        customer.invoice_email = fnox_customer_data["EmailInvoice"]
        customer.save()
    else:
        print("Customer do not exist in database")


# -----------------------------
# views
# -----------------------------


@login_required
def customer_list(request):

    # get all customer obejcts
    all_customers = Customer.objects.all()

    # paginate all customer objects into different pages
    paginator = Paginator(all_customers, 100)

    # fetch page 1
    page = request.GET.get('page', 1)

    # try to open page
    try:
        customers = paginator.page(page)
    except PageNotAnInteger:
        customers = paginator.page(1)  # if not a number try to open page 1
    except EmptyPage:
        customers = paginator.page(paginator.num_pages)

    context_dict = {'customers': customers}

    return render(request, 'customer_list.html', context_dict)


def customer_info(request, customer_id):

    customer = Customer.objects.filter(id=customer_id).first()

    # get all referents for customer
    referents = list(customer.referent_set.all())

    # loop over referent list and place primary referents at the
    # beginning of list
    for index, referent in enumerate(referents):
        if referent.primary:
            primary_referent = referents.pop(index)
            referents.insert(0, primary_referent)

    context_dict = {'customer': customer,
                    'navbar': 'info',
                    'referents': referents}

    return render(request, 'customer_info.html', context_dict)


def customer_orders(request, customer_id):

    # get customer from customer id
    customer = Customer.objects.filter(id=customer_id).first()

    # get customer from customer object
    orders = Order.objects.filter(customer_id=customer)

    # save customer data to context dict
    context_dict = {'customer': customer,
                    'orders': orders,
                    'navbar': 'order'}

    return render(request, 'customer_orders.html', context_dict)


def add_referent(request, customer_id):

    if request.method == "POST":

        form = ReferentForm(request.POST)

        if form.is_valid():

            form.save()

            # sends a javascript response to close the window
            return HttpResponse('<script type="text/javascript">'
                                'window.opener.location.reload(true);'
                                'window.close();'
                                '</script>')
        else:
            print(form.errors)
    else:

        form = ReferentForm(
                initial={"customer": customer_id, "primary": False})

    return render(request, "add_referent.html", {"form": form})


# -------------------------
# API calls
# -------------------------

def get_customers(request):

    # customers = Customer.objects.all()
    customers = Customer.objects.filter(active=True)
    customers = [customer.name + " id:" +
                 customer.fortnox_id for customer in customers]
    # customers = serializers.serialize('json', customers)
    customers = json.dumps(customers)
    return HttpResponse(customers, content_type="application/json")


def sync_new_customers(requests):

    TOKEN, SECRET = get_api_keys()

    fnox_ids = get_fnox_customer_ids()
    db_ids = get_db_customer_ids()

    diff = set(fnox_ids) - set(db_ids)

    if diff:
        print('adding customers with id')
        print(diff)
        for fnox_id in diff:
            add_customer_to_db(fnox_id)
    else:
        print("No new Customers to print")

    return HttpResponse(1, content_type='application/json')


def sync_fnox_customer_info(request, fnox_id):

    update_db_customer_info(fnox_id)

    return HttpResponse(1, content_type='application/json')


def remove_referent(request, referent_id):

    referent = Referent.objects.get(pk=referent_id)
    referent.delete()

    return HttpResponse(1, content_type='application/json')
