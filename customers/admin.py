from django.contrib import admin
from customers.models import Referent, Customer


# Register your models here.
class ReferentInline(admin.TabularInline):
    model = Referent
    extra = 1


class CustomerAdmin(admin.ModelAdmin):
    list_display = ("name", "fortnox_id", "active")
    search_fields = ["name", "fortnox_id", "orgnr", "tel1", "email"]
    inlines = [
        ReferentInline
    ]


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Referent)
