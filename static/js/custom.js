
var formsys = {

    helper: { 
        
        get_element_identity(id) {
        // takes an id string and uses regular expression to identify which field type the element has
        // and from which row

        //use regex o extract information
        var match = /\-(product|price_list|price_type|amount|base|price|discount|position|exception)/.exec(id);
        var match2 = /\d+/.exec(id)

        // save results from regex to variables
        var field = match[0]
        // remove dash in beginning of string
        field = field.slice(1, field.length)
        var row = match2[0]

        return {field: field, row: row}
        },
    },

    customers: {

        sync_new_customers: function() {

            // ajax call to get ew customers from fortnox
            $.ajax({
                beforeSend: function() {
                    // show spinning wheel (loading)
                    $("#loading_wheel").removeClass("d-none")
                },
                url: "/customers/sync_new_customers/",
                dataType: 'json',
                success: function () {
                    // hide spinning wheel (loading done)
                    $("#loading_wheel").addClass("d-none")
                    location.reload(true)
                }
                
            })
        },

        sync_fnox_customer_info: function() {
            fnox_id = $("#fnox_id").text()
            console.log(fnox_id)
            // ajax call to get customer info from fortnox
            $.ajax({
                url: "/customers/sync_fnox_customer_info/" + fnox_id + "/",
                dataType: 'json',
                success: function () {
                    location.reload(true)
                }
                
            })
        },

        add_referent: function(customer_id) {

            // open window to add referent to customer
            url = '/customers/add_referent/' + customer_id + '/'
            var win_props = 'height=800, width=1000, resizable=yes, scrollbars=yes';
            add_referent_win = open(url, 'add_referent_win', win_props);
        },

        
        remove_referent: function(referent_id) {
            
            // make dialoge box to confirm or deny action
            confirm_choice = confirm("Är du säker att du vill ta bort referenten?")

            // if action confrmed, send ajax call to remove referent
            if ( confirm_choice == true ) {

                $.ajax({
                    url: '/customers/remove_referent/' + referent_id,
                    dataType: 'json',
                    success: function() {
                        location.reload()
                    }
                })
            }
        }

    },

    order: {

        autocomplete_price_list: function() {
    
                // make ajax call to fetch price lists
                $.ajax({
                    url: "/products/get_all_price_lists/",
                    type: "GET",
                    dataType: "json",
                    success: function(price_lists) {
                        
                        // populate field with atocomplete functionality
                        $("#id_order_price_list").autocomplete({
                            minLength: 0,
                            source: price_lists
                        });
                    
                        // set options for autocomplete to display all products diectly on click
                        $("#id_order_price_list").autocomplete( "search", "" )
                    }
                });
    
        },

        mark_as_waiting: function() {
            $("#id_waiting").prop("checked", true)
            $("#id_ongoing").prop("checked", false)
            $("#id_completed").prop("checked", false)
            $("#id_waiting_reason").parent().parent().removeClass("d-none")
            $("#add_order_badge_row").empty()
            $("#add_order_badge_row").append('<span class="badge badge-warning">Avvakta</span>')
            
        },
        mark_as_completed: function() {
            $("#id_waiting").prop("checked", false)
            $("#id_completed").prop("checked", true)
            $("#id_ongoing").prop("checked", false)
            $("#id_waiting_reason").parent().parent().addClass("d-none")
            $("#add_order_badge_row").empty()
            $("#add_order_badge_row").append('<span class="badge badge-dark">Avslutad</span>')
        },
        mark_as_ongoing: function() {
            $("#id_waiting").prop("checked", false)
            $("#id_completed").prop("checked", false)
            $("#id_ongoing").prop("checked", true)
            $("#id_waiting_reason").parent().parent().addClass("d-none")
            $("#add_order_badge_row").empty()
            $("#add_order_badge_row").append('<span class="badge badge-success">Pågående</span>')
        }
    },

   gather_note: {
        
        add_product_row: function() {

            // get total number of forms from django management form and save to variable
            total_forms = $('#id_gathernoteproducts_set-TOTAL_FORMS').val()
            // increment total number of forms in the django management form
            total_forms = Number(total_forms) + 1
            // update total number of forms in django management form
            $('#id_gathernoteproducts_set-TOTAL_FORMS').val(total_forms)

            // creat html content to add

            var html_content = `
            <div class="row bg-light p-2 m-0">
                <div class="col-1">
                    <div class="form-group">
                        <input name="gathernoteproducts_set-${ total_forms - 1 }-amount" value="" id="id_gathernoteproducts_set-${ total_forms - 1 }-amount" class="form-control form-control-sm" type="text">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <input name="gathernoteproducts_set-${ total_forms - 1 }-product" value="" id="id_gathernoteproducts_set-${ total_forms - 1 }-product" maxlength="128" class="form-control form-control-sm" type="text">
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <input name="gathernoteproducts_set-${ total_forms - 1 }-price_type" id="id_gathernoteproducts_set-${ total_forms - 1 }-price_type" max_length="128" class="form-control form-control-sm" type="text">
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <input name="gathernoteproducts_set-${ total_forms - 1 }-position" value="lagervägen" id="id_gathernoteproducts_set-${ total_forms - 1 }-position" maxlength="128" class="form-control form-control-sm" type="text">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <input type="text" name="gathernoteproducts_set-${ total_forms - 1 }-comment" class="form-control form-control-sm" maxlength="128" id="id_gathernoteproducts_set-${ total_forms - 1 }-comment" />    
                    </div>
                </div>
                <div class="col d-none">
                    <div class="form-group">
                        <input name="gathernoteproducts_set-${ total_forms - 1 }-oep_id" value="" id="id_gathernoteproducts_set-${ total_forms - 1 }-oep_id" type="hidden">
                    </div>
                </div>
                <div class="col d-none">
                    <div class="form-group">
                        <input name="gathernoteproducts_set-${ total_forms - 1 }-id" value="" id="id_gathernoteproducts_set-${ total_forms - 1 }-id" type="hidden">
                    </div>
                </div>
                <div class="col d-none">
                    <div class="form-group">
                        <input name="gathernoteproducts_set-${ total_forms - 1 }-DELETE" id="id_gathernoteproducts_set-${ total_forms - 1 }-DELETE" class="form-check-input" type="checkbox">
                    </div>
                </div>
                <div class="col-1">
                    <div class="btn btn-sm btn-primary" name="${ total_forms - 1 }" id="remove_gn_product_btn">Ta bort</div>
                </div>
            </div>`

            // add html to the end of referent_contianer
            $('#gn_products_container').append(html_content);
        },

        remove_product_row: function(row) {

            // check delete checkbox and hide row
            $("#id_gathernoteproducts_set-" + row + "-DELETE").prop("checked", true)
            $("#id_gathernoteproducts_set-" + row + "-product").parent().parent().parent().addClass("d-none")
        },

        get_products_from_oe: function() {

           // get total forms from parent window (order event) and child window (gather note)
            parent_total_forms = window.opener.$("#id_ordereventhasproduct_set-TOTAL_FORMS").val()
            total_forms = $('#id_gathernoteproducts_set-TOTAL_FORMS').val()

            // loop over products in parent window, get their values and add them to child window.
            for (i=0; i < parent_total_forms; i++) {
                parent_product = window.opener.$("#id_ordereventhasproduct_set-" + i + "-product").val()
                parent_amount = window.opener.$("#id_ordereventhasproduct_set-" + i + "-amount").val()
                parent_rent_forward = window.opener.$("#id_ordereventhasproduct_set-" + i + "-rent_forward").prop("checked")
                if (parent_rent_forward == true) {
                    checked = "checked"
                }
                else {
                    checked = ""
                }
                
                // increment total number of forms in child window
                total_forms = Number(total_forms) + 1
                $('#id_gathernoteproducts_set-TOTAL_FORMS').val(total_forms)

                   var html_content = `
                    <div class="row bg-light p-2 m-0">
                        <div class="col-2">
                            <div class="form-group">
                                <input type="text" value="${parent_amount}"  name="gathernoteproducts_set-${ total_forms -1 }-amount" class="form-control form-control-sm" id="id_gathernoteproducts_set-${ total_forms -1 }-amount" />
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <input type="text" value="${parent_product}" name="gathernoteproducts_set-${ total_forms -1 }-product" maxlength="128" id="id_gathernoteproducts_set-${ total_forms -1 }-product" class="form-control form-control-sm" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input name="gathernoteproducts_set-${ total_forms -1 }-rent_forward" id="id_gathernoteproducts_set-${ total_forms -1 }-rent_forward" class="form-control form-control-sm" type="checkbox" ${ checked }>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <input name="gathernoteproducts_set-${ total_forms -1 }-position" value="lagervägen" class="form-control form-control-sm" id="id_gathernoteproducts_set-${ total_forms -1 }-position" maxlength="128" type="text">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <input type="text" name="gathernoteproducts_set-${ total_forms -1 }-comment" value="" class="form-control form-control-sm" maxlength="128" id="id_gathernoteproducts_set-${ total_forms -1 }-comment" />    
                            </div>
                        </div>
                        <div class="col d-none">
                            <div class="form-group">
                                <input name="gathernoteproducts_set-${ total_forms -1 }-id" value="" id="id_gathernoteproducts_set-${ total_forms -1 }-id" type="hidden">
                            </div>
                        </div>
                        <div class="col d-none">
                            <div class="form-group">
                                <input type="checkbox" name="gathernoteproducts_set-${ total_forms -1 }-DELETE" id="id_ordereventhasproduct_set-${ total_forms -1 }-DELETE" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="btn btn-sm btn-primary" name="${ total_forms -1 }" id="remove_gn_product_btn">Ta bort</div>
                        </div>
                    </div>
                `;

                // add html to the end of gn product container
                $('#gn_products_container').append(html_content);
                    }
        },

        autocomplete_products: function(field, row) {

            // check if selection field is a product field
            if (field == "product") {

                // make ajax call to get all products and populate atuocomplete list
                $.ajax({
                    url: '/products/get_products/',
                    dataType: 'json',
                    success: function (data) {
                        
                        // create an array to put product names inside
                        var product_list = []
                        // loop over products and store names in array
                        for (i=0; i < data.length; i++) {
                            product_list.push(data[i]["fields"]["name"])
                        }
                        
                        // populate field with autocomplete functionality
                        $( "#id_gathernoteproducts_set-" + row + "-" + field ).autocomplete({
                            minLength: 0,
                            source: product_list
                        });

                        // set options for autocomplete to display all products diectly on click
                        $( "#id_gathernoteproducts_set-" + row + "-" + field ).autocomplete( "search", "" );
                    }
                });
            }
            
        },

        autocomplete_price_type: function(field, row) {
            
            // get product from product field
            product = $("#id_gathernoteproducts_set-" + row + "-" +"product").val()

            // make ajax call to fetch price lists for product
            $.ajax({
                url: "/products/get_price_type/",
                type: "POST",
                dataType: "json",
                data: {
                    "product": product,
                    "delivery_event": "True",
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(price_lists) {
                    
                    // populate field with atocomplete functionality
                    $("#id_gathernoteproducts_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: price_lists
                    });
                
                    // set options for autocomplete to display all products diectly on click
                    $( "#id_gathernoteproducts_set-" + row + "-" + field ).autocomplete( "search", "" )
                }
            });

        },



        autocomplete_position: function(field, row) {

            // if field is position field
            if (field == "position") {
                
                // make ajax call and get all inventory positions
                $.ajax({
                    url: '/products/get_inventory_positions/',
                    dataType: 'json',
                    success: function (positions) {
                        // create an array to put position names inside
                        var positions_list = []
                        // loop over positions and store them in an array
                        for (i=0; i < positions.length; i++) {
                            positions_list.push(positions[i])
                        }
                        
                        // populate field with autocomplete functionality
                        $( "#id_gathernoteproducts_set-" + row + "-" + field ).autocomplete({
                            minLength: 0,
                            source: positions_list
                        });

                        // set options for autocomplete to display all positions diectly on click
                        $( "#id_gathernoteproducts_set-" + row + "-" + field ).autocomplete( "search", "" );
                    }
                });
            }
        },

        mark_as_gathered: function() {
            $("#id_ongoing").val("False")
            $("#id_gathered").val("True")
            $("#ongoing_badge").parent().prepend('<div id="gathered_badge" class="badge badge-success">Plockad</div>')
            $("#ongoing_badge").remove()

        },

        mark_as_not_gathered: function() {
            $("#id_ongoing").val("True")
            $("#id_gathered").val("False")
            $("#gathered_badge").parent().prepend('<div id="ongoing_badge" class="badge badge-warning">Plockning pågår</div>')
            $("#gathered_badge").remove()

        },

        filter: function() {
            ongoing = $("#gn_is_ongoing").prop('checked')
            gathered = $("#gn_is_gathered").prop('checked')
            console.log(ongoing)
            console.log(gathered)

            $.ajax({
                url: '/orders/filter_gather_notes/',
                type: 'POST',
                dataType: 'json',
                data: {"ongoing": ongoing,
                       "gathered": gathered,
                       'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                    },
                success: function(gather_notes) {
                    console.log('japp')
                    console.log(gather_notes)
                }

            })

            
        }
    },

    delivery_note: {

    },

    return_receipt: {

        add_return_receipt: function() {
            $.ajax({
                url: "/orders/add_return_receipt/",
                dataType: "json",
                success: function() {
                    location.reload(true)
                }
            })
        },

        autocomplete_customer : function() {
            $.ajax({
                url: "/customers/get_customers/",
                dataType: "json",
                success: function(customers) {
                    // populate field with autocomplete functionality
                    $("#id_customer").autocomplete({
                        minLength: 0,
                        source: customers
                    });

                }
            })
        },

        autocomplete_project: function() {
            customer = $("#id_customer").val()
        
            var match = /(?:id:)(.*)/.exec(customer)
            fortnox_id = match[1]
            console.log(fortnox_id)
            $.ajax({
                url: "/orders/get_projects/",
                type: "POST",
                dataType: "json",
                data: {
                    "fortnox_id": fortnox_id,
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(projects) {
                    console.log(projects)
                    // populate field with autocomplete functionality
                    $("#id_project").autocomplete({
                        minLength: 0,
                        source: projects
                    });
                    
                    $("#id_project").autocomplete( "search", "" );

                }
            })
        },
    },

    counting_note: {

        add_product_row: function() {

            // get total number of forms from django management form and save to variable
            total_forms = $('#id_countingnoteproducts_set-TOTAL_FORMS').val()
            // increment total number of forms in the django management form
            total_forms = Number(total_forms) + 1
            // update total number of forms in django management form
            $('#id_countingnoteproducts_set-TOTAL_FORMS').val(total_forms)

            // creat html content to add
            var html_content = `
            <div class="row bg-light p-2 m-0">
            <div class="col-2">
                <div class="form-group">
                    <input type="text" name="countingnoteproducts_set-${ total_forms -1 }-product" maxlength="128" id="id_countingnoteproducts_set-${ total_forms -1 }-product" class="form-control form-control-sm" />
                </div>
            </div>
            <div class="col-1">
                <fieldset disabled="">
                <div class="form-group">
                    <input type="text" name="countingnoteproducts_set-${ total_forms - 1 }-amount_out" value="0" class="form-control form-control-sm" id="id_countingnoteproducts_set-${ total_forms - 1 }-bad_amount" />
                </div>
                </fieldset>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <input type="text" name="countingnoteproducts_set-${ total_forms - 1 }-amount" value="0" class="form-control form-control-sm" id="id_countingnoteproducts_set-${ total_forms - 1 }-amount" />
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <input type="text" name="countingnoteproducts_set-${ total_forms - 1 }-bad_amount" value="0" class="form-control form-control-sm" id="id_countingnoteproducts_set-${ total_forms - 1 }-bad_amount" />
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <input type="text" name="countingnoteproducts_set-${ total_forms - 1 }-price_type" value="" class="form-control form-control-sm" id="id_countingnoteproducts_set-${ total_forms - 1 }-price_type" />
                </div>
            </div>
            <div class="col-1">
                <div class="form-group">
                    <input type="text" name="countingnoteproducts_set-${ total_forms - 1 }-position" value="lagervägen" class="form-control form-control-sm" maxlength="128" id="id_countingnoteproducts_set-${ total_forms - 1 }-position" />    
                </div>
             </div>
             <div class="col-1">
                <div class="form-group">
                    <select name="countingnoteproducts_set-${ total_forms -1}-exception" class="form-control form-control-sm" id="id_countingnoteproducts_set-${ total_forms -1 }-exception">
                        <option value="inget" selected="">Inget</option>
                        <option value="kass">Kass</option>
                    </select>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <input type="text" name="countingnoteproducts_set-${ total_forms - 1 }-comment" class="form-control form-control-sm" maxlength="128" id="id_countingnoteproducts_set-${ total_forms - 1 }-comment" />    
                </div>
            </div>
                <div class="col-1 d-none">
                    <div class="form-group">
                        <input name="countingnoteproducts_set-${ total_forms - 1 }-oep_id" value="" class="form-control form-control-sm" id="id_counting_noteproducts_set-${ total_forms - 1 }-oep_id" >
                    </div>
                </div>
                <div class="col d-none">
                    <div class="form-group">
                        <input name="countingnoteproducts_set-${ total_forms - 1 }-id" value="" id="id_counting_noteproducts_set-${ total_forms - 1 }-id" type="hidden">
                    </div>
                </div>
            <div class="col d-none">
                    <div class="form-group">
                        <input type="checkbox" name="countingnoteproducts_set-${ total_forms -1 }-DELETE" id="id_ordereventhasproduct_set-${ total_forms -1}-DELETE" />
                    </div>
                </div>
                <div class="col-1">
                    <div class="btn btn-sm btn-primary" name="${ total_forms -1 }" id="remove_cn_product_btn">Ta bort</div>
                </div>
            </div>
            `;

            // add html to the end of referent_contianer
            $('#cn_products_container').append(html_content);
        },

        autocomplete_products: function(field, row) {

            // check if selection field is a product field
            if (field == "product") {

                // make ajax call to get all products and populate atuocomplete list
                $.ajax({
                    url: '/products/get_products/',
                    dataType: 'json',
                    success: function (data) {
                        
                        // create an array to put product names inside
                        var product_list = []
                        // loop over products and store names in array
                        for (i=0; i < data.length; i++) {
                            product_list.push(data[i]["fields"]["name"])
                        }
                        
                        // populate field with autocomplete functionality
                        $( "#id_countingnoteproducts_set-" + row + "-" + field ).autocomplete({
                            minLength: 0,
                            source: product_list
                        });

                        // set options for autocomplete to display all products diectly on click
                        $( "#id_countingnoteproducts_set-" + row + "-" + field ).autocomplete( "search", "" );
                    }
                });
            }
            
        },

        autocomplete_position: function(field, row) {

            // if field is position field
            if (field == "position") {
                
                // make ajax call and get all inventory positions
                $.ajax({
                    url: '/products/get_inventory_positions/',
                    dataType: 'json',
                    success: function (positions) {
                        // create an array to put position names inside
                        var positions_list = []
                        // loop over positions and store them in an array
                        for (i=0; i < positions.length; i++) {
                            positions_list.push(positions[i])
                        }
                        
                        // populate field with autocomplete functionality
                        $( "#id_countingnoteproducts_set-" + row + "-" + field ).autocomplete({
                            minLength: 0,
                            source: positions_list
                        });

                        // set options for autocomplete to display all positions diectly on click
                        $( "#id_countingnoteproducts_set-" + row + "-" + field ).autocomplete( "search", "" );
                    }
                });
            }
        },

        autocomplete_price_type: function(field, row) {
            
            // get product from product field
            product = $("#id_countingnoteproducts_set-" + row + "-" +"product").val()

            // make ajax call to fetch price lists for product
            $.ajax({
                url: "/products/get_price_type/",
                type: "POST",
                dataType: "json",
                data: {
                    "product": product,
                    "delivery_event": "False",
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(price_lists) {
                    
                    // populate field with atocomplete functionality
                    $("#id_countingnoteproducts_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: price_lists
                    });
                
                    // set options for autocomplete to display all products diectly on click
                    $( "#id_countingnoteproducts_set-" + row + "-" + field ).autocomplete( "search", "" )
                }
            });

        },

        remove_product_row: function(row) {

            // check delete checkbox and hide row
            $("#id_countingnoteproducts_set-" + row + "-DELETE").prop("checked", true)
            $("#id_countingnoteproducts_set-" + row + "-product").parent().parent().parent().addClass("d-none")
        },

        mark_as_counted: function() {
            $("#id_ongoing").val("False")
            $("#id_counted").val("True")
            $("#ongoing_badge").parent().prepend('<span id="counted_badge" class="badge badge-success">Avräknad</span>')
            $("#ongoing_badge").remove()
        },

        mark_as_not_counted: function() {
            $("#id_ongoing").val("True")
            $("#id_counted").val("False")
            $("#counted_badge").parent().prepend('<span id="ongoing_badge" class="badge badge-warning">Pågående</span>')
            $("#counted_badge").remove()
        },

        send_products_to_oe: function() {

            // get total forms from parent window (order event) and child window (gather note)
            parent_total_forms = window.opener.$("#id_ordereventhasproduct_set-TOTAL_FORMS").val()
            child_total_forms = $('#id_countingnoteproducts_set-TOTAL_FORMS').val()

            order_event_id = window.opener.$("#order_event_id").val()
            invoice_id = "None"
            response = $.ajax({
                url: '/orders/get_order_defaults/',
                dataType: 'json',
                type: "POST",
                async: false,
                data: {
                    "order_event_id": order_event_id,
                    "invoice_id": invoice_id,
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function (defaults) {
                    order_discount = defaults["discount"]
                    order_std_price_list = defaults["std_price_list"]
                }
            });
            console.log(response)
            responseJSON = response["responseJSON"]
            std_price_list = responseJSON["std_price_list"]
            discount = responseJSON["std_price_list"]
            console.log(responseJSON)

            for (i=0; i < parent_total_forms; i++) {
                window.opener.$("#id_ordereventhasproduct_set-" + i + "-DELETE").prop("checked", true)
                window.opener.$("#id_ordereventhasproduct_set-" + i + "-product").parent().parent().parent().addClass("d-none")
            }

            for (i=0; i < child_total_forms; i++) {
                child_product = $('#id_countingnoteproducts_set-' + i + '-product').val()
                console.log(i)
                console.log(child_product)
                child_amount = $('#id_countingnoteproducts_set-' + i + '-amount').val()
                child_bad_amount = $('#id_countingnoteproducts_set-' + i + '-bad_amount').val()
                child_rent_forward = $('#id_countingnoteproducts_set-' + i + '-rent_forward').prop("checked")
                child_exception = $('#id_countingnoteproducts_set-' + i + '-exception').val()
                console.log(child_exception)
                console.log(child_rent_forward)
                if (child_exception == "kass") {
                    price_type = "ersättning"
                }
                else {
                    if (child_rent_forward == true) {
                        price_type = "vidareuthyrning"
                    }
                    else {
                        price_type = "uthyrning"
                    }
                }

               // increment total number of forms in parent window
                parent_total_forms = Number(parent_total_forms) + 1
                window.opener.$('#id_ordereventhasproduct_set-TOTAL_FORMS').val(parent_total_forms)
                order_event_id = window.opener.$("#order_event_id").val()
                invoice_id = "None"

                console.log("html " + child_product)
                var html_content =  `

                <div class="row bg-light pt-2 m-0">
                    <div class="col-2">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms - 1 }-product" value="${ child_product }" maxlength="128" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-product" />
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-price_list" value="${ std_price_list }" maxlength="128" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-price_list" />
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-price_type" value="" maxlength="128" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-price_type" />
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-amount" value="${ child_amount }" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-amount" />
                        </div>
                    </div>
                        <div class="col-1 d-none">
                            <div class="form-group">
                                <input name="ordereventhasproduct_set-${ parent_total_forms -1 }-return_is_match" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-return_is_match" class="form-control-sm form-check-input" type="checkbox" checked>
                            </div>
                        </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-base" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-base" />
                        </div>
                    </div>
                    <div class="col-1 d-none">
                        <div class="form-group  ">
                            <input name="ordereventhasproduct_set-${ parent_total_forms -1 }-base_is_default" class="form-control-sm form-check-input" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-base_is_default" type="checkbox" checked>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-price" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-price" />
                        </div>
                    </div>
                    <div class="col-1 d-none">
                        <div class="form-group  ">
                            <input name="ordereventhasproduct_set-${ parent_total_forms -1 }-price_is_default" class="form-control-sm form-check-input" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-price_is_default" type="checkbox" checked>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-discount" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-discount" />
                        </div>
                    </div>
                    <div class="col-1 d-none">
                        <div class="form-group  ">
                            <input name="ordereventhasproduct_set-${ parent_total_forms -1 }-discount_is_default" class="form-control-sm form-check-input" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-discount_is_default" type="checkbox" checked>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-total_price" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-total_price" />
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-total_price_per_month" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-total_price_per_month" />
                        </div>
                    </div>
                    <div class="col d-none">
                        <div class="form-group">
                            <input type="text" name="ordereventhasproduct_set-${ parent_total_forms -1 }-account" value="" maxlength="4" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-account" />
                        </div>
                    </div>
                    <div class="col d-none">
                        <div class="form-group">
                            <input type="hidden" name="ordereventhasproduct_set-${ parent_total_forms -1 }-id" id="id_ordereventhasproduct_set-${ parent_total_forms -1 }-id" />
                        </div>
                    </div>
                    <div class="col d-none">
                        <div class="form-group">
                            <input type="checkbox" name="ordereventhasproduct_set-${ parent_total_forms -1 }-DELETE" id="id_ordereventhasproduct_set-${ parent_total_forms -1}-DELETE" />
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="btn btn-sm btn-primary" name="${ parent_total_forms -1 }" id="remove_product">Ta bort</div>
                    </div>
                    <div id="not_match_flag_${ parent_total_forms -1 }" class="col-1">
                        
                    </div>
                </div>
                `

            // add html to the end of gn product container
            window.opener.$('#product_container').append(html_content);

            }
        },

        send_amount: function(selector) {
            row_id = $(selector).siblings().children().children().attr("id")
            data = formsys.helper.get_element_identity(row_id)
            row = data["row"]
            amount_out = $("#id_countingnoteproducts_set-" + row + "-amount_out").val()
            $("#id_countingnoteproducts_set-" + row + "-amount").val(amount_out)
        }
    },

    action_list: {

        autocomplete_position: function(element) {

                
                // make ajax call and get all inventory positions
                $.ajax({
                    url: '/products/get_inventory_positions/',
                    dataType: 'json',
                    success: function (positions) {
                        // create an array to put position names inside
                        var positions_list = []
                        // loop over positions and store them in an array
                        for (i=0; i < positions.length; i++) {
                            positions_list.push(positions[i])
                        }
                        
                        // populate field with autocomplete functionality
                        $(element).autocomplete({
                            minLength: 0,
                            source: positions_list
                        });

                        // set options for autocomplete to display all positions diectly on click
                        $(element).autocomplete( "search", "" );
                    }
                });
            
        },

        autocomplete_products: function(element) {

                // make ajax call to get all products and populate atuocomplete list
                $.ajax({
                    url: '/products/get_products/',
                    dataType: 'json',
                    success: function (data) {
                        
                        // create an array to put product names inside
                        var product_list = []
                        // loop over products and store names in array
                        for (i=0; i < data.length; i++) {
                            product_list.push(data[i]["fields"]["name"])
                        }
                        
                        // populate field with autocomplete functionality
                        $(element).autocomplete({
                            minLength: 0,
                            source: product_list
                        });

                        // set options for autocomplete to display all products diectly on click
                        $(element).autocomplete( "search", "" );
                    }
                });
            
        },

    },

    order_event: {

        add_product_row: function() {
            // get total number of forms from django management form and save to variable
            total_forms = $('#id_ordereventhasproduct_set-TOTAL_FORMS').val()
            // increment total number of forms in the django management form
            total_forms = Number(total_forms) + 1
            // update total number of forms in django management form
            $('#id_ordereventhasproduct_set-TOTAL_FORMS').val(total_forms)
            delivery_event = $("#id_delivery_event").val()
            order_event_id = $("#order_event_id").val()
            invoice_id = "None"

            // ajax call to get default order values, discount and price list
            $.ajax({
                url: '/orders/get_order_defaults/',
                dataType: 'json',
                type: "POST",
                data: {
                    "order_event_id": order_event_id,
                    "invoice_id": invoice_id,
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function (defaults) {
                    order_discount = defaults["discount"]
                    order_std_price_list = defaults["std_price_list"]

                                // creat html content to add
                    var html_content =  `

                    <div class="row bg-light pt-2 m-0">
                        <div class="col-2">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms - 1 }-product" maxlength="128" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-product" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-price_list" value="${ order_std_price_list }" maxlength="128" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-price_list" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-price_type" maxlength="128" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-price_type" />
                            </div>
                        </div>
                        <div class="col-1 d-none">
                            <div class="form-group">
                                <input name="ordereventhasproduct_set-${ total_forms -1 }-return_is_match" id="id_ordereventhasproduct_set-${ total_forms -1 }-return_is_match" class="form-control-sm form-check-input" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-amount" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-amount" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-base" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-base" />
                            </div>
                        </div>
                        <div class="col-1 d-none">
                            <div class="form-group  ">
                                <input name="ordereventhasproduct_set-${ total_forms -1 }-base_is_default" class="form-control-sm form-check-input" id="id_ordereventhasproduct_set-${ total_forms -1 }-base_is_default" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-price" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-price" />
                            </div>
                        </div>
                        <div class="col-1 d-none">
                            <div class="form-group  ">
                                <input name="ordereventhasproduct_set-${ total_forms -1 }-price_is_default" class="form-control-sm form-check-input" id="id_ordereventhasproduct_set-${ total_forms -1 }-price_is_default" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-discount" value="${ order_discount }" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-discount" />
                            </div>
                        </div>
                        <div class="col-1 d-none">
                            <div class="form-group  ">
                                <input name="ordereventhasproduct_set-${ total_forms -1 }-discount_is_default" class="form-control-sm form-check-input" id="id_ordereventhasproduct_set-${ total_forms -1 }-discount_is_default" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-total_price" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-total_price" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-total_price_per_month" value="0" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-total_price_per_month" />
                            </div>
                        </div>
                        <div class="col d-none">
                            <div class="form-group">
                                <input type="text" name="ordereventhasproduct_set-${ total_forms -1 }-account" maxlength="4" class="form-control form-control-sm" id="id_ordereventhasproduct_set-${ total_forms -1 }-account" />
                            </div>
                        </div>
                        <div class="col d-none">
                            <div class="form-group">
                                <input type="hidden" name="ordereventhasproduct_set-${ total_forms -1 }-id" id="id_ordereventhasproduct_set-${ total_forms -1 }-id" />
                            </div>
                        </div>
                        <div class="col d-none">
                            <div class="form-group">
                                <input type="checkbox" name="ordereventhasproduct_set-${ total_forms -1 }-DELETE" id="id_ordereventhasproduct_set-${ total_forms -1}-DELETE" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="btn btn-sm btn-primary" name="${ total_forms -1 }" id="remove_product">Ta bort</div>
                        </div>
                        <div id="not_match_flag_${ total_forms -1 }" class="col-1">
                            
                        </div>
                    </div>
                    `
                    // add html to the end of product_contianer
                    $('#product_container').append(html_content);
                    
                }
            });
        },

        remove_product_row: function(row) {

            // check delee field and hide row
            $("#id_ordereventhasproduct_set-" + row + "-DELETE").prop("checked", true)
            $("#id_ordereventhasproduct_set-" + row + "-product").parent().parent().parent().addClass("d-none")
        },

        autocomplete_products: function(field, row) {

            // make ajax call to get all products and populate atuocomplete list
            $.ajax({
                url: '/products/get_products/',
                dataType: 'json',
                success: function (data) {
                    
                    // create an array to put product names inside
                    var product_list = []
                    // loop over products and store names in array
                    for (i=0; i < data.length; i++) {
                        product_list.push(data[i]["fields"]["name"])
                    }
                    
                    // populate field with autocomplete functionality
                    $( "#id_ordereventhasproduct_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: product_list
                    });

                    // set options for autocomplete to display all products diectly on click
                    $( "#id_ordereventhasproduct_set-" + row + "-" + field ).autocomplete( "search", "" );
                }
            });
        },

        autocomplete_price_list: function(field, row) {
            
            // get product from product field
            product = $("#id_ordereventhasproduct_set-" + row + "-" +"product").val()
            console.log(product)

            // make ajax call to fetch price lists for product
            $.ajax({
                url: "/products/get_price_lists/",
                type: "POST",
                dataType: "json",
                data: {
                    "product": product,
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(price_lists) {
                    
                    // populate field with atocomplete functionality
                    $("#id_ordereventhasproduct_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: price_lists
                    });
                
                    // set options for autocomplete to display all products diectly on click
                    $( "#id_ordereventhasproduct_set-" + row + "-" + field ).autocomplete( "search", "" )
                }
            });

        },

        autocomplete_price_type: function(field, row) {
            product = $("#id_ordereventhasproduct_set-" + row + "-" +"product").val()
            price_list = $("#id_ordereventhasproduct_set-" + row + "-" +"price_list").val()
            delivery_event = $("#id_delivery_event").val()

            // make ajax call to fetch price type for product
            $.ajax({
                url: "/products/get_price_type/",
                type: "POST",
                dataType: "json",
                data: {
                    "product": product,
                    "delivery_event": delivery_event,
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(price_types) {
                    // populate field with atocomplete functionality
                    $("#id_ordereventhasproduct_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: price_types,
                        close: function() {
                            price_type = $("#id_ordereventhasproduct_set-" + row + "-" +"price_type").val()
                            console.log(price_type)

                            response = $.ajax({
                                url: "/products/get_prices/",
                                type: "POST",
                                dataType: "json",
                                data: {
                                    "product": product,
                                    "price_list": price_list,
                                    "price_type": price_type,
                                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                                },
                                success: function(data) {
                                    if (price_type == 'uthyrning') {
                                        $("#id_ordereventhasproduct_set-" + row + "-price").val(data.rent.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'vidareuthyrning') {
                                        $("#id_ordereventhasproduct_set-" + row + "-price").val(data.rent.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'försäljning') {
                                        $("#id_ordereventhasproduct_set-" + row + "-price").val(data.selling.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'försäljning förbrukningsmaterial') {
                                        $("#id_ordereventhasproduct_set-" + row + "-price").val(data.selling.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'ersättning') {
                                        // ersättning price is same as selling price
                                        $("#id_ordereventhasproduct_set-" + row + "-price").val(data.selling.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_ordereventhasproduct_set-" + row + "-account").val(data.account_nr)
                                    }

                                    // mark default values as true
                                    formsys.order_event.is_base_default(field, row)
                                    formsys.order_event.is_price_default(field, row)
                                    formsys.order_event.is_discount_default(field, row)
                                    formsys.order_event.is_return_product_match(field,row)
                                    // calc price on row after change
                                    formsys.order_event.calc_total_price_row(field,row)

                                }
                                });
                        }
                            
                    });
                    // set options for autocomplete to display all products diectly on click
                    $( "#id_ordereventhasproduct_set-" + row + "-" + field ).autocomplete( "search", "" )

                }
            });
        },

        calc_total_price_row: function(field, row) {
            // get variables from forms
            delivery_event = $("#id_delivery_event").val()
            price_type = $("#id_ordereventhasproduct_set-" + row + "-price_type").val()
            base = Number($("#id_ordereventhasproduct_set-" + row + "-base").val())
            amount = Number($("#id_ordereventhasproduct_set-" + row + "-amount").val())
            price = Number($("#id_ordereventhasproduct_set-" + row + "-price").val())
            discount = Number($("#id_ordereventhasproduct_set-" + row + "-discount").val())
            discount_factor = 1-(discount/100)

            total_price = (amount*price*discount_factor).toFixed(2)
            total_price_per_month = (total_price*30).toFixed(2)
            
            $("#id_ordereventhasproduct_set-" + row + "-total_price").val(total_price)

            // if price_type is uthyrning set total_price*30, else just set it to total_price*1
            if (price_type == "uthyrning" | price_type == "vidareuthyrning") {
            
                $("#id_ordereventhasproduct_set-" + row + "-total_price_per_month").val(total_price_per_month)
            }
            else {

                $("#id_ordereventhasproduct_set-" + row + "-total_price_per_month").val(total_price)
            }
            
            // update fields
            $("#id_ordereventhasproduct_set-" + row + "-total_price_per_month").parent().parent().removeClass("d-none")
            $("#prodhead_total_price_per_month").removeClass("d-none")
            
            
            formsys.order_event.calc_sum_products()

        },

        calc_sum_products: function() {
            // get total number of rows wit products
            total_products = $("#id_ordereventhasproduct_set-TOTAL_FORMS").val()

            // create variable to store calculations in
            sum_base_rent = 0
            sum_base_forward = 0
            sum_total_sale = 0
            sum_price = 0
            sum_price_30 = 0

            // loop over products and calculate sum of total price and total price 30 days
            for (i=0; i < total_products; i++) {
                amount = Number($("#id_ordereventhasproduct_set-" + i + "-amount").val())
                price_type = $("#id_ordereventhasproduct_set-" + i + "-price_type").val()
                base = Number($("#id_ordereventhasproduct_set-" + i + "-base").val())
                price = Number($("#id_ordereventhasproduct_set-" + i + "-total_price").val())
                price_30 = Number($("#id_ordereventhasproduct_set-" + i + "-total_price_per_month").val())
                
                if ( price_type == "uthyrning" ) {
                    sum_base_rent += amount*base
                }
                else if ( price_type == "vidareuthyrning" ) {
                    sum_base_forward += amount*base
                }
                else if ( price_type == "försäljning" || price_type == "försäljning förbrukningsmaterial") {
                    sum_total_sale += price
                }

                sum_price += price
                sum_price_30 += price_30
            }

            console.log(sum_total_sale)
            // update fields
            sum_total = (sum_price)
            sum_total_30 = (sum_base_rent + sum_base_forward + sum_price_30)
            $("#id_base_total_rent").val(sum_base_rent.toFixed(2))
            $("#id_base_total_forward").val(sum_base_forward.toFixed(2))
            $("#id_sum_total_sale").val(sum_total_sale.toFixed(2))
            $("#id_sum_total").val(sum_total.toFixed(2))
            $("#id_sum_total_30").val(sum_total_30.toFixed(2))

        },

        is_base_default: function(field, row) {
            // get needed field values
            base = $("#id_ordereventhasproduct_set-" + row + "-base").val()
            product = $("#id_ordereventhasproduct_set-" + row + "-product").val()
            price_list = $("#id_ordereventhasproduct_set-" + row + "-price_list").val()
            
            // ajax call to check if price is default or not
            $.ajax({
                url: '/orders/is_base_default/',
                type: 'POST',
                dataType: 'json',
                data: {"base": base,
                       "product": product,
                       "price_list": price_list,
                       'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                    },
                success: function(base_is_default) {
                    // if base is default remove red border around field and mark checkbox as true
                    if (base_is_default) {
                        $("#id_ordereventhasproduct_set-" + row + "-base").parent().removeClass("border rounded border-danger")
                        $("#id_ordereventhasproduct_set-" + row + "-base_is_default").prop('checked', true);
                    }
                    // if base is not default, add red border around field and mark checkbox as false
                    else {
                        $("#id_ordereventhasproduct_set-" + row + "-base").parent().addClass("border rounded border-danger")
                        $("#id_ordereventhasproduct_set-" + row + "-base_is_default").prop('checked', false);
                    }
                }

            })
        },

        is_price_default: function(field, row) {
            // get nessesary information from fields
            price = $("#id_ordereventhasproduct_set-" + row + "-price").val()
            product = $("#id_ordereventhasproduct_set-" + row + "-product").val()
            price_list = $("#id_ordereventhasproduct_set-" + row + "-price_list").val()
            price_type = $("#id_ordereventhasproduct_set-" + row + "-price_type").val()
            // make ajax call to check if price is default
            $.ajax({
                url: '/orders/is_price_default/',
                type: 'POST',
                dataType: 'json',
                data: {"price": price,
                       "product": product,
                       "price_list": price_list,
                       "price_type": price_type,
                       'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                    },
                success: function(price_is_default) {
                    // if price is default remove red border around field and mark checkbox as true
                    if (price_is_default) {
                        $("#id_ordereventhasproduct_set-" + row + "-price").parent().removeClass("border rounded border-danger")
                        $("#id_ordereventhasproduct_set-" + row + "-price_is_default").prop('checked', true);
                    }
                    // if price is not default add red border around field and mark checkbox as false
                    else {
                        $("#id_ordereventhasproduct_set-" + row + "-price").parent().addClass("border rounded border-danger")
                        $("#id_ordereventhasproduct_set-" + row + "-price_is_default").prop('checked', false);
                    }
                }

            })
        },

        is_discount_default: function(field, row) {
            // get nessesary informtion from fields
            discount = $("#id_ordereventhasproduct_set-" + row + "-discount").val()
            order_event_id = $("#order_event_id").val()
            invoice_id = "None"
            // make ajax call to check if discount is default value or not
            $.ajax({
                url: '/orders/is_discount_default/',
                type: 'POST',
                dataType: 'json',
                data: {"discount": discount,
                       "order_event_id": order_event_id,
                       "invoice_id": invoice_id,
                       'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                    },
                success: function(discount_is_default) {
                    // if discount is default remove red border from field and mark checkbox as true
                    if (discount_is_default) {
                        $("#id_ordereventhasproduct_set-" + row + "-discount").parent().removeClass("border rounded border-danger")
                        $("#id_ordereventhasproduct_set-" + row + "-discount_is_default").prop('checked', true);
                    }
                    // if discount is not default add red border around field and mark checbox as false
                    else {
                        $("#id_ordereventhasproduct_set-" + row + "-discount").parent().addClass("border rounded border-danger")
                        $("#id_ordereventhasproduct_set-" + row + "-discount_is_default").prop('checked', false);
                    }
                }

            })
        },

        is_return_product_match: function(field, row) {

            delivery_event = $("#id_delivery_event").val()
            if ( delivery_event != "True") {
                // get price type from field
                price_type = $("#id_ordereventhasproduct_set-" + row + "-price_type").val()
                // only if price type is uthyrning check if product is match with rented product
                if (price_type == "uthyrning" || price_type == "vidareuthyrning") {
                    // get nessesary information from fields
                    order_id = $("#id_order").val()
                    order_event_id = $("#order_event_id").val()
                    base = $("#id_ordereventhasproduct_set-" + row + "-base").val()
                    price = $("#id_ordereventhasproduct_set-" + row + "-price").val()
                    product = $("#id_ordereventhasproduct_set-" + row + "-product").val()
                    discount = $("#id_ordereventhasproduct_set-" + row + "-discount").val()
                    // make ajax call
                    $.ajax({
                        url: '/orders/is_return_product_match/' + order_id + '/' + order_event_id + '/',
                        type: 'POST',
                        dataType: 'json',
                        data: {"price": price,
                               "product": product,
                               "base": base,
                               "discount": discount,
                               "price_type": price_type,
                               'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                            },
                        success: function(return_is_match) {
                            // if return is match remove flag and mark checkbox as true
                            if (return_is_match) {
                                $("#not_match_flag_" + row).empty()
                                $("#id_ordereventhasproduct_set-" + row + "-return_is_match").prop('checked', true);
                            }
                            // if not match remove flag, add new flag and mark checkbox as false
                            else {
                                $("#not_match_flag_" + row).empty()
                                html = `

                                    <div style="font-size:1.4em; color:#dc3545">
                                        <i data-toggle="tooltip" data-placement="top" title="Produkten matchar inte redan uthyrd produkt" class="fas fa-exclamation-circle"></i>
                                    </div>
                                       `

                                $("#not_match_flag_" + row).append(html)
                                $('[data-toggle="tooltip"]').tooltip()
                                $("#id_ordereventhasproduct_set-" + row + "-return_is_match").prop('checked', false);
                            }
                        }
        
                    })
                }
            }


        },

        mark_shipped: function() {
            $("#id_shipped").val("True")
            $("#not_shipped_badge").removeClass("badge-danger")
            $("#not_shipped_badge").addClass("badge-success")
            $("#not_shipped_badge").text("Levererad")
        },

        add_gather_note: function() {

            order_event_id = $("#order_event_id").val()

            $.ajax({
                url: '/orders/add_gather_note/',
                dataType: 'json',
                type: "POST",
                data: {
                    "order_event_id": order_event_id,
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
                },
                success: function(data) {
                    $("#id_gather_note").val(data)
                    $("#gather_note_not_done_badge").parent().prepend('<div class="badge badge-warning">Plockning pågår</div>')
                }
            });
        },

        mark_gather_note_done: function() {
            $("#id_gather_note_done").val("True")
            $("#gather_note_not_done_badge").removeClass("badge-danger")
            $("#gather_note_not_done_badge").addClass("badge-success")
            $("#gather_note_not_done_badge").text("Plocksedel Klar")
        },

        unmark_gather_note_done: function() {
            console.log('hej')
            $("#id_gather_note_done").val("False")
            $("#gather_note_done_badge").removeClass("badge-success")
            $("#gather_note_done_badge").addClass("badge-danger")
            $("#gather_note_done_badge").text("Plocksedel inte klar")
        },

        edit_gather_note: function(gather_note_id) {

            url = '/orders/edit_gather_note/' + gather_note_id + '/'
            var win_props = 'height=800, width=1000, resizable=yes, scrollbars=yes';
            gather_note_win = open(url, 'gn_win', win_props);
        },

        add_delivery_note: function() {

            total_forms = $("#id_ordereventhasproduct_set-TOTAL_FORMS").val()
            products = []
            amounts = []
            for (i=0; i < total_forms; i++) {
                product = $("#id_ordereventhasproduct_set-" + i + "-product").val()
                amount = $("#id_ordereventhasproduct_set-" + i + "-amount").val()
                products.push(product)
                amounts.push(amount)
                }

            $.ajax({
                url: '/orders/add_delivery_note/',
                type: "POST",
                data: {
                    "products": products,
                    "amounts": amounts,
                    "order_event_id": $("#order_event_id").val(),
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(data) {
                  $("#id_delivery_note").val(data)
                  $("#add_delivery_note_btn").hide()
                  $("#edit_delivery_note_btn").show()
                }
            })
        },

        edit_delivery_note: function(delivery_note_id) {

            url = '/orders/edit_delivery_note/' + delivery_note_id + '/'
            var win_props = 'height=800, width=1200, resizable=yes, scrollbars=yes';
            delivey_note_win = open(url, 'dn_win', win_props);
        },

        edit_counting_note: function(counting_note_id) {
            url = '/orders/edit_counting_note/' + counting_note_id + '/'
            var win_props = 'height=800, width=1200, resizable=yes, scrollbars=yes';
            counting_note_win = open(url, 'cn_win', win_props)
        },

        mark_counted: function() {
            $("#id_counted").val("True")
            $("#not_counted_badge").removeClass("badge-danger")
            $("#not_counted_badge").addClass("badge-success")
            $("#not_counted_badge").text("Avräknad")
            
        },

        mark_delivery_note_done: function() {
            $("#id_delivery_note_done").val("True")
            $("#delivery_note_not_done_badge").removeClass("badge-danger")
            $("#delivery_note_not_done_badge").addClass("badge-success")
            $("#delivery_note_not_done_badge").text("Plocksedel klar")
        },
        
        mark_oe_as_end_event: function() {
            $("#id_end_event").val("True")
            $("#badge_field").append('<span id="return_badge" class="badge badge-warning">Sluthändelse</span>')
        },
        
        add_order_event: function(order_id, event_type) {
            console.log(event_type)
            $.ajax({
                url: '/orders/add_order_event/' + order_id + '/' + event_type + '/',
                dataType: 'json',
                success: function(data) {
                    location.reload()
                }
            })
        },

        calculate_weight: function() {
            nr_products = $("#id_ordereventhasproduct_set-TOTAL_FORMS").val()
            products = [];
            amounts = []
            for (i=0; i < nr_products; i++) {
                product = $("#id_ordereventhasproduct_set-" + i + "-product").val()
                amount = $("#id_ordereventhasproduct_set-" + i + "-amount").val()
                products.push(product)
                amounts.push(amount)
            }

            $.ajax({
                url: "/orders/get_total_weight/",
                type: "POST",
                data: {
                    "products": products,
                    "amounts": amounts,
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(total_weight) {
                    console.log(total_weight)
                    $("#id_transport-0-total_weight").val(total_weight)
                }
            })
        }
    },

    invoices: {

        add_shipping_to_invoice: function(order_event_id, invoice_id) {

            $.ajax({
                url: "/invoices/add_shipping_to_invoice/",
                type: "POST",
                 data: {
                    "order_event_id": order_event_id,
                    "invoice_id": invoice_id,
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function() {
                    location.reload(true)
                }
            })
        },

        add_article_row: function() {
            // get total number of forms from django management form and save to variable
            total_forms = $('#id_invoicearticle_set-TOTAL_FORMS').val()
            // increment total number of forms in the django management form
            total_forms = Number(total_forms) + 1
            // update total number of forms in django management form
            $('#id_invoicearticle_set-TOTAL_FORMS').val(total_forms)
            
            invoice_id = $("#invoice_id").val()
            order_event_id = "None"

            $.ajax({
                url: '/orders/get_order_defaults/',
                dataType: 'json',
                type: "POST",
                data: {
                    "order_event_id": order_event_id,
                    "invoice_id": invoice_id,
                    "csrfmiddlewaretoken": $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function (defaults) {
                    order_discount = defaults["discount"]
                    order_std_price_list = defaults["std_price_list"]

                    // creat html content to add
                    var html_content =  `

                    <div class="row bg-light pt-2 m-0">
                        <div class="col-2 d-none">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms - 1 }-invoice" maxlength="128" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-invoice" />
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms - 1 }-product" maxlength="128" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-product" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-price_list" maxlength="128" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-price_list" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-price_type" maxlength="128" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-price_type" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-amount" value="0" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-amount" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-days_rented" value="0" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-days_rented" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-base" value="0" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-base" />
                            </div>
                        </div>
                        <div class="col-1 d-none">
                            <div class="form-group  ">
                                <input name="invoicearticle_set-${ total_forms -1 }-base_is_default" class="form-control-sm form-check-input" id="id_invoicearticle_set-${ total_forms -1 }-base_is_default" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-price" value="0" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-price" />
                            </div>
                        </div>
                        <div class="col-1 d-none">
                            <div class="form-group  ">
                                <input name="invoicearticle_set-${ total_forms -1 }-price_is_default" class="form-control-sm form-check-input" id="id_invoicearticle_set-${ total_forms -1 }-price_is_default" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-discount" value="${ order_discount }" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-discount" />
                            </div>
                        </div>
                        <div class="col-1 d-none">
                            <div class="form-group  ">
                                <input name="invoicearticle_set-${ total_forms -1 }-discount_is_default" class="form-control-sm form-check-input" id="id_invoicearticle_set-${ total_forms -1 }-discount_is_default" type="checkbox" checked>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-total_price" value="0" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-total_price" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="form-group">
                                <input type="text" name="invoicearticle_set-${ total_forms -1 }-account" maxlength="4" class="form-control form-control-sm" id="id_invoicearticle_set-${ total_forms -1 }-account" />
                            </div>
                        </div>
                        <div class="col d-none">
                            <div class="form-group">
                                <input type="hidden" name="invoicearticle_set-${ total_forms -1 }-id" id="id_invoicearticle_set-${ total_forms -1 }-id" />
                            </div>
                        </div>
                        <div class="col d-none">
                            <div class="form-group">
                                <input type="checkbox" name="invoicearticle_set-${ total_forms -1 }-DELETE" id="id_invoicearticle_set-${ total_forms -1}-DELETE" />
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="btn btn-sm btn-primary" name="${ total_forms -1 }" id="remove_article">Ta bort</div>
                        </div>
                    </div>
                    `
                    // add html to the end of product_contianer
                    $('#invoice_container').append(html_content);
                    
                        }

                    });
                    

                },
      

        remove_article_row: function(row) {

            // check delee field and hide row
            $("#id_invoicearticle_set-" + row + "-DELETE").prop("checked", true)
            $("#id_invoicearticle_set-" + row + "-product").parent().parent().parent().addClass("d-none")
        },

        autocomplete_articles: function(field, row) {

            // make ajax call to get all products and populate atuocomplete list
            $.ajax({
                url: '/products/get_products/',
                dataType: 'json',
                success: function (data) {
                    
                    // create an array to put product names inside
                    var product_list = []
                    // loop over products and store names in array
                    for (i=0; i < data.length; i++) {
                        product_list.push(data[i]["fields"]["name"])
                    }
                    
                    // populate field with autocomplete functionality
                    $( "#id_invoicearticle_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: product_list
                    });

                    // set options for autocomplete to display all products diectly on click
                    $( "#id_invoicearticle_set-" + row + "-" + field ).autocomplete( "search", "" );
                }
            });
        },

        autocomplete_price_list: function(field, row) {
            
            // get product from product field
            product = $("#id_invoicearticle_set-" + row + "-" +"product").val()
            console.log(product)

            // make ajax call to fetch price lists for product
            $.ajax({
                url: "/products/get_price_lists/",
                type: "POST",
                dataType: "json",
                data: {
                    "product": product,
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(price_lists) {
                    
                    // populate field with atocomplete functionality
                    $("#id_invoicearticle_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: price_lists
                    });
                
                    // set options for autocomplete to display all products diectly on click
                    $( "#id_invoicearticle_set-" + row + "-" + field ).autocomplete( "search", "" )
                }
            });

        },

        autocomplete_price_type: function(field, row) {

            product = $("#id_invoicearticle_set-" + row + "-" +"product").val()
            price_list = $("#id_invoicearticle_set-" + row + "-" +"price_list").val()

            // make ajax call to fetch price type for product
            $.ajax({
                url: "/products/get_price_type/",
                type: "POST",
                dataType: "json",
                data: {
                    "product": product,
                    "delivery_event": "invoice",
                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                },
                success: function(price_types) {

                    // populate field with atocomplete functionality
                    $("#id_invoicearticle_set-" + row + "-" + field ).autocomplete({
                        minLength: 0,
                        source: price_types,
                        close: function() {
                            price_type = $("#id_invoicearticle_set-" + row + "-" +"price_type").val()

                            response = $.ajax({
                                url: "/products/get_prices/",
                                type: "POST",
                                dataType: "json",
                                data: {
                                    "product": product,
                                    "price_list": price_list,
                                    "price_type": price_type,
                                    'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                                },
                                success: function(data) {
                                    
                                    if (price_type == 'uthyrning') {
                                        $("#id_invoicearticle_set-" + row + "-price").val(data.rent.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'vidareuthyrning') {
                                        $("#id_invoicearticle_set-" + row + "-price").val(data.rent.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'försäljning') {
                                        $("#id_invoicearticle_set-" + row + "-price").val(data.selling.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'försäljning förbrukningsmaterial') {
                                        $("#id_invoicearticle_set-" + row + "-price").val(data.selling.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-account").val(data.account_nr)
                                    }
                                    else if (price_type == 'ersättning') {
                                        // ersättning price is same as selling price
                                        $("#id_invoicearticle_set-" + row + "-price").val(data.selling.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-base").val(data.base.toFixed(2))
                                        $("#id_invoicearticle_set-" + row + "-account").val(data.account_nr)
                                    
                                   }

                                    // mark default values as true
                                    formsys.invoices.is_base_default(field, row)
                                    formsys.invoices.is_price_default(field, row)
                                    formsys.invoices.is_discount_default(field, row)

                                    // calc price on row after change
                                    console.log("eyy")
                                    formsys.invoices.calc_total_price_row(field,row)
                                    console.log("eyy2")
                                }
                                });
                        }
                            
                    });
                    // set options for autocomplete to display all products diectly on click
                    $( "#id_invoicearticle_set-" + row + "-" + field ).autocomplete( "search", "" )
                }
            });
        },

        is_base_default: function(field, row) {
            base = $("#id_invoicearticle_set-" + row + "-base").val()
            product = $("#id_invoicearticle_set-" + row + "-product").val()
            price_list = $("#id_invoicearticle_set-" + row + "-price_list").val()
            

            $.ajax({
                url: '/orders/is_base_default/',
                type: 'POST',
                dataType: 'json',

                data: {"base": base,
                       "product": product,
                       "price_list": price_list,
                       'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                    },
                success: function(base_is_default) {
                    if (base_is_default) {
                        $("#id_invoicearticle_set-" + row + "-base").parent().removeClass("border rounded border-danger")
                        $("#id_invoicearticle_set-" + row + "-base_is_default").prop('checked', true);
                    }
                    else {
                        $("#id_invoicearticle_set-" + row + "-base").parent().addClass("border rounded border-danger")
                        $("#id_invoicearticle_set-" + row + "-base_is_default").prop('checked', false);
                    }
                }

            })
        },

        is_price_default: function(field, row) {
            price = $("#id_invoicearticle_set-" + row + "-price").val()
            product = $("#id_invoicearticle_set-" + row + "-product").val()
            price_list = $("#id_invoicearticle_set-" + row + "-price_list").val()
            price_type = $("#id_invoicearticle_set-" + row + "-price_type").val()
            

            $.ajax({
                url: '/orders/is_price_default/',
                type: 'POST',
                dataType: 'json',
                data: {"price": price,
                       "product": product,
                       "price_list": price_list,
                       "price_type": price_type,
                       'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                    },
                success: function(price_is_default) {
                    if (price_is_default) {
                        $("#id_invoicearticle_set-" + row + "-price").parent().removeClass("border rounded border-danger")
                        $("#id_invoicearticle_set-" + row + "-price_is_default").prop('checked', true);
                    }
                    else {
                        $("#id_invoicearticle_set-" + row + "-price").parent().addClass("border rounded border-danger")
                        $("#id_invoicearticle_set-" + row + "-price_is_default").prop('checked', false);
                    }
                }

            })
        },

        calc_total_price_row: function(field, row) {
            console.log("ops")
            // get variables from forms
            price_type = $("#id_invoicearticle_set-" + row + "-price_type").val()
            base = Number($("#id_invoicearticle_set-" + row + "-base").val())
            amount = Number($("#id_invoicearticle_set-" + row + "-amount").val())
            price = Number($("#id_invoicearticle_set-" + row + "-price").val())
            discount = Number($("#id_invoicearticle_set-" + row + "-discount").val())
            discount_factor = 1-(discount/100)

            total_price = (amount*price*discount_factor).toFixed(2)
            
            $("#id_invoicearticle_set-" + row + "-total_price").val(total_price)

        },

        is_discount_default: function(field, row) {
            discount = $("#id_invoicearticle_set-" + row + "-discount").val()
            invoice_id = $("#invoice_id").val()
            order_event_id = "None"           
            $.ajax({
                url: '/orders/is_discount_default/',
                type: 'POST',
                dataType: 'json',
                data: {"discount": discount,
                       "order_event_id": order_event_id,
                       "invoice_id" : invoice_id,
                       'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val()
                    },
                success: function(discount_is_default) {
                    if (discount_is_default) {
                        $("#id_invoicearticle_set-" + row + "-discount").parent().removeClass("border rounded border-danger")
                        $("#id_invoicearticle_set-" + row + "-discount_is_default").prop('checked', true);
                    }
                    else {
                        $("#id_invoicearticle_set-" + row + "-discount").parent().addClass("border rounded border-danger")
                        $("#id_invoicearticle_set-" + row + "-discount_is_default").prop('checked', false);
                    }
                }

            })
        },

        autocomplete_customer : function() {
            $.ajax({
                url: "/customers/get_customers/",
                dataType: "json",
                success: function(customers) {
                    // populate field with autocomplete functionality
                    $("#invoice_customer").autocomplete({
                        minLength: 0,
                        source: customers
                    });

                }
            })
        },

        send_to_fortnox: function(invoice_id, slector) {

            $.ajax({
                url: "/invoices/send_invoice_fnox/" + invoice_id + "/",
                dataType: "json",
                success: function(response) {
                    console.log(response)
                    if ( response == 201 ) {
                        location.reload(true)
                    }
                    else {
                        error = response["ErrorInformation"]["message"]
                       $(selector).parent().append("error: " + error) 
                    }
                    
                }
            })
        },

        send_all_fnox_invoices: function() {

                    // ajax call to get ew customers from fortnox
                    $.ajax({
                        beforeSend: function() {
                            // show spinning wheel (loading)
                            $("#loading_wheel").removeClass("d-none")
                        },
                        url: "/invoices/send_all_fnox_invoices/",
                        dataType: 'json',
                        success: function (errors) {
                            // hide spinning wheel (loading done)
                            $("#loading_wheel").addClass("d-none")
                            
                            html= ""
                            for (i=0; i< errors.length; i++) {
                                console.log(errors[i])
                                error = errors[i]
                                invoice = error["invoice"]
                                invoice_error = error["error"]["message"]
                                html += "invoice: " + invoice + ", error: " + invoice_error + "</br>"
                                console.log(html)
                            }
                            $("#invoice_errors").append(html)
                            $("#invoice_errors").removeClass("d-none")

                        }
                        
                    })
                },


    },

    reports: {
 
        autocomplete_price_list: function() {
    
                // make ajax call to fetch price lists
                $.ajax({
                    url: "/products/get_all_price_lists/",
                    type: "GET",
                    dataType: "json",
                    success: function(price_lists) {
                        
                        // populate field with atocomplete functionality
                        $("#id_pricelist").autocomplete({
                            minLength: 0,
                            source: price_lists
                        });
                    
                        // set options for autocomplete to display all products diectly on click
                        $("#id_pricelist").autocomplete( "search", "" )
                    }
                });
    
        },
    }
};

$(document).ready(function () {
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
});

// convert checkboxes to right bootstrap class
$( ":checkbox" ).removeClass("form-control")
$( ":checkbox" ).addClass("form-check-input")

// add date picker widget to date ad timefields
$( "#id_invoiced_to" ).datepicker( { dateFormat: 'yy-mm-dd' } );
$( "#id_completion_time" ).datepicker( { dateFormat: 'yy-mm-dd' } );
$( "#id_created_date" ).datepicker( { dateFormat: 'yy-mm-dd' } );
$( "#id_starting_time" ).datepicker( { dateFormat: 'yy-mm-dd' } );
$( "#id_transport-0-time" ).datetimepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_datetime" ).datetimepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_rent_stop_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_rent_start_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_expiry_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_delivery_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#invoice_start_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#invoice_end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_start_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_end_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_date_from" ).datepicker({ dateFormat: 'yy-mm-dd' });
$( "#id_date_to" ).datepicker({ dateFormat: 'yy-mm-dd' });



// button to synch customers from fortnox
$("#sync_new_customers").on("click", function(){
    formsys.customers.sync_new_customers()
})

// button to synch customers from fortnox
$("#sync_fnox_customer_info").on("click", function(){
    formsys.customers.sync_fnox_customer_info()
})

// button to mark order as waiting
$("#mark_as_waiting").on("click", function() {
    formsys.order.mark_as_waiting()
})

// button to unmark order as waiting
$("#mark_as_completed").on("click", function() {
    formsys.order.mark_as_completed()
})

// button to mark order as waiting
$("#mark_as_ongoing").on("click", function() {
    formsys.order.mark_as_ongoing()
})

// add product row on gahter note when button is clicked.
$("#add_gn_product").on("click", function(){
    formsys.gather_note.add_product_row();
})

// remove product row on gather note when button is clicked
$("#gn_products_container").on("click", "#remove_gn_product_btn", function() {
    row = $(this).attr("name")
    formsys.gather_note.remove_product_row(row)
})

// button for fetching products from orde event
$("#get_products_from_oe_btn").on("click", function() {
    formsys.gather_note.get_products_from_oe()
})

// auocompletes the product field in gather note product row
$('#gn_products_container').on('click', '.form-control', function() {
    // get id of clicked element
    var select_id = $(this).attr("id");
    // process the id string
    data = formsys.helper.get_element_identity(select_id)
    // call autocomplete functions
    if (data["field"] == "product") {
        formsys.gather_note.autocomplete_products(data["field"], data["row"])
    }
    else if (data["field"] == "position") {
        formsys.gather_note.autocomplete_position(data["field"], data["row"])
    }
    else if (data["field"] == "price_type") {
        formsys.gather_note.autocomplete_price_type(data["field"], data["row"])
    }
});

$(".al_position").on("click", function() {
    element = $(this)
    formsys.action_list.autocomplete_position(element)
})

$(".al_product").on("click", function() {
    element = $(this)
    formsys.action_list.autocomplete_products(element)
})

// button for adding a gahter note to the order event
$("#add_gather_note_btn").on("click", function() {
    formsys.order_event.add_gather_note();
    $("#edit_gather_note_btn").show()
    $("#add_gather_note_btn").hide()
});

// button for editing gather note
$("#edit_gather_note_btn").on("click", function(){
    var gather_note_id = $("#id_gather_note").val()
    formsys.order_event.edit_gather_note(gather_note_id)
})

// only display edit gahter note button when there is
// something in the field. Else show add button 
$(document).ready(function () {
    var gather_note = $("#id_gather_note").val()
    if (gather_note == "") {
        $("#edit_gather_note_btn").hide()
    }
    else {
        $("#add_gather_note_btn").hide()
    }

});

// button for adding return order event
$("#add_return_btn").on("click", function () {
    var order_id = $("#order_id").val()
    formsys.order_event.add_order_event(order_id, order_type="return")
});

// button for adding delivery order event
$("#add_delivery_btn").on("click", function () {
    var order_id = $("#order_id").val()
    formsys.order_event.add_order_event(order_id, order_type="delivery")
});

// button for marking gather note as gathered
$("#mark_as_gathered_btn").on("click", function() {
    formsys.gather_note.mark_as_gathered();
})

// button for marking gather note as not gathered
$("#mark_as_not_gathered_btn").on("click", function() {
    formsys.gather_note.mark_as_not_gathered();
})

// button for marking gather note as completed
$("#mark_gather_note_done_btn").on("click", function() {
    formsys.order_event.mark_gather_note_done();
})

// button for marking gather note as completed
$("#unmark_gather_note_done_btn").on("click", function() {
    formsys.order_event.unmark_gather_note_done();
})

// button to add product on order event
$("#add_product").on("click", function() {
    formsys.order_event.add_product_row()
})

// auocompletes the product field on order_event product row
$('#product_container').on('click', '.form-control', function() {
    // get id of clicked element
    var select_id = $(this).attr("id");
    // process the id string
    data = formsys.helper.get_element_identity(select_id)

    // decide what kind of field was seected and call appropriet
    // autocomplete function.
    if (data["field"] == "product") {
        formsys.order_event.autocomplete_products(data["field"], data["row"])
    } 
    else if (data["field"] == "price_list") {
        formsys.order_event.autocomplete_price_list(data["field"], data["row"])
    }
    else if (data["field"] == "price_type") {
        formsys.order_event.autocomplete_price_type(data["field"], data["row"])
    }
});



//  
$("#product_container").on('change', '.form-control', function() {
    // get id of clicked element
    var select_id = $(this).attr("id");
    // process the id string
    return_event = $("#id_return_event").val()
    data = formsys.helper.get_element_identity(select_id)
    formsys.order_event.is_base_default(data["field"], data["row"])
    formsys.order_event.is_price_default(data["field"], data["row"])
    formsys.order_event.is_discount_default(data["field"], data["row"])
    if (return_event == "True") {
        formsys.order_event.is_return_product_match(data['field'], data['row'])
    }
    formsys.order_event.calc_total_price_row(data["field"], data["row"])
});

$("#calculate_sum_products").on('click', function() {
    formsys.order_event.calc_sum_products()
})

// button for adding transport as shipped
$("#mark_shipped_btn").on("click", function() {
    formsys.order_event.mark_shipped()
})

// button for removing product rows
$("#product_container").on("click", "#remove_product", function() {
    row = $(this).attr("name")
    formsys.order_event.remove_product_row(row)
})

// only display edit delivery note button when there is
// something in the field. Else show add button 
$(document).ready(function () {
    var delivery_note = $("#id_delivery_note").val()
    if (delivery_note == "") {
        $("#edit_delivery_note_btn").hide()
    } else {
        $("#add_delivery_note_btn").hide()
    }
});

// button to add delivery note
$("#add_delivery_note_btn").on("click", function(){
    formsys.order_event.add_delivery_note()
})

// button to edit delivery note
$("#edit_delivery_note_btn").on("click", function(){
    delivery_note_id = $("#id_delivery_note").val()
    formsys.order_event.edit_delivery_note(delivery_note_id)
})

// button for marking delivery note done
$("#mark_delivery_note_done_btn").on("click", function(){
    formsys.order_event.mark_delivery_note_done();
})

// button to edit counting note
$("#edit_counting_note_btn").on("click", function() {
    var counting_note_id = $("#id_counting_note").val()
    formsys.order_event.edit_counting_note(counting_note_id)
})

// button to mark order event as counted
$("#mark_counted_btn").on("click", function() {
    formsys.order_event.mark_counted()
})

// add product row on counting note
$("#add_cn_product").on("click", function() {
    formsys.counting_note.add_product_row()
})

// button to add return receipt
$("#add_return_receipt_btn").on("click", function() {
    formsys.return_receipt.add_return_receipt()
})

$("#invoice_customer").on("click", function() {
    formsys.invoices.autocomplete_customer()
})

$("#id_customer").on("click", function() {
    console.log('test')
    formsys.return_receipt.autocomplete_customer()
})

$("#id_project").on("click", function() {
    formsys.return_receipt.autocomplete_project()
})
// auocompletes the product field in gather note product row
$('#cn_products_container').on('click', '.form-control', function() {
    // get id of clicked element
    var select_id = $(this).attr("id");
    // process the id string
    data = formsys.helper.get_element_identity(select_id)
    // call autocomplete function
    if (data["field"] == "product") {
        formsys.counting_note.autocomplete_products(data["field"], data["row"])
    }
    else if (data["field"] == "position") {
        formsys.counting_note.autocomplete_position(data["field"], data["row"])
    }
    else if (data["field"] == "price_type") {
        formsys.counting_note.autocomplete_price_type(data["field"], data["row"])
    }
});

// remove product row on gather note when button is clicked
$("#cn_products_container").on("click", "#remove_cn_product_btn", function() {
    row = $(this).attr("name")
    formsys.counting_note.remove_product_row(row)
})

$("#cn_products_container").on("change", ".form-control", function() {
    id = $(this).attr("id")
    data = formsys.helper.get_element_identity(id)
    console.log(data)
    row = data["row"]
    exception = $("#id_countingnoteproducts_set-" + row + "-exception").val()
    console.log(exception)
    if (exception == "kass") {
        $("#id_countingnoteproducts_set-" + row + "-price_type").val("ersättning")
    }
    else {
        $("#id_countingnoteproducts_set-" + row + "-price_type").val("uthyrning")
    }
})

// button to mark counting note as counted
$("#mark_as_counted_btn").on("click", function() {
    formsys.counting_note.mark_as_counted()
})

// button to mark counting note as not counted
$("#mark_as_not_counted_btn").on("click", function() {
    console.log("aaa")
    formsys.counting_note.mark_as_not_counted()
})

// button to transfer products on order event to counting note
$("#send_products_to_oe_btn").on("click", function() {
    formsys.counting_note.send_products_to_oe()
})

// button to add referent
$("#add_referent").on("click", function() {
    customer_id = $("#customer_id").text()
    formsys.customers.add_referent(customer_id)
})

// button to calculate total weight of products on order event
$("#calculate_weight").on("click", function() {
    formsys.order_event.calculate_weight()
})

// button to put shipping on invoice
$(".put_on_invoice").on("click", function() {
    
    // get order event id from name atribute on button
    order_event_id = $(this).attr("name")
    // get invoice id from input field
    invoice_id = $("#invoice_nr_"+ order_event_id).val()
    formsys.invoices.add_shipping_to_invoice(order_event_id, invoice_id)
})

// button to add article on invoice
$("#add_article").on("click", function() {
    formsys.invoices.add_article_row()
})

// button for removing article on invoice
$("#invoice_container").on("click", "#remove_article", function() {
    row = $(this).attr("name")
    formsys.invoices.remove_article_row(row)
})

// auocompletes the article field on order_event product row
$('#invoice_container').on('click', '.form-control', function() {
    // get id of clicked element
    var select_id = $(this).attr("id");
    // process the id string
    data = formsys.helper.get_element_identity(select_id)

    // decide what kind of field was seected and call appropriet
    // autocomplete function.
    if (data["field"] == "product") {
        formsys.invoices.autocomplete_articles(data["field"], data["row"])
    } 
    else if (data["field"] == "price_list") {
        formsys.invoices.autocomplete_price_list(data["field"], data["row"])
    }
    else if (data["field"] == "price_type") {
        formsys.invoices.autocomplete_price_type(data["field"], data["row"])
    }
});

$("#invoice_container").on('change', '.form-control', function() {
    // get id of clicked element
    var select_id = $(this).attr("id");
    // process the id string
    data = formsys.helper.get_element_identity(select_id)
    formsys.invoices.is_base_default(data["field"], data["row"])
    formsys.invoices.is_price_default(data["field"], data["row"])
    formsys.invoices.is_discount_default(data["field"], data["row"])
    formsys.invoices.calc_total_price_row(data["field"], data["row"])

});

$(".send_to_fortnox").on("click", function() {
    selector = this
    invoice_id = $(selector).attr("name")
    console.log(invoice_id)
    console.log("yes")
    formsys.invoices.send_to_fortnox(invoice_id, selector)
})

$("#send_all_invoices").on("click", function() {
    console.log("yes")
    formsys.invoices.send_all_fnox_invoices()
})


$("#calculate_sum_products").on('click', function() {
    formsys.order_event.calc_sum_products()
})

// button for adding transport as shipped
$("#mark_shipped_btn").on("click", function() {
    formsys.order_event.mark_shipped()
})

// button for marking order event as end event
$("#mark_oe_as_end_event").on("click", function() {
    formsys.order_event.mark_oe_as_end_event()
})

// button for removing product rows
$("#product_container").on("click", "#remove_product", function() {
    row = $(this).attr("name")
    formsys.order_event.remove_product_row(row)
})

$("#gn_is_ongoing").on("change", function() {
    console.log('hej')
    formsys.gather_note.filter()
})

$("#gn_is_gathered").on("change", function() {
    console.log('hej')
    formsys.gather_note.filter()
})

$("#id_order_price_list").on("click", function() {
    console.log('test')
    formsys.order.autocomplete_price_list()
})

$("#id_pricelist").on("click", function() {
    console.log('test')
    formsys.reports.autocomplete_price_list()
})

$(".amount_arrow").on("click", function() {
    formsys.counting_note.send_amount(this)
})


// button to remove customer referent
$(".btn").on("click", function() {
    // get id of button
    id = $(this).attr("id")

    //use regex o extract information
    var match = /(remove_referent)/.exec(id);
    var match2 = /\d+/.exec(id)

    // save results from regex to variables
    var field = match[0]
    var row = match2[0]

    // get referent id
    referent_id = $("#referent_id_" + row).text()

    if ( field == "remove_referent" ) {
        formsys.customers.remove_referent(referent_id)
        }
})

