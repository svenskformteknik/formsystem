from django.shortcuts import render
from products.models import PriceList, Product
from orders.models import OrderEvent, Transport
from reports.forms import WriteOffForm, TransportForm
# -----------------------------------------
# Helper functions
# ----------------------------------------


def merge_equal_products(products_list):
    '''
    takes a list of products with duplicate products and makes
    a smaller list with unique products. If it finds prodcts
    with equal names it adds their amount.
    '''
    # create a empty list to store products in
    compressed_list = []

    # loop over all producst from the list supplied to the function
    for list_product in products_list:
        # set match to false as default
        match = False

        # if compressed list is empty add list_product to compressed list
        if not compressed_list:
            compressed_list.append(list_product)
        else:
            # if compressed list already has products loop over products
            # in compressed list

            for product in compressed_list:
                # if list_product and product in compressed list are equal
                if product.product == list_product.product:
                    # add the amount of list product to compressed product
                    product.amount += list_product.amount
                    # set match true and break out of loop. We do not want
                    # to compare more since we found a match
                    match = True
                    break
                else:
                    # if not equal set match to false
                    match = False

            # if we did not find a match after looping over all
            # the compressed products add the list_product to compressed list
            if not match:
                compressed_list.append(list_product)

    return compressed_list

# ---------------------------------------------
# Views
# ---------------------------------------------


def write_off(request):

    report_data = []

    if request.method == "POST":

        form = WriteOffForm(request.POST)

        date_from = request.POST["date_from"]
        date_to = request.POST["date_to"]
        pricelist = request.POST["pricelist"]

        p_compensation = []
        p_sold = []

        order_events = OrderEvent.objects.filter(
                rent_start_date__range=(date_from, date_to))

        for oe in order_events:
            products = oe.ordereventhasproduct_set.all()
            for product in products:
                if product.price_type == "ersättning":
                    p_compensation.append(product)
                elif product.price_type == "försäljning":
                    p_sold.append(product)

        sold_products = merge_equal_products(p_sold)
        compensation_products = merge_equal_products(p_compensation)
        pricelist = PriceList.objects.get(name=pricelist)

        for product in compensation_products:
            product_object = Product.objects.get(pk=product.id)
            value = product_object.price_set.get(pricelist=pricelist).selling
            product.value = round(product.amount*value, 2)

        for product in sold_products:
            product_object = Product.objects.get(pk=product.id)
            value = product_object.price_set.get(pricelist=pricelist).selling
            product.value = round(product.amount*value, 2)

        report_data = [[date_from, date_to, pricelist.name, ""]]

        for product in compensation_products:
            report_data.append(
                    ["Kass", product.amount, product.product, product.value])

        for product in sold_products:
            report_data.append(
                    ["Såld", product.amount, product.product, product.value])
    else:
        form = WriteOffForm()

    context_dict = {
            "report_data": report_data,
            "form": form,
            }

    return render(request, "write_off_report.html", context_dict)


def transports(request):

    report_data = []

    if request.method == "POST":

        form = TransportForm(request.POST)

        date_from = request.POST["date_from"]
        date_to = request.POST["date_to"]

        transports = Transport.objects.filter(time__range=(date_from, date_to))
        report_data = [[date_from, "", date_to, ""]]

        for transport in transports:
            report_data.append([transport.order_event.order.customer.name,
                                transport.time,
                                transport.order_event.order.project,
                                transport.price
                                ])

    else:
        form = TransportForm()

    context_dict = {
                    "report_data": report_data,
                    "form": form
                    }

    return render(request, "transport_report.html", context_dict)
