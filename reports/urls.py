from django.conf.urls import url
from reports import views

urlpatterns = [

    # views --------------------------------------
    url(r'^write_off/$', views.write_off, name='write_off'),
    url(r'^transports/$', views.transports, name='transports'),
]
