
from django import forms


class WriteOffForm(forms.Form):
    pricelist = forms.CharField(
            required=True, max_length=128, label="Prislista")
    date_from = forms.DateField(required=True, label="datum (från)")
    date_to = forms.DateField(required=True, label="datum (till)")

    def __init__(self, *args, **kwargs):
        super(WriteOffForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

        self.fields['pricelist'].widget.attrs.update({
            'placeholder': 'Prislista'})
        self.fields['date_from'].widget.attrs.update({
            'placeholder': 'Datum (från)'})
        self.fields['date_to'].widget.attrs.update({
            'placeholder': 'Datum (till)'})


class TransportForm(forms.Form):
    date_from = forms.DateField(required=True, label="datum (från)")
    date_to = forms.DateField(required=True, label="datum (till)")

    def __init__(self, *args, **kwargs):
        super(TransportForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs[
                    'class'] = 'form-control form-control-sm'

        self.fields['date_from'].widget.attrs.update({
            'placeholder': 'Datum (från)'})
        self.fields['date_to'].widget.attrs.update({
            'placeholder': 'Datum (till)'})
