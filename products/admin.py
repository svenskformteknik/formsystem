from django.contrib import admin
from products.models import Supplier, SupplierHasProduct, \
    InventoryPosition, Inventory, Price, PriceList, Product, ProductGroup, \
    ProductHasAccount, ProductType, Account, ActionListProduct


# Register your models here.
class ProductInline(admin.TabularInline):
    model = SupplierHasProduct
    extra = 1


class InventoryAdmin(admin.ModelAdmin):

    list_display = ('product', 'amount', 'position')


class ActionListProductAdmin(admin.ModelAdmin):
    list_display = ("product", "amount", "position")


class InventoryInline(admin.TabularInline):
    model = Inventory
    extra = 1


class ProductPriceInline(admin.TabularInline):
    model = Price
    extra = 1


class ProductAccountInline(admin.TabularInline):
    model = ProductHasAccount
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    list_display = ("name", "product_type", "group")
    list_filter = ("product_type", "group")
    search_fields = ["name"]

    inlines = [
        ProductInline,
        InventoryInline,
        ProductPriceInline,
        ProductAccountInline
    ]


admin.site.register(ProductType)
admin.site.register(ProductGroup)
admin.site.register(Account)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductHasAccount)
admin.site.register(Inventory, InventoryAdmin)
admin.site.register(Supplier)
admin.site.register(SupplierHasProduct)
admin.site.register(PriceList)
admin.site.register(Price)
admin.site.register(InventoryPosition)
admin.site.register(ActionListProduct, ActionListProductAdmin)
