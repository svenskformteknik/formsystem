from django.db import models


# Create your models here.
class ProductType(models.Model):
    product_type = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.product_type


class ProductGroup(models.Model):
    group = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.group


class Account(models.Model):
    ACCOUNTING_CHOICES = (
        ('uthyrning', 'Uthyrning'),
        ('försäljning', 'Försäljning'),
        ('försäljning förbrukningsmaterial',
            'Försäljning förbrukningsmaterial'),
        ('ersättning', 'Ersättning'),
        ('vidareuthyrning', 'Vidareuthyrning'),
        ('frakt', 'Frakt')
    )
    account_type = models.CharField(max_length=128,
                                    choices=ACCOUNTING_CHOICES,
                                    default='uthyrning')

    account_nr = models.IntegerField(unique=True)

    def __str__(self):
        return self.account_type


class Product(models.Model):
    name = models.CharField(max_length=128, unique=True)
    product_type = models.ForeignKey(ProductType)
    group = models.ForeignKey(ProductGroup)
    unit = models.FloatField()
    weight = models.FloatField()
    total_nr = models.IntegerField()
    number_in_stock = models.IntegerField()
    # picture = models.ImageField()

    def __str__(self):
        return self.name


class ProductHasAccount(models.Model):
    product = models.ForeignKey(Product)
    account_type = models.ForeignKey(Account)

    def __str__(self):
        return str(self.product) + ' ' + str(self.account_type)


class InventoryPosition(models.Model):
    position = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.position


class Inventory(models.Model):
    product = models.ForeignKey(Product)
    amount = models.IntegerField()
    position = models.ForeignKey(InventoryPosition)

    class Meta:
        unique_together = ("product", "position")

    def __str__(self):
        return str(self.product)


class ActionListProduct(models.Model):
    product = models.CharField(max_length=128)
    amount = models.IntegerField()
    position = models.CharField(max_length=128)

    def __str__(self):
        return str(self.product)


class Supplier(models.Model):
    name = models.CharField(max_length=128, unique=True)
    contact = models.CharField(max_length=128)
    address = models.CharField(max_length=128)
    address2 = models.CharField(max_length=128, blank=True, null=True)
    postnr = models.CharField(max_length=12)
    place = models.CharField(max_length=128)
    email = models.EmailField()

    def __str__(self):
        return self.name


class SupplierHasProduct(models.Model):
    product = models.ForeignKey(Product)
    Supplier = models.ForeignKey(Supplier)

    def __str__(self):
        return str(self.product)


class PriceList(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name


class Price(models.Model):
    pricelist = models.ForeignKey(PriceList)
    base = models.FloatField()
    rent = models.FloatField()
    selling = models.FloatField()
    buying = models.FloatField()
    post_work = models.FloatField()
    product = models.ForeignKey(Product)

    def __str__(self):
        return str(self.product) + ' ' + str(self.base)
