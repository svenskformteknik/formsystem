from django.conf.urls import url
from products import views

urlpatterns = [

    # views --------------------------------------
    url(r'^action_list/$', views.action_list, name='action_list'),
    url(r'^inventory/$', views.inventory, name='inventory'),
    url(r'^product_details/(?P<product_id>[\w\-]+)/$',
        views.product_details, name='product_details'),

    # API calls ----------------------------------
    url(r'^get_products/$', views.get_products, name='get_products'),

    url(r'^get_price_lists/$', views.get_price_lists,
        name='get_price_lists'),
    url(r'^get_all_price_lists/$', views.get_all_price_lists,
        name="get_all_price_lists"),

    url(r'^get_price_type/$', views.get_price_type, name='get_price_type'),
    url(r'^get_prices/$', views.get_prices, name='get_prices'),
    url(r'^get_inventory_positions/$', views.get_inventory_positions,
        name='get_inventory_positions'),
]
