from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
import json

from products.models import Product, PriceList, Account, InventoryPosition, \
    Inventory, ActionListProduct


# -----------------------------
# Helper functions
# -----------------------------


def remove_inventory_check(products_list):
    errors = []
    for product in products_list:

        # get position id from gather note product
        product_pos_id = InventoryPosition.objects.get(
            position=product.position).id
        # get product object from identity of gather note product
        model_product = Product.objects.get(name=product.product)

        # get all inventory for product object at gather note products position
        inventory_at_position = model_product.inventory_set.filter(
            position=product_pos_id).first()

        # if there is a product at that position
        if inventory_at_position:

            # check to see if inventory amount is more
            # than gather note product amount
            if inventory_at_position.amount >= product.amount:
                pass
            else:
                # if not, give error and go to next
                print(product.id)
                errors.append(
                    "Det finns inte tillräckligt många" + " "
                    + str(product.product)
                    + " i lager på position: " + str(product.position))
        else:

            # if not, give error
            errors.append("Det finns inga " + str(
                product.product) + " på position: " + str(product.position))

    return errors


def remove_from_inventory(products_list):
    for product in products_list:

        # get position id from gather note product
        product_pos_id = InventoryPosition.objects.get(
            position=product.position).id
        # get product object from identity of gather note product
        model_product = Product.objects.get(name=product.product)
        # get all inventory for product object at gather note products position
        inventory_at_position = model_product.inventory_set.filter(
            position=product_pos_id).first()

        if inventory_at_position:
            # if it is, subtract it from inventory and save to database
            if product.price_type == "vidareuthyrning":
                pass
            elif product.price_type == "försäljning" or product.price_type ==\
                    "försäljning förbrukningsmaterial":
                inventory_at_position.amount -= product.amount
                model_product.number_in_stock -= product.amount
                model_product.total_nr -= product.amount
                model_product.save()
                inventory_at_position.save()
            elif product.price_type == "uthyrning":
                inventory_at_position.amount -= product.amount
                model_product.number_in_stock -= product.amount
                model_product.save()
                inventory_at_position.save()

        else:
            print("error - no inventory product")


def add_to_inventory(products_list):
    for product in products_list:

        # get position id from gather note product
        product_pos = InventoryPosition.objects.get(
            position=product.position)
        # get product object from identity of gather note product
        model_product = Product.objects.get(name=product.product)
        # get all inventory for product object at gather note products position
        inventory_at_position = model_product.inventory_set.filter(
            position=product_pos.id).first()

        if inventory_at_position:
            if product.price_type == "vidareuthyrning":
                pass
            elif product.price_type == "försäljning" or product.price_type ==\
                    "försäljning förbrukningsmaterial":
                # if it is, add it from inventory and save to database
                inventory_at_position.amount += product.amount
                model_product.number_in_stock += product.amount
                model_product.total_nr += product.amount
                model_product.save()
                inventory_at_position.save()
            elif product.price_type == "uthyrning":
                # if it is, add it from inventory and save to database
                inventory_at_position.amount += product.amount
                model_product.number_in_stock += product.amount
                model_product.save()
                inventory_at_position.save()

        else:
            if product.price_type == "vidareuthyrning":
                pass
            elif product.price_type == "försäljning" or product.price_type ==\
                    "försäljning förbrukningsmaterial":
                inventory_at_position = Inventory()
                inventory_at_position.product = model_product
                inventory_at_position.amount = product.amount
                inventory_at_position.position = product_pos
                model_product.number_in_stock += product.amount
                model_product.total_nr += product.amount
                model_product.save()
                inventory_at_position.save()
            elif product.price_type == "uthyrning":
                inventory_at_position = Inventory()
                inventory_at_position.product = model_product
                inventory_at_position.amount = product.amount
                inventory_at_position.position = product_pos
                model_product.number_in_stock += product.amount
                model_product.save()
                inventory_at_position.save()


def send_products_to_actionlist(products_list):
    for product in products_list:
        alist_product = ActionListProduct.objects.filter(
                product=product.product, position=product.position).first()
        print('test')
        print(alist_product)
        if alist_product:
            alist_product.amount += product.amount
            alist_product.save()
        else:
            # create action list item and save to database
            alist_product = ActionListProduct()
            alist_product.amount = product.amount
            alist_product.product = product.product
            alist_product.position = product.position
            alist_product.save()

        # decrease total number of product
        model_product = Product.objects.get(name=product.product)
        model_product.total_nr -= product.amount
        model_product.save()


def retriev_products_from_actionlist(products_list, increase_total=True):
    errors = []

    # check if product  is on action list
    for product in products_list:
        alist_product = ActionListProduct.objects.filter(
            product=product.product, position=product.position).first()

        # if it exists
        if alist_product:

            # check that enough exists
            if alist_product.amount < product.amount:
                errors.append(
                        "Det finns inte tillräckligt många "
                        + str(product.product)
                        + " på åtgärdslistan vid position: "
                        + str(product.position))

        else:
            # if product do not exist give error
            errors.append("Produkten " + str(product.amount) + " " + str(
                product.product) +
                          " vid position: " + str(product.position) +
                          " finns inte på Åtgärdslistan")

    # check for errors
    if errors:

        # if there are any errors return them
        return errors
    else:

        # if no errors delete products on action
        # list and increse total number of product
        for product in products_list:
            alist_product = ActionListProduct.objects.filter(
                product=product.product, position=product.position).first()
            alist_product.amount -= product.amount

            # if product amount = 0 , delete action list product
            if alist_product.amount == 0:
                alist_product.delete()
            else:
                # otherwise save change
                alist_product.save()

            if increase_total:

                # increase total number of product
                model_product = Product.objects.get(name=product.product)
                model_product.total_nr += product.amount
                model_product.save()

    return errors


def check_inventory_positions(products_list):
    errors = []
    for product in products_list:
        print(product.position)
        try:
            InventoryPosition.objects.get(position=str(product.position))
            print("exists")
        except:
            print("not exists")
            if product.position == "" or product.position is None:
                product.position = "___"
            errors.append(
                    "lagerplats" + " " + str(
                        product.position) + " existerar inte")
    return errors


def update_inventory(products_list, add, counting_note=False,
                     gather_note=False):

    # check if products list comes from counting note
    if counting_note:

        # check taht inventor positions exists
        errors = check_inventory_positions(products_list)
        if errors:
            return errors
        else:
            # divide products into two categories, exceptions and "normal"
            exceptions = [product for product in products_list if
                          product.exception == "kass"]
            products_list = [product for product in products_list if
                             product.exception == "inget"]

            # remove bad_amount from amount (number that should be added
            # or removed from inventory)
            for product in products_list:
                product.amount = product.amount - product.bad_amount

            # if products should be added to inventory
            if add:

                add_to_inventory(products_list)

                # send any exceptions to the action list
                if exceptions:
                    send_products_to_actionlist(exceptions)

            # if products should be removed from inventory
            else:

                # check if enough products exists in inventory
                errors = remove_inventory_check(products_list)

                # if there are no errors return errors
                if errors:
                    return errors
                else:

                    # if no errors remove them from invenory
                    remove_from_inventory(products_list)

                # if there are exceptions retriev them from action list
                if exceptions:
                    errors = retriev_products_from_actionlist(exceptions)

                    # if there are errors, return them
                    if errors:
                        return errors

    # if products comes from gather note
    if gather_note:

        # check taht inventor positions exists
        errors = check_inventory_positions(products_list)
        if errors:
            return errors
        else:

            # if products should be added to inventory, add them
            if add:
                add_to_inventory(products_list)

            # if not remove them
            else:

                # check if enough products exists
                errors = remove_inventory_check(products_list)

                # if errors return them
                if errors:
                    return errors
                else:
                    # remove products
                    remove_from_inventory(products_list)

    # if no error were detected return a true statement
    return []


# ------------------------------
# Views
# ------------------------------

def inventory(request):

    all_products = Product.objects.all()

    return render(request, "inventory.html", {"all_products": all_products})


def product_details(request, product_id):

    product = Product.objects.get(pk=product_id)
    inventory = product.inventory_set.all()
    prices = product.price_set.all()
    suppliers = product.supplierhasproduct_set.all()
    return render(request, "product_details.html",
                  {"product": product,
                   "inventory": inventory,
                   "prices": prices,
                   " suppliers": suppliers})


def action_list(request):

    # get all action list products
    al_products = ActionListProduct.objects.all()
    # make error and success list
    errors = []
    success = []

    if request.method == "POST":
        try:
            # if button al12 is pressed, that is if we want to send
            # action list product to inventory
            request.POST["al12"]

            al1 = ActionListProduct()
            try:
                al1.amount = int(request.POST["al1_amount"])
            except:
                errors.append("antalet är inte ett giltigt nummer")
            al1.product = request.POST["al1_product"]
            al1.position = request.POST["al1_position"]
            al2 = ActionListProduct()
            try:
                al2.amount = int(request.POST["al2_amount"])
            except:
                errors.append("antalet är itne ett giltigt nummer")
            al2.product = request.POST["al2_product"]
            al2.position = request.POST["al2_position"]

            # check if field data are correct
            try:
                Product.objects.get(name=al2.product)
            except:
                errors.append("Det finns ingen lagerprodukt med namnet: "
                              + str(al2.product))
            try:
                InventoryPosition.objects.get(position=al2.position)
            except:
                errors.append("det finns ingen lagerposition med namnet: " +
                              str(al2.position))
            # if no errors proceed
            if not errors:
                # get product from action list
                retriev_errors = retriev_products_from_actionlist(
                    [al1], increase_total=False)
                # if there are errors add them to error list
                if retriev_errors:
                    for error in retriev_errors:
                        errors.append(error)
                else:
                    # if no errors proceed to add product to inventory
                    add_to_inventory([al2])
                    # increase total amount of product
                    model_product = Product.objects.get(name=al2.product)
                    model_product.total_nr += al2.amount
                    model_product.save()

                    success.append("Ändring lyckades!")

        # if button al12 not pressed pass to next
        except:
            pass

        try:
            # if button al34 is pressed, that is if
            # we want to send inventory item to action list
            request.POST["al34"]

            al3 = ActionListProduct()
            try:
                al3.amount = int(request.POST["al3_amount"])
            except:
                errors.append("antalet är inte ett giltigt nummer")

            al3.product = request.POST["al3_product"]
            al3.position = request.POST["al3_position"]
            al4 = ActionListProduct()
            try:
                al4.amount = int(request.POST["al4_amount"])
            except:
                errors.append("antalet är inte ett giltigt nummer")
            al4.product = request.POST["al4_product"]
            al4.position = request.POST["al4_position"]

            # check if field data is ok
            try:
                Product.objects.get(name=al3.product)
            except:
                errors.append("Det finns ingen lagerprodukt med namnet: "
                              + str(al3.product))
            try:
                InventoryPosition.objects.get(position=al3.position)
            except:
                errors.append("det finns ingen lagerposition med namnet: "
                              + str(al3.position))
            try:
                Product.objects.get(name=al4.product)
            except:
                errors.append("Det finns ingen lagerprodukt med namnet: "
                              + str(al4.product))
            try:
                InventoryPosition.objects.get(position=al4.position)
            except:
                errors.append("det finns ingen lagerposition med namnet: "
                              + str(al4.position))

            # if no errors proceed to remove product from inventory
            if not errors:
                inv_errors = remove_inventory_check([al3])
                # check if any errors from inventory check
                if inv_errors:
                    # if errors add them to error list
                    for error in inv_errors:
                        errors.append(error)
                # if no errors proceed to remove product from inventory
                if not errors:
                    remove_from_inventory([al3])
                    send_products_to_actionlist([al4])
                    model_product = Product.objects.get(name=al3.product)
                    # subtract product amount from total amount on product
                    model_product.total_nr -= al3.amount
                    model_product.save()
                    success.append("Ändring lyckades")
        # if button al34 is not pressed, do nothing
        except:
            pass

    return render(request, "action_list.html", {
        "al_products": al_products,
        "errors": errors,
        "success": success
    })

# -----------------------------
# API calls
# -----------------------------


def get_products(request):

    products = serializers.serialize(
        "json", Product.objects.all(), fields=('name'))

    return HttpResponse(products, content_type='application/json')


def get_price_lists(request):

    # get product name from post data
    product_name = request.POST["product"]

    # get product from product name
    product = Product.objects.filter(name=product_name).first()

    # get prices that belongs to producs
    prices = product.price_set.all()

    # make list of all prices lists
    prices_list = [price.pricelist.name for price in prices]
    prices_list = json.dumps(prices_list)

    return HttpResponse(prices_list, content_type='application/json')


def get_all_price_lists(request):

    price_lists = PriceList.objects.all()
    price_lists = [price_list.name for price_list in price_lists]
    price_lists = json.dumps(price_lists)

    return HttpResponse(price_lists, content_type='application/json')


def get_price_type(request):

    # get product name from post data
    product_name = request.POST["product"]
    delivery_event = request.POST["delivery_event"]

    # check if order event is delivery event
    if delivery_event == "True":
        delivery_event = True
    elif delivery_event == "invoice":
        delivery_event = "invoice"
    else:
        delivery_event = False

    # get product from product name
    product = Product.objects.filter(name=product_name).first()
    # get accounts that belong to products

    # filter price types depending on order event type
    if delivery_event:
        accounts = product.producthasaccount_set.filter(
                account_type__account_type='uthyrning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='vidareuthyrning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='försäljning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='försäljning förbrukningsmaterial')
    else:
        accounts = product.producthasaccount_set.filter(
                account_type__account_type='uthyrning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='vidareuthyrning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='ersättning') \

    if delivery_event == "invoice":
        accounts = product.producthasaccount_set.filter(
                account_type__account_type='uthyrning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='vidareuthyrning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='försäljning') \
            | product.producthasaccount_set.filter(
                account_type__account_type='försäljning förbrukningsmaterial')\
            | product.producthasaccount_set.filter(
                account_type__account_type='ersättning') \

    accounts_list = [account.account_type.account_type for account in accounts]
    accounts_list = json.dumps(accounts_list)

    return HttpResponse(accounts_list, content_type='application/json')


def get_prices(request):

    # get product, price list, price type from post data
    product = request.POST['product']
    price_list = request.POST['price_list']
    price_type = request.POST['price_type']

    product = Product.objects.filter(name=product).first()
    price_list = PriceList.objects.filter(name=price_list)
    price = product.price_set.filter(pricelist=price_list).first()
    account = Account.objects.filter(account_type=price_type).first()

    # store data in context dict
    price_data = {'base': price.base*product.unit,
                  'rent': product.unit*price.rent,
                  'selling': product.unit*price.selling,
                  'post_work': product.unit*price.post_work,
                  'account_type': account.account_type,
                  'account_nr': account.account_nr}

    price_data = json.dumps(price_data)

    return HttpResponse(price_data, content_type='application/json')


def get_inventory_positions(request):

    positions = InventoryPosition.objects.all()
    positions = [position.position for position in positions]
    positions = json.dumps(positions)

    return HttpResponse(positions, content_type='application/json')
