Utvecklingsmiljö
==================

Utvecklingsmiljön består av fyra programvaror

   * databas (PostgreSQL)
   * wsgi server (uwsgi)
   * reverse proxy server (Nginx)
   * web framework (django)

Dessa fyra programvaror körs i ett virtaliseringsystem för operativsystem kallat docker
. Dockers uppgift är att isolera en programvara på ett sådant sätt att den endast har
fasta beroenden av andra mjukvaror. Isoleringen som kallas en container kan man sedan
flytta mellan olika datorer/servrar (med olika hårdvaruuppsättnignar) utan att det blir
problem med versionskonflikter). Så länge man kan installera docker i sig på hårdvaran
som man använder ska det i teorin gå att köra containern där utan problem.

I utvecklingsmiljön så använder jag 3 containrar. En för databasen, en för django och
uwsgi, och en för Nginx. Dessa kan man enkelt starta upp med rätt konfigurationer genom
att köra filen ``docker-compose.yml`` som finns i git repositoryn.



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   postgreSQL
   uwsgi
   Nginx
   django
