Simon tidslogg
======================================


180124
--------------------------------------
* programmerat databasen för produkter, leverantörer, lager och prislista.
* fört in testdata och kontrollerat att databasen fungerar som tänkt. Verkar fungera.
* programmerat djangos inbyggda admin-interface så att data presenteras på rätt sätt. Krävdes en del research för att få det att fungera.
* Installerade och konfigurerade dokumentationshemsidan för projektet där bla denna tidslogg ska finnas. Mer dokumentation om projektet kommer att läggas upp efter hand.

180125
-------------------------------------
* programmerat databasen för kunder, kundreferenser, ordrar, projekt, orderhändelser, transporter, plocksedlar och följesedlar.
* fört in testdata för de nya databasgrupperna och testat så allt fungerat som det är tänkt. Allt verkar fungera som tänkt.
* programmerat admin-interfacet så att data presenteras på rätt sätt.
* påbörjat att skriva lite på dokumentationen av utvecklingsmiljön.

180128
------------------------------------
* Installerat programvara för att dokumentera databasen på ett bra sätt.
* Gjort små förändringar i databasens design.
* kontrollerat vissa relationskopplingar och tänkt igenom hurvida det ska vara zero-to-many eller one-to-may förhållanden mellan vissa entiteter.
* dokumenterat hela den nuvarande designen på databasen i ett ERR-diagram.
* fortsatt skriva på dokumentationen av utvecklingsmiljön.

180129
------------------------------------
* implementerade twitter bootstrap i systemet för att enkelt kunna formge de olika delarna i systemet.
* implementerade loginfunktionalitet i systemet och skapade inloggningssida för användare.
* korrigerade ett misstag i eer-digrammet på databasen.
* implementerade en listningsfunktion av alla kunder i databasen med "pagination"

180130
------------------------------------
* implementerat en funktion att se all lagrad information om en kund
* implementerat en funktion att se alla ordar som tillhör en specifik kund.
* veckomöte
* gjort research på django formsets

180131
------------------------------------
* läst och lärt mig grunderna i javascrip
* läst och lärt mig grunderna i jQuery

180201
------------------------------------
* fördjupat mig i JQuery
* lärt mig grunderna i AJAX

180202
------------------------------------
* gått igenom fakturering med Kalle

180205
------------------------------------
* gjort tester på AJAX calls och fått systemet att funka med dessa!
* börjat formulera faktureringssytemet

180206
------------------------------------
* suttit och gått igenom några orderexempel för hand för att bättre förstå systemet med fakturering av uthyret material etc.

180207
------------------------------------
* fått en ide på hur faktureringssystemet kan fungera och hur databasen kan se ut.
* kollat nogrannare på fortnox API. Verkar vara ett enkelt system med bra dokumentation som jag tror inte ska vara några problem att använda. MEN, såvitt jag vet så verkar det inte funka att lägga till en extra kolumn för tex dagshyra som gör att summan inte beräknas rätt på fakturaartiklar i vårat fall. Jag har skickat iväg ett ärende om detta i fortnox. Vi får se vad de svarar.
* har programmerat in den uppdaterade databasstrukturen.

180208
------------------------------------
* fått api nyckel och testmiljö för fortnox.
* experimenterat med api calls i python och kan nu skicka data mellan systemet och fortnox.
* All information på fakturor och kunder etc. i fortnox går nu att lista, hämta, uppdatera eller skapa från vårat system mha api calls.
* fått svar från fortnox support och fått bekräftat att det inte går att lägga till fler fält under artiklar på fakturor :(
* updaterat adminsidan efter nya databasstrukturen
* läst och fördjupat mig i django forms.

180211
------------------------------------
* läst, testat och implementerat funktion för att ha flera forms på samma sida.

180212
------------------------------------
* implementerat "lägg till kund"-sida
* implementerat sida som visar all information om en kund
* implementerat sida som visar alla ordrar som en kund har
* implementerat sida som visar all information om en order
* gjort ändringar i databaseng

180213
------------------------------------
* implementerat "lägg till order"-sida
* implementerat "editera order"-sida

180214-16
------------------------------------
* jobbat på implementering av "lägg till orderhändelse"-sida
* många små problem som jag gjort reasearch på och löst i samband med orderhändelse-sidan
* implmenterat en någorlunda fungerande orderhändelsesida

180219
------------------------------------
* implementerat en "editera orderhändelse" sida
* implementerat en "lista alla orderhändelser för en order"-sida

180220-21
------------------------------------
* klurat kring hur orderhändelseflödet ska fungera

180222
------------------------------------
* gått igenom med Kalle och Henrik hur orderhändelseflödet ska gå till

180223
------------------------------------
* börjat lägga till plocksedel-sida
* behövde ett bättre sätt att organisera javascript-koden så börjaed göra research på detta
* läst på om hur javascrip hanterar objekt och hur man kan använda detta till att bättre organisera koden

180226
------------------------------------
* omstrukturerat och skrivit om ganska mycket av javascript koden för att dels minska ner på koden men också ha den mer strukturerad och lättöverskådlig.
* stötte på en del problem med dopdown lister så har nu implementerat ett bättre system med autocomplete-fält

180226-180301
-----------------------------------
* skrivit om och omstrukturerat hela koden som hanterar orderhändelseflödet
* Nästan klar med Leveransfallet. Bara lite smågrejer kvar att lägga till innan jag börjar på returfallet.
