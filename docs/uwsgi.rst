Uwsgi
=====

uwsgi är en typ av webserver som ligger mellan proxyservern och en webapplikation skriven i python. WSGI står för web server gateway interface och som namnet antyder har den som uppgift att översätta förfrågningar från proxy servern så att webbapplikationen förstår vad som efterfrågas. På samma sätt agerar uwsgi översätare från webapplikationen (i detta fall django) till proxy servern.


