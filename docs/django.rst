Django
=================

formsystem programmeras huvudsakligen i programmeringsspråket Python vilket är ett general-purpose språk som inte direkt är anpassat för webbutvekling men som fungerar utmärkt som språk att använda på en server för att tex ta emot data och manipulera denna på olika sätt.

För att göra utvecklingen enklare och snabbare använder formsystem sig utav ett utvecklingsramverk speciellt utvecklat för programmering av datadrivna webbsidor. Wikipedia summerar det bra:

från wikipedia:

 "Django är ett ramverk för utveckling av webbsidor. Django är skrivet i Python. Det utvecklades ursprungligen för nyhetssidor och släpptes under BSD-licensen 2005. Namnet kommer av gitarristen Django Reinhardt. Syftet med Django är att göra det enklare att utveckla avancerade databas-drivna webbplatser. Det ska vara lätt att återanvända komponenter och det ska gå snabbt att skriva kod. Django genererar automatiskt administrations-gränssnitt utifrån data-modellen som anges i Python-kod."

Django följer MTV-principen vilket innebär att ramverket enkelt beskrivet består av tre lager:

Model - Detta lager innehåller allt som har med datan att göra. Hur man kommer åt den, hur man kollar att den är rätt, hur datan är relaterad till varandra, etc.

Template - Detta lager är presentationslagret och bestämmer hur datan ska presenteras på en webbsida.

View - Detta lager innehåller själva logiken som hämtar och manipulerar data på olika sätt för att sedan skickas vidare till template laget.

Enkelt fungerar django så att man definierar hur databasen ska se ut i en fil kallat ``models.py``. Sedan programmerar man i en fil kallat ``views.py`` hur data ska manipuleras och sedan har man ett gäng mallar ``templates`` som man presenterar datan på.
