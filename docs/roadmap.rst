Roadmap
==================

1. Implementera "redigera orderhändelse"

   a. skapa kundorder-view
   b. skapa orderhändelse-view
   c. skapa inspektera order-view

2. Implementera och testa fakturaalgoritm på enkelt fall

   a. för in testdata
   b. skapa faktura-ckeck algoritm
