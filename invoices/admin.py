from django.contrib import admin
from invoices.models import InvoiceArticle, Invoice, InvoiceTriggerDate


class InvoiceArticleInline(admin.TabularInline):
    model = InvoiceArticle
    extra = 1


class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'fnox_invoice_id', 'invoice_type',
                    'date', 'order', 'created', 'completed')

    list_filter = ("invoice_type", "created", "completed")

    search_fields = [
            "id",
            "fnox_invoice_id",
            "order__id",
            ]

    inlines = [
        InvoiceArticleInline
    ]


class InvoiceTriggerDateAdmin(admin.ModelAdmin):
    list_display = ('order', 'date')


# Register your models here.
admin.site.register(InvoiceTriggerDate, InvoiceTriggerDateAdmin)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(InvoiceArticle)
