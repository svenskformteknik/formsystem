# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-03-25 06:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('invoice_nr', models.IntegerField()),
                ('date', models.DateField()),
                ('invoice_address', models.CharField(max_length=128)),
                ('invoice_address2', models.CharField(blank=True, max_length=128, null=True)),
                ('invoice_postnr', models.CharField(max_length=12)),
                ('invoice_place', models.CharField(max_length=128)),
                ('invoice_land', models.CharField(max_length=128)),
                ('delivery_address', models.CharField(max_length=128)),
                ('delivery_address2', models.CharField(blank=True, max_length=128, null=True)),
                ('delivery_postnr', models.CharField(max_length=12)),
                ('delivery_place', models.CharField(max_length=128)),
                ('delivery_land', models.CharField(max_length=128)),
                ('customer_nr', models.IntegerField()),
                ('customer_referent', models.CharField(max_length=128)),
                ('customer_ordernr', models.IntegerField()),
                ('delivery_terms', models.CharField(max_length=128)),
                ('delivery_type', models.CharField(max_length=128)),
                ('vat_nr', models.IntegerField()),
                ('our_referent', models.CharField(max_length=128)),
                ('payment_terms', models.CharField(max_length=128)),
                ('expiry_date', models.DateField()),
                ('delay_interest', models.FloatField()),
                ('delivery_date', models.DateField()),
                ('our_order_nr', models.IntegerField()),
                ('our_address', models.CharField(max_length=128)),
                ('our_address2', models.CharField(max_length=128)),
                ('our_postnr', models.IntegerField()),
                ('our_place', models.CharField(max_length=128)),
                ('our_land', models.CharField(max_length=128)),
                ('our_homepage_url', models.URLField()),
                ('our_tel', models.IntegerField()),
                ('our_email', models.EmailField(max_length=254)),
                ('plusgiro', models.IntegerField()),
                ('bankgiro', models.IntegerField()),
                ('seat', models.CharField(max_length=128)),
                ('our_orgnr', models.IntegerField()),
                ('our_momsregnr', models.CharField(max_length=128)),
                ('fskatt', models.CharField(max_length=128)),
                ('iban', models.CharField(max_length=128)),
                ('bic', models.CharField(max_length=128)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.Order')),
            ],
        ),
        migrations.CreateModel(
            name='InvoiceArticle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('unit', models.IntegerField()),
                ('article_type', models.CharField(max_length=128)),
                ('rent_per_day', models.FloatField()),
                ('amount', models.IntegerField()),
                ('price', models.IntegerField()),
                ('account', models.IntegerField()),
                ('discount', models.IntegerField()),
                ('invoice', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invoices.Invoice')),
            ],
        ),
        migrations.CreateModel(
            name='InvoiceTriggerDate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.Order')),
            ],
        ),
    ]
