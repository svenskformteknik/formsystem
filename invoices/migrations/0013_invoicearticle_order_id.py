# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-04-04 13:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoices', '0012_auto_20180328_1135'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoicearticle',
            name='order_id',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
    ]
