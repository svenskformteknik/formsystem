from __future__ import absolute_import, unicode_literals
import random
import datetime
from orders.models import Order
from invoices.views import generate_period_invoice
from celery.decorators import task


@task(name="period_invoiceing")
def period_invoiceing():

    def get_period_orders():
        '''
        returns a list of all orders with
        trigger dates equal to todays date
        '''
        triggered_orders = []

        # get all ongoing orders
        active_orders = Order.objects.filter(ongoing=True)
        # loop through orders and put all orders with the last
        # trigger date equal to todays date in a list
        for order in active_orders:
            trigger_dates = order.invoicetriggerdate_set.all()
            trigger_dates.order_by("date")
            trigger_date = trigger_dates.last()
            if trigger_date:
                if trigger_date.date == datetime.date.today():
                    triggered_orders.append(order)

        return triggered_orders

    # get all orders that should be triggerd for
    # period invoiceing
    period_orders = get_period_orders()

    # create varaiable to store number of processed invoices
    # for displaying in result message
    invoices_generated = 0
    if period_orders:
        for order in period_orders:
            # generate period invoice from order
            generate_period_invoice(order)
            invoices_generated += 1

    return str(invoices_generated) + " nya periodfakturor skapade"


@task(name="sum_two_numbers")
def add(x, y):
    return x + y


@task(name="multiply_two_numbers")
def mul(x, y):
    total = x * (y * random.randint(3, 100))
    print("THIS IS MULTIPLICATION")
    return total


@task(name="sum_list_numbers")
def xsum(numbers):
    return sum(numbers)
