from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.forms import inlineformset_factory
from invoices.models import Invoice, InvoiceTriggerDate, InvoiceArticle
from invoices.forms import InvoiceForm, InvoiceArticleForm
import datetime
from orders.models import Order, OrderEvent, ApiKey
from products.models import Account
from customers.models import Customer
import requests
import json
# Create your views here.


####################################
# helper functions
####################################


def add_rent_period(invoice, order_event, date):
    invoice_article = InvoiceArticle()
    invoice_article.invoice = invoice
    invoice_article.product = "Hyrperiod: " + date
    invoice_article.price_list = ' '
    invoice_article.price_type = " "
    invoice_article.amount = 0
    invoice_article.days_rented = 0
    invoice_article.base = 0
    invoice_article.base_is_default = True
    invoice_article.price = 0
    invoice_article.total_price = 0
    invoice_article.price_is_default = True
    invoice_article.discount = 0
    invoice_article.account = 0
    invoice_article.save()


def get_api_keys():

    # get api keys from database
    fortnox_api = ApiKey.objects.filter(name="fortnox").first()
    TOKEN = fortnox_api.access_token
    SECRET = fortnox_api.client_secret

    # return keys
    return TOKEN, SECRET


def get_rented_products(order_event):
    '''Filter and return all products marked as uthyrning'''

    products = order_event.ordereventhasproduct_set.all()
    rented_products = []
    for product in products:
        if product.price_type == 'uthyrning' or\
                product.price_type == "vidareuthyrning":
            rented_products.append(product)
    return rented_products


def products_equal(prod1, prod2):
    '''Check if two products are equal
    - return True if name price, base discount and price type are equal
    '''
    print(
        str(prod1.product) + " " + str(prod1.price) + " " + str(prod1.base)
        + " " + str(prod1.discount) + " " + str(prod1.price_type))
    print(
        str(prod2.product) + " " + str(prod2.price) + " " + str(prod2.base)
        + " " + str(prod2.discount) + " " + str(prod2.price_type))
    if prod1.product == prod2.product and prod1.price == prod2.price\
            and prod1.base == prod2.base and prod1.discount == prod2.discount\
            and prod1.price_type == prod2.price_type:
        return True
    else:
        return False


def rented_out(order, latest_order_event=None):
    '''
    return a list of all products out (rented) right now.
    Also return any potential warnings for returned products
    not matching delivered products
    '''

    # if latest order_event is supplied and it is a return event
    if latest_order_event and latest_order_event.return_event:
        order_events = order.orderevent_set.all().order_by(
            "created_date").exclude(id=latest_order_event.id)
    else:
        # get order events belonging to order and sort by creation date
        order_events = order.orderevent_set.all().order_by("created_date")

    # create two empty storage varaibles
    warnings = []
    products_out = []

    # loop through all order events
    for order_event in order_events:

        # get all rented products from order event
        rented_products = get_rented_products(order_event)

        # check if order event is delivery event
        if order_event.delivery_event:

            # for each product in rented products compare to
            # products in products_out.
            for p1 in rented_products:
                found = False
                for p2 in products_out:
                    # if products are equal
                    if products_equal(p1, p2):
                        # increase amount and break out from loop
                        p2.amount += p1.amount
                        found = True
                        break

                # if product not found in products_out add
                # it to products_out
                if not found:
                    products_out.append(p1)

        # if order event is return event do the same thing as above
        # but decrease amount if match is found
        elif order_event.return_event:
            for p1 in rented_products:
                found = False
                for p2 in products_out:
                    if products_equal(p1, p2):
                        p2.amount -= p1.amount
                        found = True
                        break

                # if return product not found a match put in warning bin
                if not found:
                    warnings.append(p1)
                    print('Waring - return product not matching')

    products_out = [product for product in products_out if product.amount != 0]

    return products_out, warnings


def make_fnox_invoice(invoice):

    invoice_articles = invoice.invoicearticle_set.all()
    invoice_rows = []
    last_row = False
    for article in invoice_articles:

        if "Hyrperiod" in article.product:
            last_row = {
                    "Description": article.product,
                    "AccountNumber": 0,
                    }
        else:
            row = {
                "AccountNumber": article.account,
                "ArticleNumber": str(article.account),
                "DeliveredQuantity": 1,
                # "Discount": article.discount,
                # "DiscountType": "PERCENT",
                "Price": article.total_price,
                "Unit": "st",
                "VAT": 25
                }

            if (article.price_type == "uthyrning" or article.price_type ==
                    "vidareuthyrning") and article.days_rented != 0:
                row["Description"] = str(article.amount) +\
                        " st " + str(article.product) + " " +\
                        str(article.days_rented) + " dagar"
            else:
                row["Description"] = str(article.amount) +\
                        " st " + article.product

            invoice_rows.append(row)

    if last_row:
        invoice_rows.append(last_row)

    TOKEN, SECRET = get_api_keys()

    try:
        r = requests.post(
            url="https://api.fortnox.se/3/invoices",
            headers={
                "Access-Token": TOKEN,
                "Client-Secret": SECRET,
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
            data=json.dumps({
                "Invoice": {
                            "Address1": invoice.invoice_address,
                            "Address2": invoice.invoice_address2,
                            "City": invoice.invoice_place,
                            "CustomerNumber": invoice.customer_nr,
                            "DeliveryAddress1": invoice.delivery_address,
                            "DeliveryAddress2": invoice.delivery_address,
                            "DeliveryCity": invoice.delivery_place,
                            "DeliveryCountry": invoice.delivery_country,
                            "PriceList": "A",
                            "YourOrderNumber": invoice.order.project,
                            "DueDate": str(invoice.expiry_date),
                            "InvoiceDate": str(invoice.date),
                            "InvoiceRows": invoice_rows,
                            "OurReference": invoice.our_referent,
                            "YourReference": invoice.order.person_ordering,
                            }
            })
        )
        print('Response HTTP Status Code : {status_code}'.format(
            status_code=r.status_code))
        print('Response HTTP Response Body : {content}'.format(
            content=r.content))
    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')
        return None

    return r


def create_new_invoice(invoice_type, order):

    # create invoice, populate fields and return it
    invoice = Invoice()
    invoice.invoice_type = invoice_type
    invoice.order = order
    invoice.date = datetime.date.today()
    invoice.invoice_address = order.customer.invoice_address
    invoice.invoice_address2 = order.customer.invoice_address2
    invoice.invoice_postnr = order.customer.invoice_postnr
    invoice.invoice_place = order.customer.invoice_place
    invoice.invoice_land = order.customer.invoice_country
    invoice.delivery_address = order.work_address
    invoice.delivery_address2 = order.work_address2
    invoice.delivery_postnr = order.work_postnr
    invoice.delivery_place = order.work_place
    invoice.delivery_land = "Sverige"
    invoice.customer_nr = order.customer.fortnox_id
    # primary_referent = order.customer.referent_set.filter
    # (primary=True).first()
    # if primary_referent:
    #    invoice.customer_referent = primary_referent.surname + " " +\
    #        primary_referent.lastname
    # else:
    #    invoice.customer_referent = "Ingen referent"
    invoice.customer_referent = order.person_ordering
    invoice.customer_ordernr = 0
    invoice.delivery_terms = "None"
    invoice.delivery_type = "None"
    invoice.vat_nr = 0
    invoice.our_referent = order.our_referent
    invoice.payment_terms = order.customer.payment_terms
    invoice.expiry_date = datetime.date.today() + datetime.timedelta(
        days=30)
    invoice.delay_interest = 0
    invoice.delivery_date = datetime.date.today()
    invoice.our_order_nr = 0
    invoice.save()

    return invoice


def add_invoice_article(order_type,
                        article_type,
                        product, current_date,
                        invoice, order_event=None):
    """Adds an article to a invoice"""

    # create invoice article and populate it with information
    invoice_article = InvoiceArticle()
    invoice_article.invoice = invoice
    invoice_article.product = product.product
    invoice_article.price_list = product.price_list
    invoice_article.discount = product.discount
    invoice_article.discount_is_default = product.discount_is_default
    discount_fraction = 1-(invoice_article.discount/100)

    if order_type == 'pre_payment':
        if article_type == "uthyrning":
            invoice_article.price_type = "uthyrning"
            invoice_article.order_id = None
        elif article_type == "vidareuthyrning":
            invoice_article.price_type = "vidareuthyrning"
            invoice_article.order_id = None

    else:
        invoice_article.price_type = product.price_type
        invoice_article.order_id = order_event.id

    invoice_article.amount = product.amount

    if order_type == "delivery" and article_type == 'rented':
        invoice_article.days_rented = (
            current_date - order_event.rent_start_date).days
    elif order_type == 'delivery' and article_type == 'rented_forward':
        invoice_article.days_rented = (
            current_date - order_event.rent_start_date).days
    elif order_type == 'delivery' and article_type == 'sold':
        invoice_article.days_rented = 0
    elif order_type == 'delivery' and article_type == 'sold_consumption':
        invoice_article.days_rented = 0

    elif order_type == 'return' and article_type == 'rented':
        invoice_article.days_rented = (
            current_date - order_event.rent_stop_date).days
    elif order_type == 'return' and article_type == 'rented_forward':
        invoice_article.days_rented = (
            current_date - order_event.rent_stop_date).days
    elif order_type == "return" and article_type == 'compensation':
        invoice_article.days_rented = 0

    elif order_type == 'pre_payment':
        invoice_article.days_rented = 30

    if order_type == "pre_payment":
        invoice_article.base = 0
    else:
        invoice_article.base = product.base

    invoice_article.base_is_default = product.base_is_default
    invoice_article.price = product.price
    invoice_article.price_is_default = product.price_is_default

    if order_type == "delivery" and article_type == 'rented':
        invoice_article.total_price = round(product.amount*(
            product.price)*invoice_article.days_rented
                    * discount_fraction, 2)
    elif order_type == "delivery" and article_type == 'rented_forward':
        invoice_article.total_price = round(product.amount*(
            product.price)*invoice_article.days_rented
                    * discount_fraction, 2)
    elif order_type == 'delivery' and article_type == 'sold':
        invoice_article.total_price = round(
                product.amount*product.price*discount_fraction, 2)
    elif order_type == 'delivery' and article_type == 'sold_consumption':
        invoice_article.total_price = round(
                product.amount*product.price*discount_fraction, 2)

    elif order_type == 'return' and article_type == 'rented':
        invoice_article.total_price = round(-(
            product.amount*(
                product.price)
            * invoice_article.days_rented*discount_fraction), 2)
    elif order_type == "return" and article_type == 'rented_forward':
        invoice_article.total_price = round(-(
                product.amount*(
                    product.price)
                * invoice_article.days_rented*discount_fraction), 2)
    elif order_type == "return" and article_type == 'compensation':
        invoice_article.total_price = round(
                product.amount*product.price*discount_fraction, 2)

    elif order_type == "pre_payment":
        invoice_article.total_price = round(
                product.amount*product.price
                * invoice_article.days_rented
                * discount_fraction, 2)

    if order_type == "pre_payment":
        if article_type == "uthyrning":
            invoice_article.account = Account.objects.get(
                    account_type="uthyrning").account_nr
        elif article_type == "vidareuthyrning":
            invoice_article.account = Account.objects.get(
                    account_type="vidareuthyrning").account_nr
    else:
        invoice_article.account = product.account

    invoice_article.save()


def generate_period_invoice(order):
    '''
    Creates a period invoice and fills it up with
    payments, return payments and prepayments
    '''

    # create invoice, marked with period
    invoice = create_new_invoice('period', order)

    # get list of tigger dates and select the two last ones
    trigger_dates = list(order.invoicetriggerdate_set.all().order_by('date'))
    prev_trigger_date = trigger_dates[-2].date
    last_trigger_date = trigger_dates[-1].date

    # get all delivery events between previous_date
    # and last_trigger_date that are invoiceable for products
    deliveries = order.orderevent_set.filter(
        rent_start_date__range=(
            prev_trigger_date, last_trigger_date)).filter(
                    products_invoiceable=True)
    deliveries = deliveries.filter(delivery_event=True)

    # get all return events between previous_date and
    # last_trigger_date that are invoiceable for products
    returns = order.orderevent_set.filter(
        rent_stop_date__range=(
            prev_trigger_date, last_trigger_date)).filter(
                    products_invoiceable=True)
    returns = returns.filter(return_event=True)

    # if there are delivery events
    if deliveries:

        # loop through delivery events and filter
        # out the different product categories
        for order_event in deliveries:
            rented_products = order_event.ordereventhasproduct_set.filter(
                    price_type="uthyrning")
            rented_forward_products = order_event\
                .ordereventhasproduct_set.filter(
                    price_type="vidareuthyrning")
            sold_products = order_event.ordereventhasproduct_set.filter(
                    price_type="försäljning")
            sold_consumption_products = order_event\
                .ordereventhasproduct_set.filter(
                    price_type="försäljning förbrukningsmaterial")

            # if there are rented products add them to the invoice
            if rented_products:
                for product in rented_products:
                    add_invoice_article('delivery',
                                        'rented',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            if rented_forward_products:
                for product in rented_forward_products:
                    add_invoice_article('delivery',
                                        'rented_forward',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            # if there are sold products add them to the invoice
            if sold_products:
                for product in sold_products:
                    add_invoice_article('delivery',
                                        'sold',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            # if there are sold products add them to the invoice
            if sold_consumption_products:
                for product in sold_consumption_products:
                    add_invoice_article('delivery',
                                        'sold_consumption',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            if not order_event.base_total_rent == 0:
                invoice_article = InvoiceArticle()
                invoice_article.invoice = invoice
                invoice_article.product = "grundhyra"
                invoice_article.order_id = order_event.id
                invoice_article.price_list = " "
                invoice_article.price_type = "uthyrning"
                invoice_article.amount = 1
                invoice_article.days_rented = 0
                invoice_article.base = 0
                invoice_article.base_is_default = True
                invoice_article.price = order_event.base_total_rent
                invoice_article.price_is_default = True
                invoice_article.discount = 0
                invoice_article.discount_is_default = True
                discount_fraction = 1-(invoice_article.discount/100)

                invoice_article.total_price =\
                    round(order_event.base_total_rent * discount_fraction, 2)

                account = Account.objects.filter(
                        account_type="uthyrning").first()
                invoice_article.account = account.account_nr
                invoice_article.save()

            if not order_event.base_total_forward == 0:
                invoice_article = InvoiceArticle()
                invoice_article.invoice = invoice
                invoice_article.product = "grundhyra"
                invoice_article.order_id = order_event.id
                invoice_article.price_list = " "
                invoice_article.price_type = "vidareuthyrning"
                invoice_article.amount = 1
                invoice_article.days_rented = 0
                invoice_article.base = 0
                invoice_article.base_is_default = True
                invoice_article.price = order_event.base_total_forward
                invoice_article.price_is_default = True
                invoice_article.discount = 0
                invoice_article.discount_is_default = True
                discount_fraction = 1-(invoice_article.discount/100)

                invoice_article.total_price =\
                    round(
                        order_event.base_total_forward * discount_fraction, 2)

                account = Account.objects.filter(
                        account_type="vidareuthyrning").first()
                invoice_article.account = account.account_nr
                invoice_article.save()

            # set status on order event as not invoiceable and invoiced
            order_event.products_invoiceable = False
            order_event.products_invoiced = True
            order_event.save()

    # if there are return events
    if returns:

        # loop through return events and filter out
        # the different product categories
        for order_event in returns:
            rented_products = order_event.ordereventhasproduct_set.filter(
                    price_type="uthyrning")
            rented_forward_products = order_event.\
                ordereventhasproduct_set.filter(price_type="vidareuthyrning")
            compensation_products = order_event.\
                ordereventhasproduct_set.filter(price_type="ersättning")

            # if there are rented products add them to the invoice
            if rented_products:
                for product in rented_products:
                    add_invoice_article('return',
                                        'rented',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            if rented_forward_products:
                for product in rented_forward_products:
                    add_invoice_article('return',
                                        'rented_forward',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            # if there are replaced products add them to the invoice
            if compensation_products:
                for product in compensation_products:
                    add_invoice_article('return',
                                        'compensation',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            # set status on order event as not invoiceable and invoiced
            order_event.products_invoiceable = False
            order_event.products_invoiced = True
            order_event.save()

    # get all products that are rented right now and add pre payment to invoice
    products_out, warnings = rented_out(order)

    for product in products_out:
        if product.price_type == "uthyrning":
            add_invoice_article("pre_payment",
                                "uthyrning",
                                product,
                                last_trigger_date,
                                invoice)
        elif product.price_type == "vidareuthyrning":
            add_invoice_article("pre_payment",
                                "vidareuthyrning",
                                product,
                                last_trigger_date,
                                invoice)

    # get a list of all delivery event between previous_date
    # and last_trigger_date that are invoiceable for shipping
    delivery_shippings = order.orderevent_set.filter(
        rent_start_date__range=(prev_trigger_date, last_trigger_date)).filter(
                shipping_invoiceable=True)
    delivery_shippings = delivery_shippings.filter(delivery_event=True)

    # get a list of all return event between previous_date
    # and last_trigger_date that are invoiceable for shipping
    return_shippings = order.orderevent_set.filter(
        rent_stop_date__range=(prev_trigger_date, last_trigger_date)).filter(
                shipping_invoiceable=True)
    return_shippings = return_shippings.filter(return_event=True)

    # put all shipping events in one list
    shippings = []
    for order_event in delivery_shippings:
        shippings.append(order_event)
    for order_event in return_shippings:
        shippings.append(order_event)

    # loop through all order events with invoiceable shipping
    for order_event in shippings:
        # if transport is egen ignore and set status on order event
        if order_event.transport.transport_type.name == "egen":
            order_event.shipping_invoiceable = False
            order_event.shipping_invoiced = True
            order_event.save()

        # else add shipping to invoice
        else:
            invoice_article = InvoiceArticle()
            invoice_article.invoice = invoice
            invoice_article.product = order_event.transport.transport_type.name
            invoice_article.price_list = ' '
            invoice_article.price_type = "frakt"
            invoice_article.amount = 1
            invoice_article.days_rented = 0
            invoice_article.base = 0
            invoice_article.base_is_default = True
            invoice_article.price = order_event.transport.price
            invoice_article.total_price = order_event.transport.price
            invoice_article.price_is_default = True
            invoice_article.account = Account.objects.filter(
                    account_type="frakt").first().account_nr
            invoice_article.discount = 0
            invoice_article.order_id = order_event.id
            invoice_article.save()

            # set status on order event
            order_event.shipping_invoiceable = False
            order_event.shipping_invoiced = True
            order_event.save()

    # check all order events and mark as completed if
    # both shipping and products are invoiced
    all_order_events = order.orderevent_set.all()
    for order_event in all_order_events:
        if order_event.products_invoiced and order_event.shipping_invoiced:
            order_event.ongoing = False
            order_event.completed = True
            order_event.save()

    # generate new trigger date (+30 days)
    new_trigger_date = InvoiceTriggerDate()
    new_trigger_date.order = order
    new_trigger_date.date = last_trigger_date + datetime.timedelta(
        days=30)
    new_trigger_date.save()

    order.invoiced_to = new_trigger_date.date
    order.save()

    # set rent period
    date = str(last_trigger_date) + " - " + str(new_trigger_date)
    add_rent_period(invoice, order_event, date)


def generate_end_invoice(order):
    '''
    Creates end invoice and fills it up with
    payments, return payments and prepayments
    '''

    # create invoice, marked with slut
    invoice = create_new_invoice('slut', order)

    # get list of tigger dates and select the two last ones
    trigger_dates = list(order.invoicetriggerdate_set.all().order_by('date'))
    prev_trigger_date = trigger_dates[-2].date
    last_trigger_date = trigger_dates[-1].date

    # get all delivery events between previous_date
    # and last_trigger_date that are invoiceable for products
    deliveries = order.orderevent_set.filter(
        rent_start_date__range=(prev_trigger_date, last_trigger_date)).filter(
                products_invoiceable=True)
    deliveries = deliveries.filter(delivery_event=True)

    # get all return events between previous_date
    # and last_trigger_date that are invoiceable for products
    returns = order.orderevent_set.filter(
        rent_stop_date__range=(prev_trigger_date, last_trigger_date)).filter(
                products_invoiceable=True)
    returns = returns.filter(return_event=True)

    # if there are delivery events
    if deliveries:

        # loop through delivery events
        # and filter out the different product categories
        for order_event in deliveries:
            rented_products = order_event.ordereventhasproduct_set.filter(
                    price_type="uthyrning")
            rented_forward_products = order_event.\
                ordereventhasproduct_set.filter(price_type="vidareuthyrning")
            sold_products = order_event.ordereventhasproduct_set.filter(
                    price_type="försäljning")
            sold_consumption_products = order_event\
                .ordereventhasproduct_set.filter(
                        price_type="försäljning förbrukningsmaterial")

            # if there are rented products add them to the invoice
            if rented_products:
                for product in rented_products:
                    add_invoice_article('delivery',
                                        'rented',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            if rented_forward_products:
                for product in rented_forward_products:
                    add_invoice_article('delivery',
                                        'rented_forward',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            # if there are sold products add them to the invoice
            if sold_products:
                for product in sold_products:
                    add_invoice_article('delivery',
                                        'sold',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            # if there are sold products add them to the invoice
            if sold_consumption_products:
                for product in sold_consumption_products:
                    add_invoice_article('delivery',
                                        'sold_consumption',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            if not order_event.base_total_rent == 0:
                invoice_article = InvoiceArticle()
                invoice_article.invoice = invoice
                invoice_article.product = "grundhyra"
                invoice_article.order_id = order_event.id
                invoice_article.price_list = " "
                invoice_article.price_type = "uthyrning"
                invoice_article.amount = 1
                invoice_article.days_rented = 0
                invoice_article.base = 0
                invoice_article.base_is_default = True
                invoice_article.price = order_event.base_total_rent
                invoice_article.price_is_default = True
                invoice_article.discount = 0
                invoice_article.discount_is_default = True
                discount_fraction = 1-(invoice_article.discount/100)

                invoice_article.total_price =\
                    round(order_event.base_total_rent * discount_fraction, 2)

                account = Account.objects.filter(
                        account_type="uthyrning").first()
                invoice_article.account = account.account_nr
                invoice_article.save()

            if not order_event.base_total_forward == 0:
                invoice_article = InvoiceArticle()
                invoice_article.invoice = invoice
                invoice_article.product = "grundhyra"
                invoice_article.order_id = order_event.id
                invoice_article.price_list = " "
                invoice_article.price_type = "vidareuthyrning"
                invoice_article.amount = 1
                invoice_article.days_rented = 0
                invoice_article.base = 0
                invoice_article.base_is_default = True
                invoice_article.price = order_event.base_total_forward
                invoice_article.price_is_default = True
                invoice_article.discount = 0
                invoice_article.discount_is_default = True
                discount_fraction = 1-(invoice_article.discount/100)

                invoice_article.total_price =\
                    round(
                        order_event.base_total_forward * discount_fraction, 2)

                account = Account.objects.filter(
                        account_type="vidareuthyrning").first()
                invoice_article.account = account.account_nr
                invoice_article.save()

            # set status on order event as not invoiceable and invoiced
            order_event.products_invoiceable = False
            order_event.products_invoiced = True
            order_event.save()

    # if there are return events
    if returns:

        # loop through return events
        # and filter out the different product categories
        for order_event in returns:
            rented_products = order_event.ordereventhasproduct_set.filter(
                    price_type="uthyrning")
            rented_forward_products = order_event.\
                ordereventhasproduct_set.filter(price_type="vidareuthyrning")
            compensation_products = order_event.\
                ordereventhasproduct_set.filter(price_type="ersättning")

            # if there are rented products add them to the invoice
            if rented_products:
                for product in rented_products:
                    add_invoice_article('return',
                                        'rented',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)

            if rented_forward_products:
                for product in rented_forward_products:
                    add_invoice_article('return',
                                        'rented_forward',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)
            # if there are replaced products add them to the invoice

            if compensation_products:
                for product in compensation_products:
                    add_invoice_article('return',
                                        'compensation',
                                        product,
                                        last_trigger_date,
                                        invoice,
                                        order_event)
            # set status on order event as not invoiceable and invoiced

            order_event.products_invoiceable = False
            order_event.products_invoiced = True
            order_event.save()

    # get all products that are rented right now and add pre payment to invoice
    products_out, warnings = rented_out(order)

    for product in products_out:
        add_invoice_article("pre_payment",
                            "uthyrning",
                            product,
                            last_trigger_date,
                            invoice)

    # get a list of all delivery event between previous_date
    # and last_trigger_date that are invoiceable for shipping
    delivery_shippings = order.orderevent_set.filter(
        rent_start_date__range=(prev_trigger_date, last_trigger_date)).filter(
                shipping_invoiceable=True)
    delivery_shippings = delivery_shippings.filter(delivery_event=True)

    # get a list of all return event between previous_date
    # and last_trigger_date that are invoiceable for shipping
    return_shippings = order.orderevent_set.filter(
        rent_stop_date__range=(prev_trigger_date, last_trigger_date)).filter(
                shipping_invoiceable=True)
    return_shippings = return_shippings.filter(return_event=True)

    # put all shipping events in one list
    shippings = []
    for order_event in delivery_shippings:
        shippings.append(order_event)
    for order_event in return_shippings:
        shippings.append(order_event)
    # loop through all order events with invoiceable shipping

    for order_event in shippings:

        # if transport is egen ignore and set status on order event
        if order_event.transport.transport_type.name == "egen":
            order_event.shipping_invoiceable = False
            order_event.shipping_invoiced = True
            order_event.save()

        # else add shipping to invoice
        else:
            invoice_article = InvoiceArticle()
            invoice_article.invoice = invoice
            invoice_article.product = order_event.transport.transport_type.name
            invoice_article.price_list = ' '
            invoice_article.price_type = "frakt"
            invoice_article.amount = 1
            invoice_article.days_rented = 0
            invoice_article.base = 0
            invoice_article.base_is_default = True
            invoice_article.price = order_event.transport.price
            invoice_article.total_price = order_event.transport.price
            invoice_article.price_is_default = True
            invoice_article.account = Account.objects.filter(
                    account_type="frakt").first().account_nr
            invoice_article.discount = 0
            invoice_article.order_id = order_event.id
            invoice_article.save()

            # set status on order event
            order_event.shipping_invoiceable = False
            order_event.shipping_invoiced = True
            order_event.save()

    # check all order events and mark as completed if
    # both shipping and products are invoiced
    all_order_events = order.orderevent_set.all()
    for order_event in all_order_events:
        if order_event.products_invoiced and order_event.shipping_invoiced:
            order_event.ongoing = False
            order_event.completed = True
            order_event.save()

    unfinished_order_events = order.orderevent_set.filter(
            ongoing=True) | order.orderevent_set.filter(
                    completed=False)

    if not unfinished_order_events:
        order.ongoing = False
        order.completed = True
        order.save()

    # set invoiced to to the last order events rent stop date
    order_events = order.orderevent_set.exclude(rent_stop_date=None)
    order_events_sorted = order_events.order_by("rent_stop_date")
    last_order_event = order_events_sorted.last()
    order.invoiced_to = last_order_event.rent_stop_date
    order.save()

    # set rent period
    date = str(last_trigger_date) + " - " +\
        str(last_order_event.rent_stop_date)
    add_rent_period(invoice, order_event, date)

    return invoice.id

###################################
# views
###################################


def create_start_invoice(request, order_event_id):

    # get order event fomr order event id
    order_event = OrderEvent.objects.get(pk=order_event_id)
    # get order object from order_event
    order = order_event.order

    # create invoice and fill it with data
    invoice = Invoice()
    invoice.invoice_type = "uppstart"
    invoice.order = order
    invoice.date = datetime.date.today()
    invoice.invoice_address = order.customer.invoice_address
    invoice.invoice_address2 = order.customer.invoice_address2
    invoice.invoice_postnr = order.customer.invoice_postnr
    invoice.invoice_place = order.customer.invoice_place
    invoice.invoice_land = order.customer.invoice_country
    invoice.delivery_address = order.work_address
    invoice.delivery_address2 = order.work_address2
    invoice.delivery_postnr = order.work_postnr
    invoice.delivery_place = order.work_place
    invoice.delivery_land = "Sverige"
    invoice.customer_nr = order.customer.fortnox_id
    #  primary_referent = order.customer.referent_set.filter
    # (primary=True).first()
    #  if primary_referent:
    #      invoice.customer_referent = primary_referent.surname + " " +\
    #          primary_referent.lastname
    #  else:
    #      invoice.customer_referent = "Ingen referent"
    invoice.customer_referent = order.person_ordering
    invoice.customer_ordernr = 0
    invoice.delivery_terms = "None"
    invoice.delivery_type = "None"
    invoice.vat_nr = 0
    invoice.our_referent = order.our_referent
    invoice.payment_terms = order.customer.payment_terms
    invoice.expiry_date = datetime.date.today() + datetime.timedelta(
        days=30)
    invoice.delay_interest = 0
    invoice.delivery_date = datetime.date.today()
    invoice.our_order_nr = 0
    # save invoice to database
    invoice.save()

    # set a new trigger date at start of rent period
    trigger_date = InvoiceTriggerDate()
    trigger_date.order = order_event.order
    trigger_date.date = order_event.rent_start_date
    trigger_date.save()

    # set next trigger date at +30 days
    new_trigger_date = InvoiceTriggerDate()
    new_trigger_date.order = order_event.order
    new_trigger_date.date = order_event.rent_start_date + datetime.timedelta(
        days=30)
    trigger_date.save()
    new_trigger_date.save()

    # get articles and divide them into their types
    rented_products = order_event.ordereventhasproduct_set.filter(
        price_type="uthyrning")
    sold_products = order_event.ordereventhasproduct_set.filter(
        price_type="försäljning")
    sold_consumption_products = order_event.ordereventhasproduct_set.filter(
        price_type="försäljning förbrukningsmaterial")
    rented_forward_products = order_event.ordereventhasproduct_set.filter(
        price_type="vidareuthyrning")

    # go through each category and add articles to invoice
    if rented_products:

        # go through rented products and add to invoice
        for product in rented_products:
            invoice_article = InvoiceArticle()
            invoice_article.invoice = invoice
            invoice_article.product = product.product
            invoice_article.order_id = order_event.id

            if product.price_type == 'uthyrning':
                invoice_article.price_type = "uthyrning"
            else:
                invoice_article.price_type = product.price_type

            invoice_article.price_list = product.price_list
            invoice_article.amount = product.amount
            invoice_article.days_rented = 30
            invoice_article.base = product.base
            invoice_article.base_is_default = product.base_is_default
            invoice_article.price = product.price
            invoice_article.price_is_default = product.price_is_default
            invoice_article.discount = product.discount
            invoice_article.discount_is_default = product.discount_is_default
            discount_fraction = 1-(invoice_article.discount/100)
            invoice_article.total_price =\
                round(product.amount*(
                    product.price) *
                    invoice_article.days_rented*discount_fraction, 2)

            invoice_article.account = product.account
            invoice_article.save()

        order_event.products_invoiced = True
        order_event.products_invoiceable = False
        order_event.save()

    if sold_products:

        for product in sold_products:
            invoice_article = InvoiceArticle()
            invoice_article.invoice = invoice
            invoice_article.product = product.product
            invoice_article.order_id = order_event.id
            invoice_article.price_list = product.price_list
            invoice_article.price_type = product.price_type
            invoice_article.amount = product.amount
            invoice_article.days_rented = 0
            invoice_article.base = product.base
            invoice_article.base_is_default = product.base_is_default
            invoice_article.price = product.price
            invoice_article.price_is_default = product.price_is_default
            invoice_article.discount = product.discount
            invoice_article.discount_is_default = product.discount_is_default
            discount_fraction = 1-(invoice_article.discount/100)

            invoice_article.total_price =\
                round(product.amount * product.price * discount_fraction, 2)

            invoice_article.account = product.account
            invoice_article.save()

        order_event.products_invoiced = True
        order_event.products_invoiceable = False
        order_event.save()

    if sold_consumption_products:

        for product in sold_consumption_products:
            invoice_article = InvoiceArticle()
            invoice_article.invoice = invoice
            invoice_article.product = product.product
            invoice_article.order_id = order_event.id
            invoice_article.price_list = product.price_list
            invoice_article.price_type = product.price_type
            invoice_article.amount = product.amount
            invoice_article.days_rented = 0
            invoice_article.base = product.base
            invoice_article.base_is_default = product.base_is_default
            invoice_article.price = product.price
            invoice_article.price_is_default = product.price_is_default
            invoice_article.discount = product.discount
            invoice_article.discount_is_default = product.discount_is_default
            discount_fraction = 1-(invoice_article.discount/100)

            invoice_article.total_price =\
                round(product.amount * product.price * discount_fraction, 2)

            invoice_article.account = product.account
            invoice_article.save()

        order_event.products_invoiced = True
        order_event.products_invoiceable = False
        order_event.save()

    if rented_forward_products:

        # go through rented products and add to invoice
        for product in rented_forward_products:
            invoice_article = InvoiceArticle()
            invoice_article.invoice = invoice
            invoice_article.product = product.product
            invoice_article.order_id = order_event.id

            if product.price_type == 'vidareuthyrning':
                invoice_article.price_type = "vidareuthyrning"
            else:
                invoice_article.price_type = product.price_type

            invoice_article.price_list = product.price_list
            invoice_article.amount = product.amount
            invoice_article.days_rented = 30
            invoice_article.base = product.base
            invoice_article.base_is_default = product.base_is_default
            invoice_article.price = product.price
            invoice_article.price_is_default = product.price_is_default
            invoice_article.discount = product.discount
            invoice_article.discount_is_default = product.discount_is_default
            discount_fraction = 1-(invoice_article.discount/100)
            invoice_article.total_price =\
                round(product.amount * (
                    product.price)
                        * invoice_article.days_rented*discount_fraction, 2)

            invoice_article.account = product.account
            invoice_article.save()

        order_event.products_invoiced = True
        order_event.products_invoiceable = False
        order_event.save()

    if not order_event.base_total_rent == 0:
        invoice_article = InvoiceArticle()
        invoice_article.invoice = invoice
        invoice_article.product = "grundhyra"
        invoice_article.order_id = order_event.id
        invoice_article.price_list = " "
        invoice_article.price_type = "uthyrning"
        invoice_article.amount = 1
        invoice_article.days_rented = 0
        invoice_article.base = 0
        invoice_article.base_is_default = True
        invoice_article.price = order_event.base_total_rent
        invoice_article.price_is_default = True
        invoice_article.discount = 0
        invoice_article.discount_is_default = True
        discount_fraction = 1-(invoice_article.discount/100)

        invoice_article.total_price =\
            round(order_event.base_total_rent * discount_fraction, 2)

        account = Account.objects.filter(account_type="uthyrning").first()
        invoice_article.account = account.account_nr
        invoice_article.save()

    if not order_event.base_total_forward == 0:
        invoice_article = InvoiceArticle()
        invoice_article.invoice = invoice
        invoice_article.product = "grundhyra"
        invoice_article.order_id = order_event.id
        invoice_article.price_list = " "
        invoice_article.price_type = "vidareuthyrning"
        invoice_article.amount = 1
        invoice_article.days_rented = 0
        invoice_article.base = 0
        invoice_article.base_is_default = True
        invoice_article.price = order_event.base_total_forward
        invoice_article.price_is_default = True
        invoice_article.discount = 0
        invoice_article.discount_is_default = True
        discount_fraction = 1-(invoice_article.discount/100)

        invoice_article.total_price =\
            round(order_event.base_total_forward * discount_fraction, 2)

        account = Account.objects.filter(
                account_type="vidareuthyrning").first()
        invoice_article.account = account.account_nr
        invoice_article.save()

    if order_event.shipping_invoiceable:
        # if transport is egen ignore and set status on order event
        if order_event.transport.transport_type.name == "egen":
            order_event.shipping_invoiceable = False
            order_event.shipping_invoiced = True
            order_event.save()

        # else add shipping to invoice
        else:
            invoice_article = InvoiceArticle()
            invoice_article.invoice = invoice
            invoice_article.product = order_event.transport.transport_type.name
            invoice_article.price_list = ' '
            invoice_article.price_type = "frakt"
            invoice_article.amount = 1
            invoice_article.days_rented = 0
            invoice_article.base = 0
            invoice_article.base_is_default = True
            invoice_article.price = order_event.transport.price
            invoice_article.total_price = round(order_event.transport.price, 2)
            invoice_article.price_is_default = True
            invoice_article.account = Account.objects.filter(
                account_type="frakt").first().account_nr
            invoice_article.discount = 0
            invoice_article.order_id = order_event.id
            invoice_article.save()

            # set status on order event
            order_event.shipping_invoiceable = False
            order_event.shipping_invoiced = True
            order_event.save()

    # set invoiced to as rent start date
    order.invoiced_to = new_trigger_date.date
    order.save()

    # set rent period
    date = str(trigger_date) + " - " + str(new_trigger_date)
    add_rent_period(invoice, order_event, date)

    return HttpResponseRedirect(
                    reverse('view_invoice', kwargs={'invoice_id': invoice.id}))


def edit_invoice(request, invoice_id):

    IaInlineFormset = inlineformset_factory(
        Invoice,
        InvoiceArticle,
        form=InvoiceArticleForm,
        extra=0
    )

    invoice = Invoice.objects.get(pk=invoice_id)

    if request.method == "POST":

        invoice_form = InvoiceForm(request.POST, instance=invoice)

        articles_formset = IaInlineFormset(request.POST, instance=invoice)

        if invoice_form.is_valid() and articles_formset.is_valid():

            instance = invoice_form.save()

            invoice = Invoice.objects.get(pk=instance.pk)

            articles_formset = IaInlineFormset(request.POST, instance=invoice)

            if articles_formset.is_valid():

                articles_formset.save()

                return HttpResponseRedirect(
                    reverse('view_invoice', kwargs={
                        'invoice_id': invoice.id}))

        else:
            print(invoice_form.errors)
            print(articles_formset.errors)

    else:

        invoice_form = InvoiceForm(instance=invoice)

        articles_formset = IaInlineFormset(
            instance=invoice, queryset=InvoiceArticle.objects.all(
                ).order_by("order_id"))

    return render(request, "edit_invoice.html",
                  {"invoice": invoice,
                   "invoice_form": invoice_form,
                   "articles_formset": articles_formset})


def create_end_invoice(request, order_id):

    order = Order.objects.get(pk=order_id)
    invoice_id = generate_end_invoice(order)

    return HttpResponseRedirect(
                    reverse('view_invoice', kwargs={'invoice_id': invoice_id}))


def view_invoice(request, invoice_id):

    invoice = Invoice.objects.get(pk=invoice_id)
    order = Order.objects.get(pk=invoice.order.id)

    articles = invoice.invoicearticle_set.all().order_by("order_id")

    for article in articles:
        if article.order_id:
            oe = OrderEvent.objects.get(pk=article.order_id)
            article.oe_info = oe.info

    excl_moms = 0
    moms = 0
    pay = 0
    for article in articles:
        excl_moms += article.total_price

    excl_moms = round(excl_moms, 2)
    moms = round(excl_moms*0.25, 2)
    pay = round(excl_moms + moms, 2)

    return render(request, "view_invoice.html",
                  {"invoice": invoice,
                   "order": order,
                   "articles": articles,
                   "excl_moms": excl_moms,
                   "moms": moms,
                   "pay": pay})


def order_invoices(request, order_id):

    order = Order.objects.get(pk=order_id)

    invoices = order.invoice_set.all()

    invoiceable_shippings = order.orderevent_set.filter(
        shipping_invoiceable=True)

    return render(request, "order_invoices.html",
                  {"order": order,
                   "navbar": "invoices",
                   "invoices": invoices,
                   "invoiceable_shippings": invoiceable_shippings})


def invoices(request):

    if request.method == "POST":
        customer = request.POST["invoice_customer"]
        customer = Customer.objects.filter(name=customer)
        start_date = request.POST["invoice_start_date"]

        if start_date:
            start_date = datetime.datetime.strptime(
                    start_date, "%Y-%m-%d").date()

        end_date = request.POST["invoice_end_date"]

        if end_date:
            end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

        completed = request.POST.getlist("completed")

        if customer:
            invoices = Invoice.objects.filter(
                    order__customer=customer).order_by("date")
        else:
            invoices = Invoice.objects.all().order_by("date")

        if start_date and end_date:
            invoices = invoices.filter(date__range=(start_date, end_date))

        if completed:
            invoices = invoices.filter(completed=True)

    else:
        invoices = Invoice.objects.all().order_by("date")

    context_dict = {
                    "invoices": invoices,
                    }

    return render(request, "invoices.html", context_dict)

###############################################################################
# API calls
###############################################################################


def add_shipping_to_invoice(request):

    # get data from POST
    order_event_id = request.POST["order_event_id"]
    invoice_id = request.POST["invoice_id"]

    # get order event and invoice from id
    order_event = OrderEvent.objects.get(pk=order_event_id)
    invoice = Invoice.objects.get(pk=invoice_id)

    # add invoice article for shipping to invoice
    invoice_article = InvoiceArticle()
    invoice_article.invoice = invoice
    invoice_article.product = order_event.transport.transport_type.name
    invoice_article.price_list = ' '
    invoice_article.price_type = "frakt"
    invoice_article.amount = 1
    invoice_article.days_rented = 0
    invoice_article.base = 0
    invoice_article.base_is_default = False
    invoice_article.price = order_event.transport.price
    invoice_article.total_price = order_event.transport.price
    invoice_article.price_is_default = False
    invoice_article.account = Account.objects.filter(
        account_type="frakt").first().account_nr
    invoice_article.discount = 0
    invoice_article.order_id = order_event.id
    invoice_article.save()

    # set status on order event
    order_event.shipping_invoiceable = False
    order_event.shipping_invoiced = True
    order_event.save()

    return HttpResponse(1, content_type='application/json')


def manual_period_invoice(request, order_id):

    order = Order.objects.get(pk=order_id)
    generate_period_invoice(order)

    return HttpResponseRedirect(
                    reverse('order_invoices', kwargs={'order_id': order_id}))


def test_period_invoice(request, order_id):

    order = Order.objects.get(pk=order_id)
    generate_period_invoice(order)

    return HttpResponseRedirect(
                    reverse('order_invoices', kwargs={'order_id': order_id}))


def send_invoice_fnox(request, invoice_id):

    invoice = Invoice.objects.get(pk=invoice_id)
    r = make_fnox_invoice(invoice)

    if r.request:
        if r.status_code == 201:
            data = r.json()
            fnox_invoice_id = data["Invoice"]["DocumentNumber"]
            invoice.fnox_invoice_id = fnox_invoice_id
            invoice.completed = True
            invoice.created = False
            invoice.save()

            return HttpResponse(r.status_code, content_type='application/json')
        else:
            return HttpResponse(r.content, content_type='application/json')
    else:
        return HttpResponse(0, content_type='application/json')


def send_all_fnox_invoices(request):

    error_invoices = []
    invoices = Invoice.objects.filter(created=True)
    for invoice in invoices:
        r = make_fnox_invoice(invoice)

        if r.request:
            if r.status_code == 201:
                data = r.json()
                fnox_invoice_id = data["Invoice"]["DocumentNumber"]
                invoice.fnox_invoice_id = fnox_invoice_id
                invoice.completed = True
                invoice.created = False
                invoice.save()
            else:
                print("error to invoice")
                rjson = r.json()
                error_invoices.append(
                        {
                         "invoice": invoice.id,
                         "error": rjson["ErrorInformation"],
                        }
                        )
        else:
            print("else")

    print(error_invoices)
    error_invoices = json.dumps(error_invoices)

    return HttpResponse(error_invoices, content_type='application/json')
