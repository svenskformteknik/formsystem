from django.conf.urls import url
from invoices import views

urlpatterns = [

    url(r'^edit_invoice/(?P<invoice_id>[\w\-]+)/$', views.edit_invoice,
        name='edit_invoice'),
    url(r'^view_invoice/(?P<invoice_id>[\w\-]+)/$', views.view_invoice,
        name='view_invoice'),
    url(r'^order_invoices/(?P<order_id>[\w\-]+)/$', views.order_invoices,
        name='order_invoices'),
    url(r'^create_start_invoice/(?P<order_event_id>[\w\-]+)/$',
        views.create_start_invoice, name='create_start_invoice'),
    url(r'^create_end_invoice/(?P<order_id>[\w\-]+)/$',
        views.create_end_invoice, name='create_end_invoice'),
    url(r'^add_shipping_to_invoice/$', views.add_shipping_to_invoice,
        name='add_shipping_to_invoice'),
    url(r'^invoices/$', views.invoices, name='invoices'),

    # api calls -------------------------------------------------------
    url(r'^manual_period_invoice/(?P<order_id>[\w\-]+)/$',
        views.manual_period_invoice, name='manual_period_invoice'),
    url(r'^test_period_invoice/(?P<order_id>[\w\-]+)/$',
        views.test_period_invoice, name='test_period_invoice'),
    url(r'^send_invoice_fnox/(?P<invoice_id>[\w\-]+)/$',
        views.send_invoice_fnox, name='send_invoice_fnox'),
    url(r'^send_all_fnox_invoices/$', views.send_all_fnox_invoices,
        name='send_all_fnox_invoices'),
]
