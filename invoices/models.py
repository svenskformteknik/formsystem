from django.db import models
from orders.models import Order


# Create your models here.
class InvoiceTriggerDate(models.Model):
    order = models.ForeignKey(Order)
    date = models.DateField()

    def __str__(self):
        return str(self.date)


class Invoice(models.Model):
    INVOICE_TYPES = (
        ('uppstart', 'Uppstart'),
        ('period', 'Period'),
        ('slut', 'Slut'),
    )

    order = models.ForeignKey(Order)
    fnox_invoice_id = models.IntegerField(unique=True, null=True)
    invoice_type = models.CharField(max_length=128,
                                    choices=INVOICE_TYPES,
                                    default='period')
    date = models.DateField()

    created = models.BooleanField(default=True)
    completed = models.BooleanField(default=False)

    invoice_address = models.CharField(max_length=128)
    invoice_address2 = models.CharField(max_length=128, blank=True, null=True)
    invoice_postnr = models.CharField(max_length=12)
    invoice_place = models.CharField(max_length=128)
    invoice_country = models.CharField(max_length=128)

    delivery_address = models.CharField(max_length=128, blank=True, null=True)
    delivery_address2 = models.CharField(max_length=128, blank=True, null=True)
    delivery_postnr = models.CharField(max_length=12, blank=True, null=True)
    delivery_place = models.CharField(max_length=128, blank=True, null=True)
    delivery_country = models.CharField(max_length=128, blank=True, null=True)

    customer_nr = models.CharField(max_length=128)
    customer_referent = models.CharField(max_length=128)
    customer_ordernr = models.IntegerField()
    delivery_terms = models.CharField(max_length=128)
    delivery_type = models.CharField(max_length=128)
    vat_nr = models.IntegerField()

    our_referent = models.CharField(max_length=128)
    payment_terms = models.CharField(max_length=128)
    expiry_date = models.DateField()
    delay_interest = models.FloatField()
    delivery_date = models.DateField()
    our_order_nr = models.IntegerField()

    our_address = models.CharField(
            max_length=128, default="AB Svensk Formteknik")
    our_address2 = models.CharField(max_length=128, default="Lagervägen 25")
    our_postnr = models.CharField(max_length=128, default="14291")
    our_place = models.CharField(max_length=128, default="Skogås")
    our_country = models.CharField(max_length=128, default="Sverige")
    our_homepage_url = models.URLField(
            default="http://www.svenskformteknik.se")

    our_tel = models.CharField(max_length=128, default="08-7079097")
    our_email = models.EmailField(default="info@svenskformteknik.se")

    plusgiro = models.CharField(max_length=128, blank=True, null=True)
    bankgiro = models.CharField(max_length=128, default="457-9603")
    seat = models.CharField(
            max_length=128, default="Stockholms län, Huddinge kommun")

    our_orgnr = models.CharField(max_length=128, default="556251-9933")
    our_momsregnr = models.CharField(max_length=128, default="SE556251993301")
    fskatt = models.CharField(max_length=128, default="Godkänd för F-skatt")

    iban = models.CharField(max_length=128, default="SE2780000832790148447097")
    bic = models.CharField(max_length=128, default="SWEDSESS")

    def __str__(self):
        return str(self.id)


class InvoiceArticle(models.Model):
    invoice = models.ForeignKey(Invoice)
    product = models.CharField(max_length=128, blank=True, null=True)
    price_list = models.CharField(max_length=128, blank=True, null=True)
    price_type = models.CharField(max_length=128, blank=True, null=True)
    amount = models.IntegerField(default=0)
    days_rented = models.IntegerField(default=0)
    base = models.FloatField(default=0)
    base_is_default = models.BooleanField()
    price = models.FloatField(default=0)
    price_is_default = models.BooleanField()
    discount = models.FloatField()
    discount_is_default = models.BooleanField(default=True)
    total_price = models.FloatField()
    account = models.IntegerField()
    order_id = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.product
