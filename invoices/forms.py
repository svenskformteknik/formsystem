from django.forms import ModelForm, TextInput
from invoices.models import Invoice, InvoiceArticle


class InvoiceForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(InvoiceForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] =\
                    'form-control form-control-sm'
        self.fields['fnox_invoice_id'].required = False

    class Meta:
        model = Invoice
        fields = '__all__'
        widgets = {
            'vat_nr': TextInput(),
            'delay_interest': TextInput(),
            'our_order_nr': TextInput()
        }


class InvoiceArticleForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(InvoiceArticleForm, self).__init__(*args, **kwargs)
        # set visible field to use custom class
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] =\
                    'form-control form-control-sm'

    class Meta:
        model = InvoiceArticle
        fields = '__all__'
        widgets = {
            'amount': TextInput(),
            'days_rented': TextInput(),
            'base': TextInput(),
            'price': TextInput(),
            'discount': TextInput(),
            'total_price': TextInput(),
            'account': TextInput(),
        }
