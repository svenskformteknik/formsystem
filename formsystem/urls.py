"""formsystem URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf import settings
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # any url starting with orders is handled by order app
    url(r'^orders/', include('orders.urls')),
    # any url starting with customers is handled b customer app
    url(r'^customers/', include('customers.urls')),
    # any url starting with products is handled by products app
    url(r'^products/', include('products.urls')),
    # any url starting with invoice is handled by invoice app
    url(r'^invoices/', include('invoices.urls')),
    # any url starting with reports is handled by reports app
    url(r'^reports/', include('reports.urls')),

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
